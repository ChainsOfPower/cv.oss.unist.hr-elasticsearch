<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 15.9.2016.
 * Time: 3:43
 */

namespace AppBundle\Controller\ElasticSearch;

use AppBundle\Form\ElasticSearch\SearchProfessionalActivitiesType;
use AppBundle\Form\ElasticSearch\SearchScientificActivitiesType;
use AppBundle\Form\ElasticSearch\SearchSkillsAndCompetencesType;
use AppBundle\Form\ElasticSearch\SearchTeachingActivitiesType;
use AppBundle\Form\ElasticSearch\SearchWorkExperiencesType;
use AppBundle\Form\WorkExperienceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ElasticSearchController extends Controller
{
    /**
     * @Route("/{_locale}/skills_and_competences_search", name="skills_and_competences_search", requirements={"_locale"="hr|en"})
     * @Method({"GET"})
     */
    public function searchSkillsAndCompetences(Request $request)
    {
        $form = $this->createForm(new SearchSkillsAndCompetencesType(), null, array(
            'action' => $this->generateUrl('skills_and_competences_filter'),
            'method' => 'GET'
        ));

        return $this->render('AppBundle:ElasticSearch/skillsAndCompetences:index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/skills_and_competences_filter", name="skills_and_competences_filter")
     * @Method({"GET"})
     */
    public function filterSkillsAndCompetences(Request $request)
    {
        $dataForm = $request->get('skillsAndCompetencesSearch');
        $repositoryManager = $this->get('fos_elastica.manager');

        $skillsAndCompetencesRepository = $repositoryManager->getRepository('AppBundle:Person');

        $results = $skillsAndCompetencesRepository->filterAllQuery($dataForm['social_skills'], $dataForm['organization_skills'], $dataForm['technical_skills'], $dataForm['art_skills'], $dataForm['misc_skills']);

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/{_locale}/scientific_activities_search", name="scientific_activities_search", requirements={"_locale"="hr|en"})
     * @Method({"GET"})
     */
    public function searchScientificActivities(Request $request)
    {
        $form = $this->createForm(new SearchScientificActivitiesType(), null, array(
            'action' => $this->generateUrl('scientific_activities_filter'),
            'method' => 'GET'
        ));

        return $this->render('AppBundle:ElasticSearch/scientificActivities:index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/scientific_activities_filter", name="scientific_activities_filter")
     * @Method({"GET"})
     */
    public function filterScientificActivities(Request $request)
    {
        $dataForm = $request->get('scientificActivitiesSearch');
        $repositoryManager = $this->get('fos_elastica.manager');

        $scientificActivitiesRepository = $repositoryManager->getRepository('AppBundle:ScientificActivity');


        if($dataForm['age'] != "0")
        {
            $date = new \DateTime('-'.$dataForm['age'].' year');
        }
        else
        {
            $date = null;
        }


        $results = $scientificActivitiesRepository->filterAllQuery($dataForm['description'], $dataForm['authors'], $date);

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    /**
     * @Route("/{_locale}/professional_activities_search", name="professional_activities_search", requirements={"_locale"="hr|en"})
     * @Method({"GET"})
     */
    public function searchProfessionalActivities(Request $request)
    {
        $form = $this->createForm(new SearchProfessionalActivitiesType(), null, array(
            'action' => $this->generateUrl('professional_activities_filter'),
            'method' => 'GET'
        ));

        return $this->render('@App/ElasticSearch/professionalActivities/index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/professional_activities_filter", name="professional_activities_filter")
     * @Method({"GET"})
     */
    public function filterProfessionalActivities(Request $request)
    {
        $dataForm = $request->get('professionalActivitiesSearch');
        $repositoryManager = $this->get('fos_elastica.manager');

        $professionalActivitiesRepository = $repositoryManager->getRepository('AppBundle:ProfessionalActivity');


        if($dataForm['age'] != "0")
        {
            $date = new \DateTime('-'.$dataForm['age'].' year');
        }
        else
        {
            $date = null;
        }


        $results = $professionalActivitiesRepository->filterAllQuery($dataForm['description'], $dataForm['authors'], $date);

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/{_locale}/teaching_activities_search", name="teaching_activities_search", requirements={"_locale"="hr|en"})
     * @Method({"GET"})
     */
    public function searchTeachingActivities(Request $request)
    {
        $form = $this->createForm(new SearchTeachingActivitiesType(),null, array(
            'action' => $this->generateUrl('teaching_activities_filter'),
            'method' => 'GET'
        ));

        return $this->render('AppBundle:ElasticSearch/teachingActivities:index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/teaching_activities_filter", name="teaching_activities_filter")
     * @Method({"GET"})
     */
    public function filterTeachingActivities(Request $request)
    {
        $dataForm = $request->get('teachingActivitiesSearch');
        $repositoryManager = $this->get('fos_elastica.manager');

        $teachingActivitiesRepository = $repositoryManager->getRepository('AppBundle:TeachingActivity');

        if($dataForm['age'] != "0")
        {
            $date = new \DateTime('-'.$dataForm['age'].' year');
        }
        else
        {
            $date = null;
        }
        $results = $teachingActivitiesRepository->filterAllQuery($dataForm['description'], $dataForm['authors'], $date);

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    /**
     * @Route("/{_locale}/work_experiences_search", name="work_experiences_search", requirements={"_locale"="hr|en"})
     * @Method({"GET"})
     */
    public function searchWorkExperiences(Request $request)
    {
        $form = $this->createForm(new SearchWorkExperiencesType(), null, array(
            'action' => $this->generateUrl('work_experiences_filter'),
            'method' => 'GET'
        ));

        return $this->render('AppBundle:ElasticSearch/workExperience:index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/work_experiences_filter", name="work_experiences_filter")
     * @Method({"GET"})
     */
    public function filterWorkExperiences(Request $request)
    {
        $dataForm = $request->get('workExperiencesSearch');
        $repositoryManager = $this->get('fos_elastica.manager');

        $workExperiencesRepository = $repositoryManager->getRepository('AppBundle:WorkExperience');

        if(array_key_exists('current', $dataForm))
        {
            $dataForm['current'] = true;
        }
        else
        {
            $dataForm['current'] = false;
        }

        $results = $workExperiencesRepository->filterAllQuery($dataForm['job_title'], $dataForm['area'], $dataForm['institution'], $dataForm['function'], $dataForm['current']);

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}