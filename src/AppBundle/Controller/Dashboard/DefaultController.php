<?php

namespace AppBundle\Controller\Dashboard;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\Person;
use AppBundle\Form\PersonalType;
use AppBundle\Form\SkillsType;
use AppBundle\Form\WorkExperienceType;
use AppBundle\Form\SchoolType;
use AppBundle\Form\SpecializationType;
use AppBundle\Form\PersonLanguageType;
use AppBundle\Entity\PersonLanguage;
use Symfony\Component\Form\Form;

class DefaultController extends Controller
{
    /**
     * @Route("/profil", name="app.dashboard.default.dashboard")
     */
    public function dashboardAction(Request $request)
    {
        $person = $this->getCurrentPerson();
        $currentRouteName = $request->get('_route');
        $form = $this->createForm(new PersonalType(), $person, array(
            'action' => $this->generateUrl($currentRouteName),
            'method' => 'POST',
            'locale' => $request->getLocale()
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $person, $currentRouteName, $currentRouteName);
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // Do something on failed attempt
            }
        }

        return $this->render('AppBundle:Dashboard/Personal:form.html.twig', array(
            'page_title' => 'Osobni podaci',
            'icon'       => 'fa fa-user',
            'user'       => $person,
            'form'       => $form->createView()
        ));
    }

    /**
     * @Route("/profil/radno-iskustvo", name="app.dashboard.default.work")
     */
    public function workAction(Request $request)
    {
        $person = $this->getCurrentPerson();
        $currentRouteName = $request->get('_route');
        $form = $this->createForm(new WorkExperienceType(), $person, array(
            'action' => $this->generateUrl($currentRouteName),
            'method' => 'POST',
            'locale' => $request->getLocale()
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $person, $currentRouteName, $currentRouteName);
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // Do something on failed attempt
            }
        }

        return $this->render('AppBundle:Dashboard/WorkExperience:form.html.twig', array(
            'page_title' => 'Radno iskustvo',
            'icon'       => 'fa fa-suitcase',
            'user'       => $person,
            'form'       => $form->createView()
        ));
    }

    /**
     * @Route("/profil/skolovanje", name="app.dashboard.default.school")
     */
    public function schoolAction(Request $request)
    {
        $person = $this->getCurrentPerson();
        $currentRouteName = $request->get('_route');
        $form = $this->createForm(new SchoolType(), $person, array(
            'action' => $this->generateUrl($currentRouteName),
            'method' => 'POST',
            'locale' => $request->getLocale()
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $person, $currentRouteName, $currentRouteName);
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // Do something on failed attempt
            }
        }

        return $this->render('AppBundle:Dashboard/School:form.html.twig', array(
            'page_title' => 'Školovanje',
            'icon'       => 'fa fa-university',
            'user'       => $person,
            'form'       => $form->createView()
        ));
    }

    /**
     * @Route("/profil/usavrsavanje", name="app.dashboard.default.specialization")
     */
    public function specializationAction(Request $request)
    {
        $person = $this->getCurrentPerson();
        $currentRouteName = $request->get('_route');
        $form = $this->createForm(new SpecializationType(), $person, array(
            'action' => $this->generateUrl($currentRouteName),
            'method' => 'POST',
            'locale' => $request->getLocale()
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $person, $currentRouteName, $currentRouteName);
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // Do something on failed attempt
            }
        }

        return $this->render('AppBundle:Dashboard/Specialization:form.html.twig', array(
            'page_title' => 'Usavršavanje',
            'icon'       => 'fa fa-cubes',
            'user'       => $person,
            'form'       => $form->createView()
        ));
    }

    /**
     * @Route("/profil/jezicne-kompetencije", name="app.dashboard.default.languages")
     */
    public function languagesAction(Request $request)
    {
        $person = $this->getCurrentPerson();
        $currentRouteName = $request->get('_route');
        $form = $this->createForm(new PersonLanguageType(), $person, array(
            'action' => $this->generateUrl($currentRouteName),
            'method' => 'POST',
            'locale' => $request->getLocale()
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $person, $currentRouteName, $currentRouteName);
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // Do something on failed attempt
            }
        }

        return $this->render('AppBundle:Dashboard/Language:form.html.twig', array(
            'page_title' => 'Jezične kompetencije',
            'icon'       => 'fa fa-language',
            'user'       => $person,
            'form'       => $form->createView()
        ));
    }

    /**
     * @Route("/profil/vjestine-i-kompetencije", name="app.dashboard.default.skills")
     */
    public function skillsAction(Request $request)
    {
        $person = $this->getCurrentPerson();
        $currentRouteName = $request->get('_route');
        $form = $this->createForm(new SkillsType(), $person, array(
            'action' => $this->generateUrl($currentRouteName),
            'method' => 'POST',
            'locale' => $request->getLocale()
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $person, $currentRouteName, $currentRouteName);
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // Do something on failed attempt
            }
        }

        return $this->render('AppBundle:Dashboard/Skills:form.html.twig', array(
            'page_title' => 'Vještine i kompetencije',
            'icon'       => 'fa fa-certificate',
            'user'       => $person,
            'form'       => $form->createView()
        ));
    }

    /**
     * @Route("/profil/postavke/ukloni-fotografiju", name="app.dashboard.default.removeProfilePicture")
     */
    public function removeProfilePictureAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $person = $this->getCurrentPerson();
        if ($person->getProfilePicturePath()) {
            $person->setProfilePicturePath(null);

            $em->persist($person);
            $em->flush();

            $this->addFlash( 'success', 'Fotografija je uspješno uklonjena.' );
        }

        return $this->redirect(
            $this->generateUrl('app.dashboard.default.dashboard')
        );
    }

    /**
     * Handles the persist attempt
     *
     * @param Request $request
     * @param Form $form
     * @param mixed $entity
     * @param string $onSuccessRouteName
     * @param string $onFailedRouteName
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|boolean|NULL
     */
    private function handlePersistAttempt(Request $request, Form $form, $entity, $onSuccessRouteName, $onFailedRouteName)
    {
        $em = $this->getDoctrine()->getEntityManager();
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                if ($onSuccessRouteName == 'app.dashboard.default.dashboard') {
                    $entity->upload();
                }

                $em->persist($entity);
                $em->flush();

                $this->addFlash( 'success', 'Izmjene su uspješno spremljene.' );

                return $this->redirect(
                    $this->generateUrl($onSuccessRouteName)
                );
            }

            $this->addFlash( 'error', 'Došlo je do pogreške. Pregledajte unesene podatke, ispravite pogreške i pokušajte ponovo. Izmjene neće biti spremljene dok se greške ne isprave.' );

            return false;
        }

        return null;
    }

    /**
     * Returns the persons mother tongue
     *
     * @param Person $person
     * @return string
     */
    private function getCurrentPersonMotherTongue(Person $person)
    {
        $relation = $this
            ->getDoctrine()
            ->getRepository('AppBundle:PersonLanguage')
            ->findOneBy(array(
                'person' => $person,
                'motherTongue' => true
            ))
        ;

        if ($relation instanceof PersonLanguage) {
            return $relation->getLanguage();
        }

        return '';
    }

    /**
     * Returns the logged-in user.
     *
     * @return Person
     */
    private function getCurrentPerson()
    {
    	return $this->get('security.context')->getToken()->getUser();
    }
}
