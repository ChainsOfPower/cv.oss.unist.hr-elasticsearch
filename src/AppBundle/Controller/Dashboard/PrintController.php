<?php

namespace AppBundle\Controller\Dashboard;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Person;
use AppBundle\Entity\PersonLanguage;

class PrintController extends Controller
{
    /**
     * @Route("/print/cv", name="app.dashboard.print.cv")
     */
    public function cvAction(Request $request)
    {
        $template = 'AppBundle:Print/CV:content.html.twig';
        if ($request->get('lang', 'hr') == 'en')
            $template = 'AppBundle:Print/CV:content_en.html.twig';

        return $this->render($template, array(
            'person' => $this->getCurrentPerson(),
            'mother_tongue' => $this->getCurrentPersonMotherTongue()
        ));
    }

    /**
     * @Route("/print/class-cv", name="app.dashboard.print.classCV")
     */
    public function classCVAction()
    {
        $person = $this->getCurrentPerson();
        $doctrine = $this->getDoctrine();
        $repo = $this->getDoctrine();
        $firstList = '';
        $secondList = '';
        $thirdList = '';
        $fourthList = '';
        $fifthList = '';
        $awards = '';

        $currentJob = $doctrine->getRepository('AppBundle:WorkExperience')->findCurrentByPerson($person);
        $lastSchool = $doctrine->getRepository('AppBundle:School')->findLastByPerson($person);
        $specialization = $doctrine->getRepository('AppBundle:Specialization')->findByPerson($person, array(
            'date' => 'DESC'
        ));

        foreach ( $person->getScientificActivities() as $activity ) {
            $activityDOM = '- ' . $activity->getAuthors() . ': ' . $activity->getDescription() . '<br />';
            $categoryName = (null != $activity->getScientificActivityCategory())? $activity->getScientificActivityCategory()->getName() : null;
            $typeName = (null != $activity->getScientificActivityType())? $activity->getScientificActivityType()->getName() : null;
            if ( $typeName != null ) {
                $typeID = substr($typeName, 0, 4);
                if ( in_array( $typeID, array('1.2.', '1.3.') ) ) {
                    $secondList .= $activityDOM;
                }
                if ( in_array( $typeID, array('1.4.', '1.5.', '1.6.', '1.7.', '1.8.') ) ) {
                    $thirdList .= $activityDOM;
                    $fourthList .= $activityDOM;
                }
                if ( in_array( $typeID, array('2.1.', '2.2.', '2.3.') ) ) {
                    $fifthList .= $activityDOM;
                }
            }
        }

        foreach ( $person->getTeachingActivities() as $activity ) {
            $activityDOM = '- ' . $activity->getAuthors() . ': ' . $activity->getDescription() . '<br />';
            $categoryName = (null != $activity->getTeachingActivityCategory())? $activity->getTeachingActivityCategory()->getName() : null;
            $typeName = (null != $activity->getTeachingActivityType())? $activity->getTeachingActivityType()->getName() : null;
            if ( $categoryName != null ) {
                $categoryID = substr($categoryName, 0, 4);
                if ( in_array( $categoryID, array('B.7.', 'B.8.') ) ) {
                    $firstList .= $activityDOM;
                }
                if ( in_array( $categoryID, array('B.3.', 'B.4.') ) ) {
                    $secondList .= $activityDOM;
                }
            }
        }

        foreach ( $person->getProfessionalActivities() as $activity ) {
            $activityDOM = '- ' . $activity->getAuthors() . ': ' . $activity->getDescription() . '<br />';
            $categoryName = (null != $activity->getProfessionalActivityCategory())? $activity->getProfessionalActivityCategory()->getName() : null;
            $typeName = (null != $activity->getProfessionalActivityType())? $activity->getProfessionalActivityType()->getName() : null;
            if ( $categoryName != null ) {
                $categoryID = substr($categoryName, 0, 4);
                if ( in_array( $categoryID, array('C.1.') ) ) {
                    $secondList .= $activityDOM;
                }
                if ( in_array( $categoryID, array('C.3.', 'C.4.') ) ) {
                    $thirdList .= $activityDOM;
                    $fourthList .= $activityDOM;
                }
                if ( in_array( $categoryID, array('C.11') ) ) {
                    $awards .= $activityDOM;
                }
            }
        }

        return $this->render('AppBundle:Print/ClassCV:content.html.twig', array(
            'person' => $person,
            'current_job' => $currentJob,
            'last_school' => $lastSchool,
            'specialization' => $specialization,
            'first_list' => $firstList,
            'second_list' => $secondList,
            'third_list' => $thirdList,
            'fourth_list' => $fourthList,
            'fifth_list' => $fifthList,
            'awards' => $awards,
            'mother_tongue' => $this->getCurrentPersonMotherTongue()
        ));
    }

    /**
     * @Route("/print/competition", name="app.dashboard.print.competition")
     */
    public function competitionAction(Request $request)
    {
        $template = 'AppBundle:Print/Competition:content.html.twig';
        if ($request->get('lang', 'hr') == 'en')
            $template = 'AppBundle:Print/Competition:content_en.html.twig';

        return $this->render($template, array(
            'person' => $this->getCurrentPerson()
        ));
    }

    private function getCurrentPersonMotherTongue()
    {
        $relation = $this
            ->getDoctrine()
            ->getRepository('AppBundle:PersonLanguage')
            ->findOneBy(array(
                'person' => $this->getCurrentPerson(),
                'motherTongue' => true
            ))
        ;

        if ($relation instanceof PersonLanguage) {
            return $relation->getLanguage();
        }

        return null;
    }

    private function getCurrentPerson()
    {
        return $this->get('security.context')->getToken()->getUser();
    }
}
