<?php

namespace AppBundle\Controller\Dashboard;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use AppBundle\Form\ScientificActivityType;
use AppBundle\Form\TeachingActivityType;
use AppBundle\Form\ProfessionalActivityType;
use AppBundle\Entity\Person;

class ActivityController extends Controller
{
    /**
     * @Route("/profil/znanstvena-djelatnost", name="app.dashboard.activity.scientific")
     */
    public function scientificAction(Request $request)
    {
        $person = $this->getCurrentPerson();
        $currentRouteName = $request->get('_route');
        $form = $this->createForm(new ScientificActivityType(), $person, array(
            'action'   => $this->generateUrl($currentRouteName),
            'method'   => 'POST',
            'locale'   => $request->getLocale()
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $person, $currentRouteName, $currentRouteName);
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // Do something on failed attempt
            }
        }

        return $this->render('AppBundle:Dashboard/ScientificActivity:form.html.twig', array(
            'page_title' => 'Znanstvena djelatnost',
            'icon'       => 'fa fa-flask',
            'form'       => $form->createView()
        ));
    }

    /**
     * @Route("/profil/nastavna-djelatnost", name="app.dashboard.activity.teaching")
     */
    public function teachingAction(Request $request)
    {
        $person = $this->getCurrentPerson();
        $currentRouteName = $request->get('_route');
        $form = $this->createForm(new TeachingActivityType(), $person, array(
            'action'   => $this->generateUrl($currentRouteName),
            'method'   => 'POST',
            'locale'   => $request->getLocale()
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $person, $currentRouteName, $currentRouteName);
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // Do something on failed attempt
            }
        }

        return $this->render('AppBundle:Dashboard/TeachingActivity:form.html.twig', array(
            'page_title' => 'Nastavna djelatnost',
            'icon'       => 'fa fa-users',
            'form'       => $form->createView()
        ));
    }

    /**
     * @Route("/profil/strucna-djelatnost", name="app.dashboard.activity.professional")
     */
    public function professionalAction(Request $request)
    {
        $person = $this->getCurrentPerson();
        $currentRouteName = $request->get('_route');
        $form = $this->createForm(new ProfessionalActivityType(), $person, array(
            'action'   => $this->generateUrl($currentRouteName),
            'method'   => 'POST',
            'locale'   => $request->getLocale()
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $person, $currentRouteName, $currentRouteName);
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // Do something on failed attempt
            }
        }

        return $this->render('AppBundle:Dashboard/ProfessionalActivity:form.html.twig', array(
            'page_title' => 'Stručna djelatnost',
            'icon'       => 'fa fa-cog',
            'form'       => $form->createView()
        ));
    }

    /**
     * Handles the persist attempt
     *
     * @param Request $request
     * @param Form $form
     * @param mixed $entity
     * @param string $onSuccessRouteName
     * @param string $onFailedRouteName
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|boolean|NULL
     */
    private function handlePersistAttempt(Request $request, Form $form, $entity, $onSuccessRouteName, $onFailedRouteName)
    {
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($entity);
                $em->flush();

                $this->addFlash( 'success', 'Izmjene su uspješno spremljene.' );

                return $this->redirect(
                    $this->generateUrl($onSuccessRouteName)
                );
            }

            $this->addFlash( 'error', 'Došlo je do pogreške. Pregledajte unesene podatke, ispravite pogreške i pokušajte ponovo. Izmjene neće biti spremljene dok se greške ne isprave.' );

            return false;
        }

        return null;
    }

    /**
     * Get logged-in person
     *
     * @return Person
     */
    private function getCurrentPerson()
    {
    	return $this->get('security.context')->getToken()->getUser();
    }
}
