<?php

namespace AppBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Form\SearchType;
use AppBundle\Entity\Person;
use AppBundle\Entity\PersonLanguage;

class SearchController extends Controller
{
	/**
	 * @Route("/", name="app.frontend.search.root")
	 * @Method({"GET"})
	 */
	public function rootAction(Request $request)
	{
		return $this->redirect($this->generateUrl('app.frontend.search.index', array(
			'_locale' => $request->getLocale()
		)));
	}

	/**
	 * @Route("/{_locale}", name="app.frontend.search.index", requirements={"_locale"="hr|en"})
	 * @Method({"GET"})
	 */
	public function indexAction(Request $request)
	{
		$form = $this->createForm(new SearchType(), null, array(
			'action' => $this->generateUrl('app.frontend.search.filter'),
			'method' => 'GET',
			'locale' => $request->get('_locale')
		));

		return $this->render('AppBundle:Search:index.html.twig', array(
			'persons'  => array(),
			'form'     => $form->createView(),
			'back_url' => $this->get('request')->server->get('HTTP_REFERER')
		));
	}

	/**
	 * @Route("/{_locale}/filter", name="app.frontend.search.filter")
	 * @Method({"GET"})
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function filterAction(Request $request)
	{
		$doctrine = $this->getDoctrine();
		$repository = $doctrine->getRepository('AppBundle:Person');
		$data = $request->get('database_search');
		$first_name = $data['first_name'];
		$last_name = $data['last_name'];
		$uid = (isset($data['university']))? (int)$data['university'] : 0 ;
		$did = (isset($data['universityDepartment']))? (int)$data['universityDepartment'] : 0 ;
		$ddid = (isset($data['universityDepartmentDesk']))? (int)$data['universityDepartmentDesk'] : 0 ;
		$said = (isset($data['scientificArea']))? (int)$data['scientificArea'] : 0 ;
		$sfid = (isset($data['scientificField']))? (int)$data['scientificField'] : 0 ;
		$sbid = (isset($data['scientificBranch']))? (int)$data['scientificBranch'] : 0 ;
		$freelancer = (isset($data['freelancer']) && $data['freelancer'])? true : false ;
		$expiration = (isset($data['expiration']) && $data['expiration'])? true : false ;

		$persons = $repository->findByQuickSearchParams(
			$first_name,
			$last_name,
			$uid,
			$did,
			$ddid,
			$said,
			$sfid,
			$sbid,
			$freelancer,
			$expiration,
			$request->get('_locale')
		);

		for ($i = 0; $i < sizeof($persons); $i++) {
			$person = $persons[$i];
			$persons[$i]['index'] = $i;
			if ($person['profile_picture'] == null || $person['profile_picture'] == '') {
				if ($person['gender'] == 'M')
					$persons[$i]['profile_picture'] = '/img/gender_male.jpg';
				else if ($person['gender'] == 'Z')
					$persons[$i]['profile_picture'] = '/img/gender_female.jpg';
				else
					$persons[$i]['profile_picture'] = '/img/gender_female.jpg';
			} else {
				$persons[$i]['profile_picture'] = '/uploads/profile/'.$person['ID'].'/'.$person['profile_picture'];
			}
			$temp = $persons[$i]['profile_picture'];
			$persons[$i]['profile_picture'] = $this->get('liip_imagine.cache.manager')->getBrowserPath($temp, '120x120');
			$persons[$i]['profile_picture_large'] = $this->get('liip_imagine.cache.manager')->getBrowserPath($temp, '300x205');
		}

		return new JsonResponse(
			array( 'personnel' => $persons )
		);
	}

	/**
	 * @Route("/preview/document/{id}/cv", name="app.frontend.search.CV")
	 * @Method({"GET"})
	 */
	public function CVAction(Request $request, $id)
	{
		$person = $this->getDoctrine()->getRepository('AppBundle:Person')->find($id);
		$template = 'AppBundle:Print/CV:content.html.twig';
		if ($request->get('lang', 'hr') == 'en')
			$template = 'AppBundle:Print/CV:content_en.html.twig';

		return $this->render($template, array(
				'person' => $person,
				'mother_tongue' => $this->getPersonMotherTongue($person)
		));
	}

	/**
	 * @Route("/preview/document/{id}/competition", name="app.frontend.search.competition")
	 * @Method({"GET"})
	 */
	public function competitionAction(Request $request, $id)
	{
		$person = $this->getDoctrine()->getRepository('AppBundle:Person')->find($id);
		$template = 'AppBundle:Print/Competition:content.html.twig';
		if ($request->get('lang', 'hr') == 'en')
			$template = 'AppBundle:Print/Competition:content_en.html.twig';

		return $this->render($template, array(
				'person' => $person
		));
	}

	/**
	 * @Route("/preview/document/list", name="app.frontend.search.list")
	 * @Method({"GET"})
	 */
	public function listAction(Request $request)
	{
		$response = array();
		$ids = explode(',', $request->get('ids', ''));
		$doctrine = $this->getDoctrine();
		$repository = $doctrine->getRepository('AppBundle:Person');
		$filter = $this->getFilterString($request->get('database_search'), $request->get('lang', 'hr'));
		$persons = $repository->findByIdsForList($ids, $request->get('lang', 'hr'));
		$total = array(
			'persons'      => 0,
			'scientific'   => 0,
			'professional' => 0,
			'mentoring'    => 0
		);
		$template = 'AppBundle:Print/List:content.html.twig';
		if ($request->get('lang', 'hr') == 'en')
			$template = 'AppBundle:Print/List:content_en.html.twig';

		foreach ($persons as $person) {
			$total['persons']++;
			$total['scientific'] += $person['scientific_papers'];
			$total['professional'] += $person['professional_papers'];
			$total['mentoring'] += $person['mentoring'];
		}

		return $this->render($template, array(
			'filter' => $filter,
			'total'  => $total,
			'persons' => $persons
		));
	}

	/**
	 * @Route("/preview/document/activity-list", name="app.frontend.search.activityList")
	 * @Method({"GET"})
	 */
	public function activityListAction(Request $request)
	{
		$response = array();
		$ids = explode(',', $request->get('ids', ''));
		$period = $request->get('period', null);
		$doctrine = $this->getDoctrine();
		$filter = $this->getFilterString($request->get('database_search'), $request->get('lang', 'hr'));
		$template = 'AppBundle:Print/ActivityList:content.html.twig';
		if ($request->get('lang', 'hr') == 'en')
			$template = 'AppBundle:Print/ActivityList:content_en.html.twig';

		$saRepository = $doctrine->getRepository('AppBundle:ScientificActivity');
		$paRepository = $doctrine->getRepository('AppBundle:ProfessionalActivity');
		$taRepository = $doctrine->getRepository('AppBundle:TeachingActivity');

		$scientificCategories = $doctrine->getRepository('AppBundle:ScientificActivityCategory')->findAll();
		$teachingCategories = $doctrine->getRepository('AppBundle:TeachingActivityCategory')->findAll();
		$professionalCategories = $doctrine->getRepository('AppBundle:ProfessionalActivityCategory')->findAll();

		// ===================
		// Scientific activity
		// ===================

		$response['A'] = array(
			'name'       => ($request->get('lang', 'hr') == 'hr')? 'ZNANSTVENA DJELATNOST' : 'SCIENTIFIC ACTIVITY' ,
			'items'      => array(), // always empty
			'categories' => array(),
			'total'      => 0
		);

		foreach ($scientificCategories as $category) {
			$cid = $category->getId();
			$response['A']['categories'][$cid] = array(
				'name'  => ($request->get('lang', 'hr') == 'hr')? $category->getName() : $category->getNameEn(),
				'items' => array(),
				'types' => array(),
				'total' => 0
			);
			foreach ($ids as $personID) {
				$person = $doctrine->getRepository('AppBundle:Person')->find($personID);
				$activity = $saRepository->findByPersonCategoryTypeAndPeriod(
					$person->getId(),
					$category->getId(),
					null,
					$period
				);
				if ($activity > 0) {
					$response['A']['categories'][$cid]['total'] += $activity;
					$response['A']['categories'][$cid]['items'][] = array(
						'first_name' => $person->getFirstName(),
						'last_name' => $person->getLastName(),
						'activity'  => $activity
					);
				}
			}
			foreach ($category->getScientificActivityTypes() as $type) {
				$tid = $type->getId();
				$response['A']['categories'][$cid]['types'][$tid] = array(
					'name'  => ($request->get('lang', 'hr') == 'hr')? $type->getName() : $type->getNameEn(),
					'items' => array(),
					'total' => 0
				);
				foreach ($ids as $personID) {
					$person = $doctrine->getRepository('AppBundle:Person')->find($personID);
					$activity = $saRepository->findByPersonCategoryTypeAndPeriod(
						$person->getId(),
						$category->getId(),
						$type->getId(),
						$period
					);
					if ($activity > 0) {
						$response['A']['categories'][$cid]['total'] += $activity;
						$response['A']['categories'][$cid]['types'][$tid]['total'] += $activity;
						$response['A']['categories'][$cid]['types'][$tid]['items'][] = array(
							'first_name' => $person->getFirstName(),
							'last_name' => $person->getLastName(),
							'activity'  => $activity
						);
					}
				}
			}
		}

		// =================
		// Teaching activity
		// =================

		$response['B'] = array(
			'name'       => ($request->get('lang', 'hr') == 'hr')? 'NASTAVNA DJELATNOST' : 'TEACHING ACTIVITY' ,
			'items'      => array(), // always empty
			'categories' => array(),
			'total' => 0
		);

		foreach ($teachingCategories as $category) {
			$cid = $category->getId();
			$response['B']['categories'][$cid] = array(
				'name'  => ($request->get('lang', 'hr') == 'hr')? $category->getName() : $category->getNameEn(),
				'items' => array(),
				'types' => array(),
				'total' => 0
			);
			foreach ($ids as $personID) {
				$person = $doctrine->getRepository('AppBundle:Person')->find($personID);
				$activity = $taRepository->findByPersonCategoryTypeAndPeriod(
					$person->getId(),
					$category->getId(),
					null,
					$period
				);
				if ($activity > 0) {
					$response['B']['categories'][$cid]['total'] += $activity;
					$response['B']['categories'][$cid]['items'][] = array(
						'first_name' => $person->getFirstName(),
						'last_name' => $person->getLastName(),
						'activity'  => $activity
					);
				}
			}
			foreach ($category->getTeachingActivityTypes() as $type) {
				$tid = $type->getId();
				$response['B']['categories'][$cid]['types'][$tid] = array(
					'name'  => ($request->get('lang', 'hr') == 'hr')? $type->getName() : $type->getNameEn(),
					'items' => array(),
					'total' => 0
				);
				foreach ($ids as $personID) {
					$person = $doctrine->getRepository('AppBundle:Person')->find($personID);
					$activity = $taRepository->findByPersonCategoryTypeAndPeriod(
						$person->getId(),
						$category->getId(),
						$type->getId(),
						$period
					);
					if ($activity > 0) {
						$response['B']['categories'][$cid]['total'] += $activity;
						$response['B']['categories'][$cid]['types'][$tid]['total'] += $activity;
						$response['B']['categories'][$cid]['types'][$tid]['items'][] = array(
							'first_name' => $person->getFirstName(),
							'last_name' => $person->getLastName(),
							'activity'  => $activity
						);
					}
				}
			}
		}

		// ====================
		// Professinal activity
		// ====================

		$response['C'] = array(
			'name'       => ($request->get('lang', 'hr') == 'hr')?  'STRUČNA DJELATNOST' : 'PROFESSIONAL ACTIVITY' ,
			'items'      => array(), // always empty
			'categories' => array(),
			'total' => 0
		);

		foreach ($professionalCategories as $category) {
			$cid = $category->getId();
			$response['C']['categories'][$cid] = array(
				'name'  => ($request->get('lang', 'hr') == 'hr')? $category->getName() : $category->getNameEn(),
				'items' => array(),
				'types' => array(),
				'total' => 0
			);
			foreach ($ids as $personID) {
				$person = $doctrine->getRepository('AppBundle:Person')->find($personID);
				$activity = $paRepository->findByPersonCategoryTypeAndPeriod(
					$person->getId(),
					$category->getId(),
					null,
					$period
				);
				if ($activity > 0) {
					$response['C']['categories'][$cid]['total'] += $activity;
					$response['C']['categories'][$cid]['items'][] = array(
						'first_name' => $person->getFirstName(),
						'last_name' => $person->getLastName(),
						'activity'  => $activity
					);
				}
			}
			foreach ($category->getProfessionalActivityTypes() as $type) {
				$tid = $type->getId();
				$response['C']['categories'][$cid]['types'][$tid] = array(
					'name'  => ($request->get('lang', 'hr') == 'hr')? $type->getName() : $type->getNameEn(),
					'items' => array(),
					'total' => 0
				);
				foreach ($ids as $personID) {
					$person = $doctrine->getRepository('AppBundle:Person')->find($personID);
					$activity = $paRepository->findByPersonCategoryTypeAndPeriod(
						$person->getId(),
						$category->getId(),
						$type->getId(),
						$period
					);
					if ($activity > 0) {
						$response['C']['categories'][$cid]['total'] += $activity;
						$response['C']['categories'][$cid]['types'][$tid]['total'] += $activity;
						$response['C']['categories'][$cid]['types'][$tid]['items'][] = array(
							'first_name' => $person->getFirstName(),
							'last_name' => $person->getLastName(),
							'activity'  => $activity
						);
					}
				}
			}
		}

		return $this->render($template, array(
			'filter' => $filter,
			'object' => $response
		));
	}

	/**
	 * Returns the persons mother tongue.
	 *
	 * @param Person $person
	 * @return string
	 */
	protected function getPersonMotherTongue(Person $person)
	{
		$relation = $this
			->getDoctrine()
			->getRepository('AppBundle:PersonLanguage')
			->findOneBy(array(
				'person' => $person,
				'motherTongue' => true
			))
		;

		if ($relation instanceof PersonLanguage) {
			return $relation->getLanguage();
		}

		return null;
	}

	private function getFilterString(array $data, $locale) {
		$response = array();
		$doctrine = $this->get('doctrine');

		if ($data['first_name']) {
			$response[] = $data['first_name'];
		}
		if ($data['last_name']) {
			$response[] = $data['last_name'];
		}
		if ($data['university']) {
			$object = $doctrine->getRepository('AppBundle:University')->find($data['university']);
			if ($locale == 'hr') {
				$response[] = $object->getName();
			} else {
				$response[] = $object->getNameEn();
			}
		}

		if ($data['universityDepartment']) {
			$object = $doctrine->getRepository('AppBundle:UniversityDepartment')->find($data['universityDepartment']);
			if ($locale == 'hr') {
				$response[] = $object->getName();
			} else {
				$response[] = $object->getNameEn();
			}
		}

		if ($data['universityDepartmentDesk']) {
			$object = $doctrine->getRepository('AppBundle:UniversityDepartmentDesk')->find($data['universityDepartmentDesk']);
			if ($locale == 'hr') {
				$response[] = $object->getName();
			} else {
				$response[] = $object->getNameEn();
			}
		}

		if ($data['scientificArea']) {
			$object = $doctrine->getRepository('AppBundle:ScientificArea')->find($data['scientificArea']);
			if ($locale == 'hr') {
				$response[] = $object->getName();
			} else {
				$response[] = $object->getNameEn();
			}
		}

		if ($data['scientificField']) {
			$object = $doctrine->getRepository('AppBundle:ScientificField')->find($data['scientificField']);
			if ($locale == 'hr') {
				$response[] = $object->getName();
			} else {
				$response[] = $object->getNameEn();
			}
		}

		if ($data['scientificBranch']) {
			$object = $doctrine->getRepository('AppBundle:ScientificBranch')->find($data['scientificBranch']);
			if ($locale == 'hr') {
				$response[] = $object->getName();
			} else {
				$response[] = $object->getNameEn();
			}
		}

		if (isset($data['freelancer']) && $data['freelancer']) {
			if ($locale == 'hr') {
				$response[] = 'Prikaži i vanjske suradnike';
			} else {
				$response[] = 'Show external associates';
			}
		}

		if (isset($data['expiration']) && $data['expiration']) {
			if ($locale == 'hr') {
				$response[] = 'Prikaži samo one osobe koje su zadnji put izabrane u zvanje prije 4 ili više godina';
			} else {
				$response[] = 'Show only persons who have been elected for more than 4 years ago';
			}
		}

		if (empty($response)) {
			if ($locale == 'hr') {
				return 'Nema zadanih uvjeta.';
			} else {
				return 'No criteria.';
			}
		}

		return implode('; ', $response);
	}
}
