<?php

namespace AppBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    /**
     * @Route("/{_locale}/impressum", name="app.frontend.default.impressum")
     * @Method({"GET"})
     */
    public function impressumAction()
    {
    	return $this->render('AppBundle:Welcome:impressum.html.twig', array(
    			'back_url' => $this->get('request')->server->get('HTTP_REFERER')
    	));
    }
}
