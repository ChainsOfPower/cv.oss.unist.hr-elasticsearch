<?php

namespace AppBundle\Controller\Administration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\UniversityDepartmentDesk;
use AppBundle\Form\UniversityDepartmentDeskType;

class UniversityDepartmentDeskController extends CoreController
{
    /**
     * @Route("/administration/university-desk", name="app.administration.universitydepartmentdesk.index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:UniversityDepartmentDesk');
        $areas = $repo->findAll();

        return $this->render('AppBundle:Administration/UniversityDepartmentDesk:index.html.twig', array(
            'page_title' => 'Odsjeci/Zavodi',
            'entities' => $areas
        ));
    }

    /**
     * @Route("/administration/university-desk/new", name="app.administration.universitydepartmentdesk.new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $area = new UniversityDepartmentDesk();
        $form = $this->createForm(new UniversityDepartmentDeskType(), $area, array(
            'action' => $this->generateUrl('app.administration.universitydepartmentdesk.new'),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $area, 'app.administration.universitydepartmentdesk.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Novi odsjek/zavod',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/university-desk/{id}/update", name="app.administration.universitydepartmentdesk.update")
     * @Method({"GET", "POST", "PUT"})
     */
    public function updateAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:UniversityDepartmentDesk');
        $area = $repo->find($id);

        if (!($area instanceof UniversityDepartmentDesk)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.universitydepartmentdesk.index')
            );
        }

        $form = $this->createForm(new UniversityDepartmentDeskType(), $area, array(
            'action' => $this->generateUrl('app.administration.universitydepartmentdesk.update', array('id' => $id)),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $area, 'app.administration.universitydepartmentdesk.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Uredi odsjek/zavod',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/university-desk/{id}/delete", name="app.administration.universitydepartmentdesk.delete")
     * @Method({"GET", "POST", "PUT", "DELETE"})
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:UniversityDepartmentDesk');
        $area = $repo->find($id);

        if (!($area instanceof UniversityDepartmentDesk)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.universitydepartmentdesk.index')
            );
        }

        $this->handleRemoveAttempt($area);

        return $this->redirect(
            $this->generateUrl('app.administration.universitydepartmentdesk.index')
        );
    }
}
