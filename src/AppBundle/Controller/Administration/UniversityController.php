<?php

namespace AppBundle\Controller\Administration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\University;
use AppBundle\Form\UniversityType;

class UniversityController extends CoreController
{
    /**
     * @Route("/administration/university", name="app.administration.university.index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:University');
        $areas = $repo->findAll();

        return $this->render('AppBundle:Administration/University:index.html.twig', array(
            'page_title' => 'Sveučilišta',
            'entities' => $areas
        ));
    }

    /**
     * @Route("/administration/university/new", name="app.administration.university.new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $area = new University();
        $form = $this->createForm(new UniversityType(), $area, array(
            'action' => $this->generateUrl('app.administration.university.new'),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $area, 'app.administration.university.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Novo sveučilište',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/university/{id}/update", name="app.administration.university.update")
     * @Method({"GET", "POST", "PUT"})
     */
    public function updateAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:University');
        $area = $repo->find($id);

        if (!($area instanceof University)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.university.index')
            );
        }

        $form = $this->createForm(new UniversityType(), $area, array(
            'action' => $this->generateUrl('app.administration.university.update', array('id' => $id)),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $area, 'app.administration.university.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Uredi sveučilište',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/university/{id}/delete", name="app.administration.university.delete")
     * @Method({"GET", "POST", "PUT", "DELETE"})
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:University');
        $area = $repo->find($id);

        if (!($area instanceof University)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.university.index')
            );
        }

        $this->handleRemoveAttempt($area);

        return $this->redirect(
            $this->generateUrl('app.administration.university.index')
        );
    }
}
