<?php

namespace AppBundle\Controller\Administration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\UniversityDepartment;
use AppBundle\Form\UniversityDepartmentType;

class UniversityDepartmentController extends CoreController
{
    /**
     * @Route("/administration/university-department", name="app.administration.universitydepartment.index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:UniversityDepartment');
        $areas = $repo->findAll();

        return $this->render('AppBundle:Administration/UniversityDepartment:index.html.twig', array(
            'page_title' => 'Sastavnice',
            'entities' => $areas
        ));
    }

    /**
     * @Route("/administration/university-department/new", name="app.administration.universitydepartment.new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $area = new UniversityDepartment();
        $form = $this->createForm(new UniversityDepartmentType(), $area, array(
            'action' => $this->generateUrl('app.administration.universitydepartment.new'),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $area, 'app.administration.universitydepartment.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Nova sastavnica',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/university-department/{id}/update", name="app.administration.universitydepartment.update")
     * @Method({"GET", "POST", "PUT"})
     */
    public function updateAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:UniversityDepartment');
        $area = $repo->find($id);

        if (!($area instanceof UniversityDepartment)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.universitydepartment.index')
            );
        }

        $form = $this->createForm(new UniversityDepartmentType(), $area, array(
            'action' => $this->generateUrl('app.administration.universitydepartment.update', array('id' => $id)),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $area, 'app.administration.universitydepartment.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Uredi sastavnicu',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/university-department/{id}/delete", name="app.administration.universitydepartment.delete")
     * @Method({"GET", "POST", "PUT", "DELETE"})
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:UniversityDepartment');
        $area = $repo->find($id);

        if (!($area instanceof UniversityDepartment)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.universitydepartment.index')
            );
        }

        $this->handleRemoveAttempt($area);

        return $this->redirect(
            $this->generateUrl('app.administration.universitydepartment.index')
        );
    }
}
