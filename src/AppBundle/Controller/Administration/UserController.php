<?php

namespace AppBundle\Controller\Administration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends CoreController
{
    /**
     * @Route("/users", name="app.administration.user.index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Person');
        $users = $repo->findBy(array(), array(
            'lastName' => 'ASC',
            'firstName' => 'ASC'
        ));

        return $this->render('AppBundle:Administration/User:index.html.twig', array(
            'page_title' => 'Korisnici',
            'entities' => $users
        ));
    }
}
