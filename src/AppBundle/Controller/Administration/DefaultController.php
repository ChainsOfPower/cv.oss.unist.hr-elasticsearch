<?php

namespace AppBundle\Controller\Administration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends CoreController
{
    /**
     * @Route("/administration", name="app.administration.default.index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Administration/Default:index.html.twig', array(
            'page_title' => 'Administracija'
        ));
    }
}
