<?php

namespace AppBundle\Controller\Administration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\ScientificBranch;
use AppBundle\Form\ScientificBranchType;

class ScientificBranchController extends CoreController
{
    /**
     * @Route("/administration/scientific/branch", name="app.administration.branch.index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificBranch');
        $branches = $repo->findAll();

        return $this->render('AppBundle:Administration/ScientificBranch:index.html.twig', array(
            'page_title' => 'Znanstvene grane',
            'entities' => $branches
        ));
    }

    /**
     * @Route("/administration/scientific/branch/new", name="app.administration.branch.new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $branch = new ScientificBranch();
        $form = $this->createForm(new ScientificBranchType(), $branch, array(
            'action' => $this->generateUrl('app.administration.branch.new'),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $branch, 'app.administration.branch.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Nova znanstvena grana',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/scientific/branch/{id}/update", name="app.administration.branch.update")
     * @Method({"GET", "POST", "PUT"})
     */
    public function updateAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificBranch');
        $branch = $repo->find($id);

        if (!($branch instanceof ScientificBranch)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.branch.index')
            );
        }

        $form = $this->createForm(new ScientificBranchType(), $branch, array(
            'action' => $this->generateUrl('app.administration.branch.update', array('id' => $id)),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $branch, 'app.administration.branch.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Uredi znanstvenu granu',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/scientific/branch/{id}/delete", name="app.administration.branch.delete")
     * @Method({"GET", "POST", "PUT", "DELETE"})
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificBranch');
        $branch = $repo->find($id);

        if (!($branch instanceof ScientificBranch)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.branch.index')
            );
        }

        $this->handleRemoveAttempt($branch);

        return $this->redirect(
            $this->generateUrl('app.administration.branch.index')
        );
    }
}
