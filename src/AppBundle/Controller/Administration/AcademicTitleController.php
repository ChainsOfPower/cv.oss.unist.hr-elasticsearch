<?php

namespace AppBundle\Controller\Administration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\AcademicTitle;
use AppBundle\Form\AcademicTitleType;

class AcademicTitleController extends CoreController
{
    /**
     * @Route("/administration/academic/title", name="app.administration.academictitle.index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:AcademicTitle');
        $titles = $repo->findAll();

        return $this->render('AppBundle:Administration/AcademicTitle:index.html.twig', array(
            'page_title' => 'Znanstvena-nastavna zvanja',
            'entities' => $titles
        ));
    }

    /**
     * @Route("/administration/academic/title/new", name="app.administration.academictitle.new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $title = new AcademicTitle();
        $form = $this->createForm(new AcademicTitleType(), $title, array(
            'action' => $this->generateUrl('app.administration.academictitle.new'),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $title, 'app.administration.academictitle.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Novo znanstveno-nastavno zvanje',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/academic/title/{id}/update", name="app.administration.academictitle.update")
     * @Method({"GET", "POST", "PUT"})
     */
    public function updateAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:AcademicTitle');
        $title = $repo->find($id);

        if (!($title instanceof AcademicTitle)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.academictitle.index')
            );
        }

        $form = $this->createForm(new AcademicTitleType(), $title, array(
            'action' => $this->generateUrl('app.administration.academictitle.update', array('id' => $id)),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $title, 'app.administration.academictitle.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Uredi znanstveno-nastavno zvanje',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/academic/title/{id}/delete", name="app.administration.academictitle.delete")
     * @Method({"GET", "POST", "PUT", "DELETE"})
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:AcademicTitle');
        $title = $repo->find($id);

        if (!($title instanceof AcademicTitle)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.academictitle.index')
            );
        }

        $this->handleRemoveAttempt($title);

        return $this->redirect(
            $this->generateUrl('app.administration.academictitle.index')
        );
    }
}
