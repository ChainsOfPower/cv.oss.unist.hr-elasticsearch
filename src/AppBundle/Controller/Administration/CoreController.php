<?php

namespace AppBundle\Controller\Administration;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;

class CoreController extends Controller
{
    /**
     * Handles the persist attempt
     * 
     * @param Request $request
     * @param Form $form
     * @param string $redirectRoute
     * @param array $redirectParams
     * @return mixed
     */
    protected function handlePersistAttempt(Request $request, Form &$form, &$entity, $redirectRoute, array $redirectParams = array())
    {
        $method = $request->getMethod();
        if ($method === 'POST' || $method === 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($entity);
                $em->flush();

                return $this->redirectToRoute($redirectRoute, $redirectParams);
            }

            return false;
        }

        return null;
    }

    /**
     * Handles the remove attempt
     *
     * @param mixed $entity
     * @return void
     */
    protected function handleRemoveAttempt(&$entity)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($entity);
        $em->flush();
    }

    /**
     * Returns the current user instance
     *
     * @deprecated use app.user instead
     * @return \AppBundle\Entity\Person
     */
    private function getCurrentPerson()
    {
        return $this->get('security.context')->getToken()->getUser();
    }
}
