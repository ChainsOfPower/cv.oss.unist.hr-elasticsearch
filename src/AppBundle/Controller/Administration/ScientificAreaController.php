<?php

namespace AppBundle\Controller\Administration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\ScientificArea;
use AppBundle\Form\ScientificAreaType;

class ScientificAreaController extends CoreController
{
    /**
     * @Route("/administration/scientific/area", name="app.administration.area.index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificArea');
        $areas = $repo->findAll();

        return $this->render('AppBundle:Administration/ScientificArea:index.html.twig', array(
            'page_title' => 'Znanstvena područja',
            'entities' => $areas
        ));
    }

    /**
     * @Route("/administration/scientific/area/new", name="app.administration.area.new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $area = new ScientificArea();
        $form = $this->createForm(new ScientificAreaType(), $area, array(
            'action' => $this->generateUrl('app.administration.area.new'),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $area, 'app.administration.area.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Novo znanstveno područje',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/scientific/area/{id}/update", name="app.administration.area.update")
     * @Method({"GET", "POST", "PUT"})
     */
    public function updateAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificArea');
        $area = $repo->find($id);

        if (!($area instanceof ScientificArea)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.area.index')
            );
        }

        $form = $this->createForm(new ScientificAreaType(), $area, array(
            'action' => $this->generateUrl('app.administration.area.update', array('id' => $id)),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $area, 'app.administration.area.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Uredi znanstveno područje',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/scientific/area/{id}/delete", name="app.administration.area.delete")
     * @Method({"GET", "POST", "PUT", "DELETE"})
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificArea');
        $area = $repo->find($id);

        if (!($area instanceof ScientificArea)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.area.index')
            );
        }

        $this->handleRemoveAttempt($area);

        return $this->redirect(
            $this->generateUrl('app.administration.area.index')
        );
    }
}
