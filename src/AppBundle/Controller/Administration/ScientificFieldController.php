<?php

namespace AppBundle\Controller\Administration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\ScientificField;
use AppBundle\Form\ScientificFieldType;

class ScientificFieldController extends CoreController
{
    /**
     * @Route("/administration/scientific/field", name="app.administration.field.index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificField');
        $fields = $repo->findAll();

        return $this->render('AppBundle:Administration/ScientificField:index.html.twig', array(
            'page_title' => 'Znanstvena polja',
            'entities' => $fields
        ));
    }

    /**
     * @Route("/administration/scientific/field/new", name="app.administration.field.new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $field = new ScientificField();
        $form = $this->createForm(new ScientificFieldType(), $field, array(
            'action' => $this->generateUrl('app.administration.field.new'),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $field, 'app.administration.field.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Novo znanstveno polje',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/scientific/field/{id}/update", name="app.administration.field.update")
     * @Method({"GET", "POST", "PUT"})
     */
    public function updateAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificField');
        $field = $repo->find($id);

        if (!($field instanceof ScientificField)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.field.index')
            );
        }

        $form = $this->createForm(new ScientificFieldType(), $field, array(
            'action' => $this->generateUrl('app.administration.field.update', array('id' => $id)),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $field, 'app.administration.field.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Uredi znanstveno polje',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/scientific/field/{id}/delete", name="app.administration.field.delete")
     * @Method({"GET", "POST", "PUT", "DELETE"})
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificField');
        $field = $repo->find($id);

        if (!($field instanceof ScientificField)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.field.index')
            );
        }

        $this->handleRemoveAttempt($field);

        return $this->redirect(
            $this->generateUrl('app.administration.field.index')
        );
    }
}
