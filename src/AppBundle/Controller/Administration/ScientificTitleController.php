<?php

namespace AppBundle\Controller\Administration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\ScientificTitle;
use AppBundle\Form\ScientificTitleType;

class ScientificTitleController extends CoreController
{
    /**
     * @Route("/administration/scientific/title", name="app.administration.scientifictitle.index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificTitle');
        $titles = $repo->findAll();

        return $this->render('AppBundle:Administration/ScientificTitle:index.html.twig', array(
            'page_title' => 'Znanstvena zvanja',
            'entities' => $titles
        ));
    }

    /**
     * @Route("/administration/scientific/title/new", name="app.administration.scientifictitle.new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $title = new ScientificTitle();
        $form = $this->createForm(new ScientificTitleType(), $title, array(
            'action' => $this->generateUrl('app.administration.scientifictitle.new'),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $title, 'app.administration.scientifictitle.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Novo znanstveno zvanje',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/scientific/title/{id}/update", name="app.administration.scientifictitle.update")
     * @Method({"GET", "POST", "PUT"})
     */
    public function updateAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificTitle');
        $title = $repo->find($id);

        if (!($title instanceof ScientificTitle)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.scientifictitle.index')
            );
        }

        $form = $this->createForm(new ScientificTitleType(), $title, array(
            'action' => $this->generateUrl('app.administration.scientifictitle.update', array('id' => $id)),
            'method' => 'POST'
        ));

        $attempt = $this->handlePersistAttempt($request, $form, $title, 'app.administration.scientifictitle.index');
        if ($attempt) {
            if ($attempt instanceof RedirectResponse) {
                return $attempt;
            } elseif ($attempt === false) {
                // TODO: handle fail case
            }
        }

        return $this->render('AppBundle:Administration:form.html.twig', array(
            'page_title' => 'Uredi znanstveno zvanje',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/administration/scientific/title/{id}/delete", name="app.administration.scientifictitle.delete")
     * @Method({"GET", "POST", "PUT", "DELETE"})
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:ScientificTitle');
        $title = $repo->find($id);

        if (!($title instanceof ScientificTitle)) {
            return new RedirectResponse(
                $this->generateUrl('app.administration.scientifictitle.index')
            );
        }

        $this->handleRemoveAttempt($title);

        return $this->redirect(
            $this->generateUrl('app.administration.scientifictitle.index')
        );
    }
}
