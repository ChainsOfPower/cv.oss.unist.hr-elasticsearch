$(document).ready(function(){

    // Array of initialized field names
    var init = [];
    // Array of personnel objects
    var list = [];
    // Current serialization result
    var serialized = $('form.filter').serialize();
    // List visibility status
    var listIsVisible = true;

    $('select').select2();

    $(window).on('load resize', function(event){
        $('ul.dropdown-menu').css({ width: $('a.dropdown-toggle').outerWidth() });
    });

    $('body').on('click', '.list-item', function(event){
        // Set person reference
        var person = list[$($(event.target).closest('.list-item')).data('i')];
        var electionDateString = '';
        // Normalize university department and department desk
        if (person.department) person.department = ', ' + person.department;
        if (person.department_desk) person.department_desk = ', ' + person.department_desk;
        // Update modal window
        $('.full-name', '.profile-modal').text(person.pre + person.first_name + " " + person.last_name + person.post);
        $('.data-00 span', '.profile-modal').text(person.university + person.department + person.department_desk);
        if (person.area || person.field) {
            if (person.field) person.field = ', ' + person.field;
            $('.data-01 span', '.profile-modal').text(person.scientific_branch + " (" + person.area + person.field + ")");
        } else {
            $('.data-01 span', '.profile-modal').text(person.scientific_branch);
        }
        if (person.academic_title != null) {
            if (person.election_date != null && person.election_date.date != undefined) {
                electionDate = person.election_date.date.split(' ');
                electionDateParts = electionDate[0].split('-');
                electionDateString = electionDateParts[2] + '.' + electionDateParts[1] + '.' + electionDateParts[0] + '.';
            }
            $('.data-02 span', '.profile-modal').text(person.academic_title + ", " + electionDateString);
        } else {
            $('.data-02 span', '.profile-modal').text("");
        }
        $('.data-03 span', '.profile-modal').text(person.scientific_papers);
        $('.data-04 span', '.profile-modal').text(person.professional_papers);
        $('.data-05 span', '.profile-modal').text(person.mentoring);
        // Define the href attribute for the "Europass CV" and "Competition" buttons
        var europass_href = $('.europass-button', '.profile-modal').data('url').replace('__id__', person.ID);
        var competition_href = $('.competition-button', '.profile-modal').data('url').replace('__id__', person.ID);
        // Update the href attribute of the "Europass CV" and "Competition" buttons
        $('.europass-button', '.profile-modal').attr('href', europass_href);
        $('.competition-button', '.profile-modal').attr('href', competition_href);
        // Update profile picture
        var img = $('<img/>').attr('src', person.profile_picture_large).on('load', function(event){
            $('.ui.image img', '.profile-modal').attr('src', person.profile_picture_large);
            $('.profile-modal').modal('show');
            $(this).remove();
        });
    })

    $('input[type="text"]', 'form.filter').on('input', function(event){
        // Get the parent form element
        var $self = $(this);
        var $form = $($self.closest('form'));
        if (listIsVisible) {
            listIsVisible = false;
            $('#list').fadeTo(400, 0.25);
        }
        window.clearTimeout($self.data("timeout"));
        $self.data("timeout", setTimeout(function(){
            $form.trigger('change');
        }, 1000));
    });

    $('.commands .print').on('click', function(event){
        $('.print-modal').modal('show');
    });

    $('form.filter')
        .on('change', function(event){
            if (init.length <= 4) {
                init.push($(event.target).attr('name'));
            } else {
                if ($('form.filter').serialize() != serialized) {
                    serialized = $('form.filter').serialize();
                    if (listIsVisible) {
                        listIsVisible = false;
                        $('#list').fadeTo(400, 0.25);
                    }
                    $(this).submit();
                }
            }
        })
        .on('submit', function(event){
            event.preventDefault();
            var form = $('form.filter');
            $.ajax({
                method: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                cache: true,
                success: function(response){
                    // Initialize template vars
                    var ids = '';
                    var template = $('#template').html();
                    var rendered = '';
                    // Update personnel array
                    list = response.personnel;
                    // Update list of ids
                    $.each(list, function(index, person){
                        ids += person.ID;
                        // Normalize name prefix
                        if (!person.pre) list[index].pre = "";
                        else list[index].pre = person.pre + " ";
                        // Normalize name postfix
                        if (!person.post) list[index].post = "";
                        else list[index].post = ", " + person.post;
                        // Normalize list
                        if (index < list.length - 1) ids += ',';
                    });
                    // Update links
                    $('[data-base]').each(function(index, btn){
                        var $btn = $(btn);
                        var href = $btn.data('base');
                        // Update query string
                        href += '?ids=' + ids;
                        href += '&lang=' + $btn.data('lang');
                        if ($btn.data('period') != undefined)
                            href += '&period=' + $btn.data('period');
                        href += '&' + $('form.filter').serialize().replace(/%5B%5D/g, '[]');
                        // Update href attribute
                        $btn.attr('href', href);
                    })
                    // Update result counter
                    $('.count').html(response.personnel.length);
                    // Generate and display the result
                    Mustache.tags = ['{|', '|}'];
                    Mustache.parse(template);
                    rendered = Mustache.render(template, response);
                    $('#list').html(rendered);
                    // Update list opacity
                    $('#list').fadeTo(400, 1);
                    listIsVisible = true;
                }
            });
        })
    ;
});
