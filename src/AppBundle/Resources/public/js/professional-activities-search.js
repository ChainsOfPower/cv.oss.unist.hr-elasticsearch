function throttle(f, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
                f.apply(context, args);
            },
            delay || 500);
    };
}

$("#searchForm").on('change keydown', throttle(function(){
    var form = $("#searchForm");
    $.ajax({
        type: "GET",
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data)
        {
            var activities = data;
            $("#results").empty();

            $.each(activities, function(k, v){
                console.log(v);
                $("#results").append('<b>' + firstName + '</b>:<br/>' + (v.firstName == null ? '/' : v.firstName) + '<br/>'
                    + '<b>' + lastName + '</b>:<br/>' + (v.lastName == null ? '/' : v.lastName) + '<br/>'
                    + '<b>' + professionalActivityDescription + '</b>:<br/>' + (v.description == null ? '/' : v.description) + '<br/>'
                    + '<b>' + professionalActivityDescriptionEn + '</b>:<br/>' + (v.descriptionEn == null ? '/' : v.descriptionEn) + '<br/>'
                    + '<b>' + authors  + '</b>:<br/>' + (v.authors == null ? '/' : v.authors) + '<br/>'
                    + '<b>' + date + '</b>:<br/>' + (v.date == null ? '/' : v.date) + '<br/>');

                var routeCV = Routing.generate('app.frontend.search.CV', {id: v.id, lang: $('html').attr('lang')});
                var routeCompetition = Routing.generate('app.frontend.search.competition', {id: v.id, lang: $('html').attr('lang')});

                var buttons = "<div class='row'>" +
                    "<a class='col-md-offset-2 col-md-3' href='" + routeCV + "'><button type='button' class='btn btn-success'>Europass CV</button></a>"
                    + "<a class='col-md-offset-2 col-md-3' href='" + routeCompetition + "'><button type='button' class='btn btn-success'>" + allActivities + "</button></a>"
                    + "</div>";

                $("#results").append(buttons + '<hr/>');

            })
        }
    })
}, 1000));