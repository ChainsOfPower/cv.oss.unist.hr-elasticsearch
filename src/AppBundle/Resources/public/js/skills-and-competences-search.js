function throttle(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
                f.apply(context, args);
            },
            delay || 500);

    };
}

$("#searchForm").on('keydown', throttle(function(){
    var form = $("#searchForm");
    $.ajax({
        type: "GET",
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data)
        {
            var skills = data;
            $("#results").empty();

            $.each(skills, function(k,v){
                console.log(v);
                $("#results").append('<b>'+ firstName +'</b> :<br/>' + (v.firstName == null ? '/' : v.firstName + '<br/>')
                    + '<b>' + lastName + '</b>:<br/>' + (v.lastName == null ? '/' : v.lastName) + '<br/>'
                    + '<b>' + technicalSkills + '</b>:<br/>' + (v.technicalSkills == null ? '/' : v.technicalSkills) + '<br/>'
                    + '<b>' + technicalSkillsEn + '</b>:<br/>' + (v.technicalSkillsEn == null ? '/' : v.technicalSkillsEn) + '<br/>'
                    + '<b>' + socialSkills + '</b>:<br/>' + (v.socialSkills == null ? '/' : v.socialSkills) + '<br/>'
                    + '<b>' + socialSkillsEn + '</b>:<br/>' + (v.socialSkillsEn == null ? '/' : v.socialSkillsEn) + '<br/>'
                    + '<b>' + organizationalSkills + '</b>:<br/>' + (v.organizationalSkills == null ? '/' : v.organizationalSkills) + '<br/>'
                    + '<b>' + organizationalSkillsEn + '</b>:<br/>' + (v.organizationalSkillsEn == null ? '/' : v.organizationalSkillsEn) + '<br/>'
                    + '<b>' + artSkills + '</b>:<br/>' + (v.artSkills == null ? '/' : v.artSkills) + '<br/>'
                    + '<b>' + artSkillsEn + '</b>:<br/>' + (v.artSkillsEn == null ? '/' : v.artSkillsEn) + '<br/>'
                    + '<b>' + miscSkills + '</b>:<br/>' + (v.miscSkills == null ? '/' : v.miscSkills) + '<br/>'
                    + '<b>' + miscSkillsEn + '</b>:<br/>' + (v.miscSkillsEn == null ? '/' : v.miscSkillsEn) + '<br/>');

                var routeCV = Routing.generate('app.frontend.search.CV', {id: v.id});
                var routeCompetition = Routing.generate('app.frontend.search.competition', {id: v.id});

                var buttons = "<div class='row'>" +
                    "<a class='col-md-offset-2 col-md-3' href='" + routeCV + "'><button type='button' class='btn btn-success'>Europass CV</button></a>"
                    + "<a class='col-md-offset-2 col-md-3' href='" + routeCompetition + "'><button type='button' class='btn btn-success'>Znanstvena, nastavna i stručna djelatnost</button></a>"
                    + "</div>";

                $("#results").append(buttons + '<hr/>');

            })
        }
    })
}, 1000));