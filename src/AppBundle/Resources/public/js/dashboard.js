(function ($, window) {

    'use strict';

    window.removeCollectionItem = function(item) {
        $(item)
            .closest('.dodo-collection-removable')
            .html(
                $('<div/>')
                .html('<p>Stavka je uspješno uklonjena. Kako biste spremili izmjene, kliknite na botun "Spremi".</p>')
                .addClass('ui positive message')
                .css({ marginBottom: 15 })
            )
        ;
    }

    $(window).on('load resize', function(event){
        $('ul.dropdown-menu').css({
            width: $('a.dropdown-toggle').outerWidth()
        });
    });

    $(document).ready(function() {

        $('select').select2();

        $('.ui.checkbox').checkbox();

        $('a[data-collection-button="add"]')
            .on('click', function (e) {
                e.preventDefault();

                var self = $(this);
                var collection = $('#' + $(this).data('collection'));
                var prototype = $('#' + $(this).data('prototype')).data('prototype');
                var item = prototype.replace(/__name__/g, collection.children().length);
                var html = "<a style=\"width: 100%;\" href=\"javascript: void(0)\" onclick='removeCollectionItem(this)' class=\"ui inverted red button\" data-collection-button=\"remove\" data-resource=\"" + collection.children().length + "\"><i class=\"fa fa-trash\"></i>&nbsp;Ukloni s popisa</a>";
                var container = $('<div class="dodo-sources-source dodo-collection-removable"/>')

                container.append($('<div class="row-fluid"/>'))
                $('.row-fluid', container).append($('<div class="item"/>'))
                $('.row-fluid .item', container).append($('<div class="field"/>'));
                $('.row-fluid .item > .field', container).append(item)
                $('.row-fluid .item', container).append(html)

                collection.append(container);
                collection.append($('<hr/>'));

                $("html, body").animate({ scrollTop: $(document).height() - $(window).height() }, 500, 'easeInOutQuint');

                self.trigger('collection.item-added');

                return false;
            })
            .on('collection.item-added', function(event) {
                $('select').select2();
            })
        ;

        $('a[data-collection-button="delete"]').on('click', function (e) {
            e.preventDefault();
            removeCollectionItem(this);
        });

    });

})(jQuery, window);
