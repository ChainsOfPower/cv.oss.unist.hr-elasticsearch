$(function() {
    $("#toggle-one").bootstrapToggle({
        on: yes,
        off: no,
        onstyle: 'success'
    });
});

function throttle(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
                f.apply(context, args);
            },
            delay || 500);

    };
}

$("#searchForm").on('change keydown', throttle(function(){
    var form = $("#searchForm");
    $.ajax({
        type: "GET",
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data)
        {
            var skills = data;
            $("#results").empty();

            $.each(skills, function(k,v){
                console.log(v);
                $("#results").append('<b>'+ firstName +'</b> :<br/>' + (v.firstName == null ? '/' : v.firstName) + '<br/>'
                    + '<b>' + lastName + '</b>:<br/>' + (v.lastName == null ? '/' : v.lastName) + '<br/>'
                    + '<b>' + jobTitle + '</b>:<br/>' + (v.jobTitle == null ? '/' : v.jobTitle) + '<br/>'
                    + '<b>' + jobTitleEn + '</b>:<br/>' + (v.jobTitleEn == null ? '/' : v.jobTitleEn) + '<br/>'
                    + '<b>' + area + '</b>:<br/>' + (v.area == null ? '/' : v.area) + '<br/>'
                    + '<b>' + areaEn + '</b>:<br/>' + (v.areaEn == null ? '/' : v.areaEn) + '<br/>'
                    + '<b>' + institution + '</b>:<br/>' + (v.institution == null ? '/' : v.institution) + '<br/>'
                    + '<b>' + institutionEn + '</b>:<br/>' + (v.institutionEn == null ? '/' : v.institutionEn) + '<br/>'
                    + '<b>' + ffunction + '</b>:<br/>' + (v.function == null ? '/' : v.function) + '<br/>'
                    + '<b>' + ffunctionEn + '</b>:<br/>' + (v.functionEn == null ? '/' : v.functionEn) + '<br/>'
                    + '<b>' + dateStart + '</b>:<br/>' + (v.dateStart == null ? '/' : v.dateStart) + '<br/>'
                    + '<b>' + dateEnd + '</b>:<br/>' + (v.dateEnd == null ? '/' : v.dateEnd) + '<br/>');

                var routeCV = Routing.generate('app.frontend.search.CV', {id: v.id, lang: $('html').attr('lang')});
                var routeCompetition = Routing.generate('app.frontend.search.competition', {id: v.id, lang: $('html').attr('lang')});

                var buttons = "<div class='row'>" +
                    "<a class='col-md-offset-2 col-md-3' href='" + routeCV + "'><button type='button' class='btn btn-success'>Europass CV</button></a>"
                    + "<a class='col-md-offset-2 col-md-3' href='" + routeCompetition + "'><button type='button' class='btn btn-success'>" + allActivities + "</button></a>"
                    + "</div>";

                $("#results").append(buttons + '<hr/>');

            })
        }
    })
}, 1000));