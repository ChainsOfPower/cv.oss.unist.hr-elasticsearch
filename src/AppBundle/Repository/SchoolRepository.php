<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Person;
use Doctrine\ORM\EntityRepository;

class SchoolRepository extends EntityRepository
{
    /**
     * Returns last one for provided person
     *
     * @param Person $person [description]
     * @return WorkExperience|null
     */
    public function findLastByPerson(Person $person)
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->select()
            ->innerJoin('s.person', 'p')
            ->where('p = :person')
            ->andWhere('s.highestDegree = :is_highest_degree')
            ->setParameter('person', $person)
            ->setParameter('is_highest_degree', true)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
