<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProfessionalActivityRepository extends EntityRepository {

    public function findByPersonCategoryTypeAndPeriod($person, $category, $type, $period = null) {
        $builder = $this->createQueryBuilder('a');
        $builder
            ->select(array(
                'COUNT(a.id)'
            ))
            ->innerJoin('a.person', 'p')
            ->where('a.person = :person')
            ->setParameter('person', $person)
            ->andWhere('a.professionalActivityCategory = :category')
            ->setParameter('category', $category)
        ;

        if ($type == null) {
            $builder->andWhere('a.professionalActivityType IS NULL');
        } else {
            $builder
                ->andWhere('a.professionalActivityType = :type')
                ->setParameter('type', $type)
            ;
        }

        if ($period !== null) {
            $period = (int)$period;
            $builder
                ->andWhere('a.date >= :date')
                ->setParameter('date', new \DateTime('-' . $period . ' years'))
            ;
        }

        $builder
            ->orderBy('p.lastName', 'ASC')
            ->addOrderBy('p.firstName', 'ASC')
        ;

        return $builder->getQuery()->getSingleScalarResult();
    }

}
