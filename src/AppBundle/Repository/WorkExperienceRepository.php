<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Person;
use Doctrine\ORM\EntityRepository;

class WorkExperienceRepository extends EntityRepository
{
    /**
     * Returns current job for provided person
     *
     * @param Person $person [description]
     * @return WorkExperience|null
     */
    public function findCurrentByPerson(Person $person)
    {
        $qb = $this->createQueryBuilder('w');

        return $qb
            ->select()
            ->innerJoin('w.person', 'p')
            ->where('p = :person')
            ->andWhere('w.current = :is_current')
            ->setParameter('person', $person)
            ->setParameter('is_current', true)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
