<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PersonRepository extends EntityRepository {

    public function findByIdsForList($ids, $lang) {
        $builder = $this->createQueryBuilder('p');
        $postfix = ( $lang == 'en' )? 'En' : '' ;

        $builder
            ->select(array(
                'p.id as ID',
                'p.firstName as first_name',
                'p.lastName as last_name',
                'p.email as email',
                'p.registrationNumber as number',
                'p.scientificTitlePre' . $postfix . ' as before_name',
                'p.scientificTitlePost' . $postfix . ' as after_name',
                'u.name' . $postfix . ' as university',
                'ud.name' . $postfix . ' as university_department',
                'udd.name' . $postfix . ' as university_department_desk',
                'sa.name' . $postfix . ' as scientific_area',
                'sf.name' . $postfix . ' as scientific_field',
                'sb.name' . $postfix . ' as scientific_branch',
                'at.name' . $postfix . ' as academic_title',
                'p.academicTitleDate as election_date',
                '(SELECT count(s_a.id)
                  FROM AppBundle\Entity\ScientificActivity s_a
                  JOIN s_a.scientificActivityCategory s_a_c
                  WHERE p.id = s_a.person AND s_a_c.name LIKE :s_a_c_name
                ) as scientific_papers',
                '(SELECT count(p_a.id)
                  FROM AppBundle\Entity\ProfessionalActivity p_a
                  JOIN p_a.professionalActivityCategory p_a_c
                  WHERE p.id = p_a.person AND (
                    p_a_c.name LIKE :p_a_c_name_1 OR
                    p_a_c.name LIKE :p_a_c_name_2
                  )
              ) as professional_papers',
                '(SELECT count(t_a.id)
                  FROM AppBundle\Entity\TeachingActivity t_a
                  JOIN t_a.teachingActivityCategory t_a_c
                  WHERE p.id = t_a.person AND t_a_c.name LIKE :t_a_c_name
              ) as mentoring',
            ))
            ->leftJoin('p.university', 'u')
            ->leftJoin('p.universityDepartment', 'ud')
            ->leftJoin('p.universityDepartmentDesk', 'udd')
            ->leftJoin('p.scientificArea', 'sa')
            ->leftJoin('p.scientificField', 'sf')
            ->leftJoin('p.scientificBranch', 'sb')
            ->leftJoin('p.academicTitle', 'at')
            ->setParameter('s_a_c_name', 'A.1.%')
            ->setParameter('t_a_c_name', 'B.6.%')
            ->setParameter('p_a_c_name_1', 'C.3.%')
            ->setParameter('p_a_c_name_2', 'C.4.%')
            ->where('p.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('p.lastName', 'ASC')
            ->addOrderBy('p.firstName', 'ASC')
        ;

        return $builder->getQuery()->getResult();
    }

    public function findByQuickSearchParams(
        $firstName = null,
        $lastName = null,
        $university = null,
        $universityDepartment = null,
        $universityDepartmentDesk = null,
        $scientificArea = null,
        $scientificField = null,
        $scientificBranch = null,
        $freelancer = false,
        $expiration = false,
        $locale = 'hr'
    ) {
        $builder = $this->createQueryBuilder('p');
        $postfix = ($locale == 'en')? 'En' : '';

        $builder
            ->select(array(
                'p.id as ID',
                'p.firstName as first_name',
                'p.lastName as last_name',
                'p.email as email',
                'p.hide as hide',
                'p.registrationNumber as number',
                'p.scientificTitlePre' . $postfix . ' as pre',
                'p.scientificTitlePost' . $postfix . ' as post',
                'u.name' . $postfix . ' as university',
                'ud.name' . $postfix . ' as department',
                'sa.name' . $postfix . ' as area',
                'sf.name' . $postfix . ' as field',
                'p.academicTitleDate as election_date',
                'p.profilePicturePath as profile_picture',
                'p.gender as gender',
                'udd.name' . $postfix . ' as department_desk',
                'sb.name' . $postfix . ' as scientific_branch',
                'at.name' . $postfix . ' as academic_title',
                '(SELECT count(s_a.id)
                  FROM AppBundle\Entity\ScientificActivity s_a
                  JOIN s_a.scientificActivityCategory s_a_c
                  WHERE p.id = s_a.person AND s_a_c.name LIKE :s_a_c_name
                ) as scientific_papers',
                '(SELECT count(p_a.id)
                  FROM AppBundle\Entity\ProfessionalActivity p_a
                  JOIN p_a.professionalActivityCategory p_a_c
                  WHERE p.id = p_a.person AND (
                    p_a_c.name LIKE :p_a_c_name_1 OR
                    p_a_c.name LIKE :p_a_c_name_2
                  )
              ) as professional_papers',
                '(SELECT count(t_a.id)
                  FROM AppBundle\Entity\TeachingActivity t_a
                  JOIN t_a.teachingActivityCategory t_a_c
                  WHERE p.id = t_a.person AND t_a_c.name LIKE :t_a_c_name
              ) as mentoring',
            ))
            ->leftJoin('p.university', 'u')
            ->leftJoin('p.universityDepartment', 'ud')
            ->leftJoin('p.universityDepartmentDesk', 'udd')
            ->leftJoin('p.scientificArea', 'sa')
            ->leftJoin('p.scientificField', 'sf')
            ->leftJoin('p.scientificBranch', 'sb')
            ->leftJoin('p.academicTitle', 'at')
            ->setParameter('s_a_c_name', 'A.1.%')
            ->setParameter('t_a_c_name', 'B.6.%')
            ->setParameter('p_a_c_name_1', 'C.3.%')
            ->setParameter('p_a_c_name_2', 'C.4.%')
        ;

        if ($firstName != '' && $firstName != null) {
            $builder
                ->andWhere('p.firstName LIKE :firstName')
                ->setParameter('firstName', $firstName.'%')
            ;
        } else {
            $builder
                ->andWhere('p.firstName != :firstName')
                ->setParameter('firstName', '')
            ;
        }

        if ($lastName != '' && $lastName != null) {
            $builder
                ->andWhere('p.lastName LIKE :lastName')
                ->setParameter('lastName', $lastName.'%')
            ;
        } else {
            $builder
                ->andWhere('p.lastName != :lastName')
                ->setParameter('lastName', '')
            ;
        }

        if ($universityDepartmentDesk !== 0) {
            $builder
                ->andWhere('p.universityDepartmentDesk = :universityDepartmentDesk')
                ->setParameter('universityDepartmentDesk', $universityDepartmentDesk)
            ;
        }

        if ($universityDepartment !== 0) {
            $builder
                ->andWhere('p.universityDepartment = :universityDepartment')
                ->setParameter('universityDepartment', $universityDepartment)
            ;
        }

        if ($university !== 0) {
            $builder
                ->andWhere('p.university = :university')
                ->setParameter('university', $university)
            ;
        }

        if ($scientificArea !== 0) {
            $builder
                ->andWhere('p.scientificArea = :scientificArea')
                ->setParameter('scientificArea', $scientificArea)
            ;
        }

        if ($scientificField !== 0) {
            $builder
                ->andWhere('p.scientificField = :scientificField')
                ->setParameter('scientificField', $scientificField)
            ;
        }

        if ($scientificBranch !== 0) {
            $builder
                ->andWhere('p.scientificBranch = :scientificBranch')
                ->setParameter('scientificBranch', $scientificBranch)
            ;
        }

        if (!$freelancer) {
            $builder
                ->andWhere('p.freelancer = :freelancer')
                ->setParameter('freelancer', false)
            ;
        }

        if ($expiration) {
            $builder
                ->andWhere('p.academicTitleDate <= :date')
                ->setParameter('date', new \DateTime('-4 year'))
            ;
        }

        $builder
            ->andWhere('p.hide != :hide')
            ->setParameter('hide', true)
        ;

        return (
            $builder
                ->orderBy('p.lastName', 'ASC')
                ->addOrderBy('p.firstName', 'ASC')
                ->getQuery()
                ->getResult()
        );
    }
}
