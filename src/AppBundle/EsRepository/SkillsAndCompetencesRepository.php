<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 15.9.2016.
 * Time: 22:34
 */

namespace AppBundle\EsRepository;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use FOS\ElasticaBundle\Repository;
use Elastica\Query;

class SkillsAndCompetencesRepository extends Repository
{
    public function filterAllQuery($socialSkills, $organizationalSkills, $technicalSkills, $artSkills, $miscSkills)
    {
        if(($socialSkills == null || $socialSkills == '')
            && ($organizationalSkills == null || $organizationalSkills == '')
            && ($technicalSkills == null || $technicalSkills == '')
            && ($artSkills == null || $artSkills == '')
            && ($miscSkills == null || $miscSkills == ''))
        {
            return array();
        }

        $boolQuery = new BoolQuery();

        if($socialSkills != null && $socialSkills != '')
        {
            $socialSkillsQuery = new Match();
            $socialSkillsQuery->setFieldQuery('socialSkills', $socialSkills);
            $boolQuery->addShould($socialSkillsQuery);

            $socialSkillsQueryEn = new Match();
            $socialSkillsQueryEn->setFieldQuery('socialSkillsEn', $socialSkills);
            $boolQuery->addShould($socialSkillsQueryEn);
        }

        if($organizationalSkills != null && $organizationalSkills != '')
        {
            $organizationalSkillsQuery = new Match();
            $organizationalSkillsQuery->setFieldQuery('organizationalSkills', $organizationalSkills);
            $boolQuery->addShould($organizationalSkillsQuery);

            $organizationalSkillsQueryEn = new Match();
            $organizationalSkillsQueryEn->setFieldQuery('organizationalSkillsEn', $organizationalSkills);
            $boolQuery->addShould($organizationalSkillsQueryEn);
        }

        if($technicalSkills != null && $technicalSkills != '')
        {
            $technicalSkillsQuery = new Match();
            $technicalSkillsQuery->setFieldQuery('technicalSkills', $technicalSkills);
            $boolQuery->addShould($technicalSkillsQuery);

            $technicalSkillsQueryEn = new Match();
            $technicalSkillsQueryEn->setFieldQuery('technicalSkillsEn', $technicalSkills);
            $boolQuery->addShould($technicalSkillsQueryEn);
        }

        if($artSkills != null && $artSkills != '')
        {
            $artSkillsQuery = new Match();
            $artSkillsQuery->setFieldQuery('artSkills', $artSkills);
            $boolQuery->addShould($artSkillsQuery);

            $artSkillsQueryEn = new Match();
            $artSkillsQueryEn->setFieldQuery('artSkillsEn', $artSkills);
            $boolQuery->addShould($artSkillsQueryEn);
        }

        if($miscSkills != null && $miscSkills != '')
        {
            $miscSkillsQuery = new Match();
            $miscSkillsQuery->setFieldQuery('miscSkills', $miscSkills);
            $boolQuery->addShould($miscSkillsQuery);

            $miscSkillsQueryEn = new Match();
            $miscSkillsQueryEn->setFieldQuery('miscSkillsEn', $miscSkills);
            $boolQuery->addShould($miscSkillsQueryEn);
        }

        $query = new Query($boolQuery);
        $query->setHighlight(array(
            "fields"    =>  array(
                "socialSkills" => new \stdClass(),
                "socialSkillsEn" => new \stdClass(),
                "organizationalSkills" => new \stdClass(),
                "organizationalSkillsEn" => new \stdClass(),
                "technicalSkills" => new \stdClass(),
                "technicalSkillsEn" => new \stdClass(),
                "artSkills" => new \stdClass(),
                "artSkillsEn" => new \stdClass(),
                "miscSkills" => new \stdClass(),
                "miscSkillsEn" => new \stdClass()
            ),
            "fragment_size" => 2000
        ));


        $data = $this->findHybrid($query);


        $allResults = [];

        foreach($data as $d)
        {
            $id = $d->getResult()->getId();
            $hit = $d->getResult()->getHit();

            $transformed = $d->getTransformed();

            if(array_key_exists("socialSkills", $hit["highlight"])){
                $socialSkillsData = $hit["highlight"]["socialSkills"][0];
            } else {
                $socialSkillsData = $hit["_source"]["socialSkills"];
            }

            if(array_key_exists("socialSkillsEn", $hit["highlight"])){
                $socialSkillsEnData = $hit["highlight"]["socialSkillsEn"][0];
            } else {
                $socialSkillsEnData = $hit["_source"]["socialSkillsEn"];
            }

            if(array_key_exists("organizationalSkills", $hit["highlight"])){
                $organizationalSkillsData = $hit["highlight"]["organizationalSkills"][0];
            } else {
                $organizationalSkillsData = $hit["_source"]["organizationalSkills"];
            }

            if(array_key_exists("organizationalSkillsEn", $hit["highlight"])){
                $organizationalSkillsEnData = $hit["highlight"]["organizationalSkillsEn"][0];
            } else {
                $organizationalSkillsEnData = $hit["_source"]["organizationalSkillsEn"];
            }

            if(array_key_exists("technicalSkills", $hit["highlight"])){
                $technicalSkillsData = $hit["highlight"]["technicalSkills"][0];
            } else {
                $technicalSkillsData = $hit["_source"]["technicalSkills"];
            }

            if(array_key_exists("technicalSkillsEn", $hit["highlight"])){
                $technicalSkillsEnData = $hit["highlight"]["technicalSkillsEn"][0];
            } else {
                $technicalSkillsEnData = $hit["_source"]["technicalSkillsEn"];
            }

            if(array_key_exists("artSkills", $hit["highlight"])){
                $artSkillsData = $hit["highlight"]["artSkills"][0];
            } else {
                $artSkillsData = $hit["_source"]["artSkills"];
            }

            if(array_key_exists("artSkillsEn", $hit["highlight"])){
                $artSkillsEnData = $hit["highlight"]["artSkillsEn"][0];
            } else {
                $artSkillsEnData = $hit["_source"]["artSkillsEn"];
            }

            if(array_key_exists("miscSkills", $hit["highlight"])){
                $miscSkillsData = $hit["highlight"]["miscSkills"][0];
            } else {
                $miscSkillsData = $hit["_source"]["miscSkills"];
            }

            if(array_key_exists("miscSkillsEn", $hit["highlight"])){
                $miscSkillsEnData = $hit["highlight"]["miscSkillsEn"][0];
            } else {
                $miscSkillsEnData = $hit["_source"]["miscSkillsEn"];
            }

            $firstName = $transformed->getFirstName();
            $lastName = $transformed->getLastName();


            $allResults[] = array(
                'id'    =>  $id,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'socialSkills' => $socialSkillsData,
                'socialSkillsEn' => $socialSkillsEnData,
                'organizationalSkills' => $organizationalSkillsData,
                'organizationalSkillsEn' => $organizationalSkillsEnData,
                'technicalSkills' => $technicalSkillsData,
                'technicalSkillsEn' => $technicalSkillsEnData,
                'artSkills' => $artSkillsData,
                'artSkillsEn' => $artSkillsEnData,
                'miscSkills' => $miscSkillsData,
                'miscSkillsEn' => $miscSkillsEnData
                );


        }

        return $allResults;

    }
}