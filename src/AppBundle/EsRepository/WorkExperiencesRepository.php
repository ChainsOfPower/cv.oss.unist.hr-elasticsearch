<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 18.9.2016.
 * Time: 20:38
 */

namespace AppBundle\EsRepository;

use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use FOS\ElasticaBundle\Repository;
use Elastica\Query;

class WorkExperiencesRepository extends Repository
{
    public function filterAllQuery($jobTitle, $area, $institution, $function, $current)
    {
        if(($jobTitle == null || $jobTitle == '')
            && ($area == null || $area == '')
            && ($institution == null || $institution == '')
            && ($function == null || $function == ''))
        {
            return array();
        }

        $boolQuery = new BoolQuery();

        if($jobTitle !== null && $jobTitle !== '')
        {
            $jobTitleQuery = new Match();
            $jobTitleQuery->setFieldQuery('jobTitle', $jobTitle);
            $boolQuery->addShould($jobTitleQuery);

            $jobTitleQueryEn = new Match();
            $jobTitleQueryEn->setFieldQuery('jobTitleEn', $jobTitle);
            $boolQuery->addShould($jobTitleQueryEn);
        }

        if($area !== null && $area!== '')
        {
            $areaQuery = new Match();
            $areaQuery->setFieldQuery('area', $area);
            $boolQuery->addShould($areaQuery);

            $areaQueryEn = new Match();
            $areaQueryEn->setFieldQuery('areaEn', $area);
            $boolQuery->addShould($areaQueryEn);
        }

        if($institution !== null && $institution !== '')
        {
            $institutionQuery = new Match();
            $institutionQuery->setFieldQuery('institution', $institution);
            $boolQuery->addShould($institutionQuery);

            $institutionQueryEn = new Match();
            $institutionQueryEn->setFieldQuery('institutionEn', $institution);
            $boolQuery->addShould($institutionQueryEn);
        }

        if($function !== null && $institution !== '')
        {
            $functionQuery = new Match();
            $functionQuery->setFieldQuery('function', $function);
            $boolQuery->addShould($functionQuery);

            $functionQueryEn = new Match();
            $functionQueryEn->setFieldQuery('functionEn', $function);
            $boolQuery->addShould($functionQueryEn);
        }
        
        $query = new Query($boolQuery);
        $query->setHighlight(array(
            "fields"    =>  array(
                "jobTitle"      => new \stdClass(),
                "jobTitleEn"    => new \stdClass(),
                "area"          => new \stdClass(),
                "areaEn"        => new \stdClass(),
                "institution"   => new \stdClass(),
                "institutionEn" => new \stdClass(),
                "function"      => new \stdClass(),
                "functionEn"    => new \stdClass()
            ),
            "fragment_size" => 2000
        ));
        if($current === true)
        {
            $filter = new \Elastica\Filter\Term();
            $filter->setTerm('current', true);
            $query->setPostFilter($filter);
        }

        $data = $this->findHybrid($query);



        $allResults = [];

        foreach($data as $d)
        {
            $hit = $d->getResult()->getHit();
            $transformed = $d->getTransformed();

            $id = $transformed->getPerson()->getId();


            if(array_key_exists("jobTitle", $hit["highlight"])){
                $jobTitleData = $hit["highlight"]["jobTitle"][0];
            } else {
                $jobTitleData = $hit["_source"]["jobTitle"];
            }

            if(array_key_exists("jobTitleEn", $hit["highlight"])){
                $jobTitleDataEn = $hit["highlight"]["jobTitleEn"][0];
            } else {
                $jobTitleDataEn = $hit["_source"]["jobTitleEn"];
            }

            if(array_key_exists("area", $hit["highlight"])){
                $areaData = $hit["highlight"]["area"][0];
            } else {
                $areaData = $hit["_source"]["area"];
            }

            if(array_key_exists("areaEn", $hit["highlight"])){
                $areaDataEn = $hit["highlight"]["areaEn"][0];
            } else {
                $areaDataEn = $hit["_source"]["areaEn"];
            }

            if(array_key_exists("institution", $hit["highlight"])){
                $institutionData = $hit["highlight"]["institution"][0];
            } else {
                $institutionData = $hit["_source"]["institution"];
            }

            if(array_key_exists("institutionEn", $hit["highlight"])){
                $institutionDataEn = $hit["highlight"]["institutionEn"][0];
            } else {
                $institutionDataEn = $hit["_source"]["institutionEn"];
            }

            if(array_key_exists("function", $hit["highlight"])){
                $functionData = $hit["highlight"]["function"][0];
            } else {
                $functionData = $hit["_source"]["function"];
            }

            if(array_key_exists("functionEn", $hit["highlight"])){
                $functionDataEn = $hit["highlight"]["functionEn"][0];
            } else {
                $functionDataEn = $hit["_source"]["functionEn"];
            }

            $dateStartData = $hit["_source"]["dateFromEs"];
            $dateEndData = $hit["_source"]["dateToEs"];

            $firstName = $transformed->getPerson()->getFirstName();
            $lastName = $transformed->getPerson()->getLastName();

            $allResults[] = array(
                'id'            =>  $id,
                'firstName'     =>  $firstName,
                'lastName'      => $lastName,
                'jobTitle'      => $jobTitleData,
                'jobTitleEn'    => $jobTitleDataEn,
                'area'          => $areaData,
                'areaEn'        => $areaDataEn,
                'institution'   => $institutionData,
                'institutionEn' => $institutionDataEn,
                'function'      => $functionData,
                'functionEn'    => $functionDataEn,
                'dateStart'     => $dateStartData,
                'dateEnd'       => $dateEndData
            );
        }

        return $allResults;
    }
}