<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 18.9.2016.
 * Time: 3:26
 */

namespace AppBundle\EsRepository;

use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use FOS\ElasticaBundle\Repository;
use Elastica\Query;

class ProfessionalActivitiesRepository extends Repository
{
    public function filterAllQuery($description, $authors, $date)
    {
        if(($description == null || $description == '') && ($authors == null || $authors == ''))
        {
            return array();
        }

        $boolQuery = new BoolQuery();

        if($description != null && $description != '')
        {
            $descriptionQuery = new Match();
            $descriptionQuery->setFieldQuery('description', $description);
            $boolQuery->addShould($descriptionQuery);

            $descriptionQueryEn = new Match();
            $descriptionQueryEn->setFieldQuery('descriptionEn', $description);
            $boolQuery->addShould($descriptionQueryEn);
        }

        if($authors != null && $authors != '')
        {
            $authorsQuery = new Match();
            $authorsQuery->setFieldQuery('authors', $authors);
            $boolQuery->addShould($authorsQuery);
        }


        $query = new Query($boolQuery);

        if($date != null)
        {
            $filter = new \Elastica\Filter\Range('dateEs', array(
                'gte' => $date->format('Y-m-d')
            ));

            $query->setPostFilter($filter);
        }

        $query->setHighlight(array(
            "fields"    =>  array(
                "authors"       =>  new \stdClass(),
                "description"   =>  new \stdClass(),
                "descriptionEn" =>  new \stdClass()
            ),
            "fragment_size" => 2000
        ));

        $data = $this->findHybrid($query);

        $allResults = [];

        foreach($data as $d)
        {
            $hit = $d->getResult()->getHit();
            $transformed = $d->getTransformed();

            $id = $transformed->getPerson()->getId();

            if(array_key_exists("description", $hit["highlight"])){
                $descriptionData = $hit["highlight"]["description"][0];
            } else {
                $descriptionData = $hit["_source"]["description"];
            }

            if(array_key_exists("descriptionEn", $hit["highlight"])){
                $descriptionDataEn = $hit["highlight"]["descriptionEn"][0];
            } else {
                $descriptionDataEn = $hit["_source"]["descriptionEn"];
            }

            if(array_key_exists("authors", $hit["highlight"])){
                $authorsData = $hit["highlight"]["authors"][0];
            } else {
                $authorsData = $hit["_source"]["authors"];
            }

            $dateData = $hit["_source"]["dateEs"];

            $firstName = $transformed->getPerson()->getFirstName();
            $lastName = $transformed->getPerson()->getLastName();

            $allResults[] = array(
                'id'            =>  $id,
                'firstName'     =>  $firstName,
                'lastName'      =>  $lastName,
                'description'   =>  $descriptionData,
                'descriptionEn' =>  $descriptionDataEn,
                'authors'       =>  $authorsData,
                'date'          =>  $dateData
            );
        }

        return $allResults;
    }
}