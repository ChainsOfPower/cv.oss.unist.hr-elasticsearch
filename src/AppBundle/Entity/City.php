<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_city")
 */
class City
{
    // ====
    // Data
    // ====

    /**
     * Area code
     * @ORM\Column(name="city_area_code", type="integer")
     * @ORM\Id
     * @var int
     */
    protected $areaCode;

    /**
     * Ime grada
     * @ORM\Column(name="city_name", type="string", length=128, nullable=false)
     * @var string
     */
    protected $name;

    // =========
    // Relations
    // =========

    /**
     * Osobe vezane uz grad
     * @ORM\OneToMany(targetEntity="Person", mappedBy="city")
     * @var ArrayCollection
     */
    protected $persons;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set areaCode
     *
     * @param integer $areaCode
     * @return City
     */
    public function setAreaCode($areaCode)
    {
        $this->areaCode = $areaCode;

        return $this;
    }

    /**
     * Get areaCode
     *
     * @return integer
     */
    public function getAreaCode()
    {
        return $this->areaCode;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add persons
     *
     * @param \AppBundle\Entity\Person $persons
     * @return City
     */
    public function addPerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons[] = $persons;

        return $this;
    }

    /**
     * Remove persons
     *
     * @param \AppBundle\Entity\Person $persons
     */
    public function removePerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons->removeElement($persons);
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name . ', ' . $this->areaCode;
    }
}
