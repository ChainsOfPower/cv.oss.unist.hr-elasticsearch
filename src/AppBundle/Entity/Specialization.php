<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_specialization")
 */
class Specialization
{
    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Datum i godina specijalizacije
     * @ORM\Column(name="specialization_date", type="date", nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var DateTime
     */
    protected $date;

    /**
     * Lokacija specijalizacije
     * @ORM\Column(name="specialization_location", type="string", length=128, nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $location;

    /**
     * Lokacija specijalizacije
     * @ORM\Column(name="specialization_location_en", type="string", length=128, nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $locationEn;

    /**
     * Lokacija specijalizacije
     * @ORM\Column(name="specialization_institution", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $institution;

    /**
     * Lokacija specijalizacije
     * @ORM\Column(name="specialization_institution_en", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $institutionEn;

    /**
     * Podrucje specijalizacije
     * @ORM\Column(name="specialization_area", type="string", length=128, nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $specializationArea;

    /**
     * Podrucje specijalizacije
     * @ORM\Column(name="specialization_area_en", type="string", length=128, nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $specializationAreaEn;

    /**
     * Osoba
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="specialization")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     * @var Person
     */
    protected $person;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Specialization
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Specialization
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set locationEn
     *
     * @param string $location
     * @return Specialization
     */
    public function setLocationEn($location)
    {
        $this->locationEn = $location;

        return $this;
    }

    /**
     * Get locationEn
     *
     * @return string
     */
    public function getLocationEn()
    {
        return $this->locationEn;
    }

    /**
     * Set institution
     *
     * @param string $institution
     * @return Specialization
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get institution
     *
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set institutionEn
     *
     * @param string $institution
     * @return Specialization
     */
    public function setInstitutionEn($institution)
    {
        $this->institutionEn = $institution;

        return $this;
    }

    /**
     * Get institutionEn
     *
     * @return string
     */
    public function getInstitutionEn()
    {
        return $this->institutionEn;
    }

    /**
     * Set specializationArea
     *
     * @param string $specializationArea
     * @return Specialization
     */
    public function setSpecializationArea($specializationArea)
    {
        $this->specializationArea = $specializationArea;

        return $this;
    }

    /**
     * Get specializationArea
     *
     * @return string
     */
    public function getSpecializationArea()
    {
        return $this->specializationArea;
    }

    /**
     * Set specializationAreaEn
     *
     * @param string $specializationArea
     * @return Specialization
     */
    public function setSpecializationAreaEn($specializationArea)
    {
        $this->specializationAreaEn = $specializationArea;

        return $this;
    }

    /**
     * Get specializationAreaEn
     *
     * @return string
     */
    public function getSpecializationAreaEn()
    {
        return $this->specializationAreaEn;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Person $person
     * @return Specialization
     */
    public function setPerson(\AppBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }
}
