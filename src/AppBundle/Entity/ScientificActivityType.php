<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_scientific_activity_type")
 */
class ScientificActivityType
{
    // ====
    // Data
    // ====

    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Autori
     * @ORM\Column(name="scientific_activity_type_name", type="string", length=128, nullable=true)
     * @var string
     */
    protected $name;

    /**
     * Autori
     * @ORM\Column(name="scientific_activity_type_name_en", type="string", length=128, nullable=true)
     * @var string
     */
    protected $nameEn;

    // =========
    // Relations
    // =========

    /**
     * Referenca na krategoriju znanstvene djelatnosti
     * @ORM\ManyToOne(targetEntity="ScientificActivityCategory", inversedBy="scientificActivityTypes")
     * @ORM\JoinColumn(name="scientific_activity_category_id", referencedColumnName="id", nullable=true)
     * @var ScientificActivityCategory
     */
    protected $scientificActivityCategory;

    /**
     * Lista znanstvenih aktivnosti
     * @ORM\OneToMany(targetEntity="ScientificActivity", mappedBy="scientificActivityType", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $scientificActivities;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scientificActivities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScientificActivityType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return ScientificActivityCategory
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set scientificActivityCategory
     *
     * @param \AppBundle\Entity\ScientificActivityCategory $scientificActivityCategory
     * @return ScientificActivityType
     */
    public function setScientificActivityCategory(\AppBundle\Entity\ScientificActivityCategory $scientificActivityCategory = null)
    {
        $this->scientificActivityCategory = $scientificActivityCategory;

        return $this;
    }

    /**
     * Get scientificActivityCategory
     *
     * @return \AppBundle\Entity\ScientificActivityCategory
     */
    public function getScientificActivityCategory()
    {
        return $this->scientificActivityCategory;
    }

    /**
     * Add scientificActivities
     *
     * @param \AppBundle\Entity\ScientificActivity $scientificActivities
     * @return ScientificActivityType
     */
    public function addScientificActivity(\AppBundle\Entity\ScientificActivity $scientificActivities)
    {
        $this->scientificActivities[] = $scientificActivities;

        return $this;
    }

    /**
     * Remove scientificActivities
     *
     * @param \AppBundle\Entity\ScientificActivity $scientificActivities
     */
    public function removeScientificActivity(\AppBundle\Entity\ScientificActivity $scientificActivities)
    {
        $this->scientificActivities->removeElement($scientificActivities);
    }

    /**
     * Get scientificActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScientificActivities()
    {
        return $this->scientificActivities;
    }

    public function __toString()
    {
        return $this->name;
    }
}
