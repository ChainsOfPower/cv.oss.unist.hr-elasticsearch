<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_scientific_area")
 */
class ScientificArea
{
    // ====
    // Data
    // ====

    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Naziv znanstvenog podrucja
     * @ORM\Column(name="scientific_area_name", type="string", length=128, nullable=false)
     * @var string
     */
    protected $name;

    /**
     * Naziv znanstvenog podrucja
     * @ORM\Column(name="scientific_area_name_en", type="string", length=128, nullable=false)
     * @var string
     */
    protected $nameEn;

    // =========
    // Relations
    // =========

    /**
     * Lista znanstvenih polja
     * @ORM\OneToMany(targetEntity="ScientificField", mappedBy="scientificArea")
     * @var ArrayCollection
     */
    protected $scientificFields;

    /**
     * Lista osoba
     * @ORM\OneToMany(targetEntity="Person", mappedBy="scientificArea")
     * @var ArrayCollection
     */
    protected $persons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scientificFields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScientificArea
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $name
     * @return University
     */
    public function setNameEn($name)
    {
        $this->nameEn = $name;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Add scientificFields
     *
     * @param \AppBundle\Entity\ScientificField $scientificFields
     * @return ScientificArea
     */
    public function addScientificField(\AppBundle\Entity\ScientificField $scientificFields)
    {
        $this->scientificFields[] = $scientificFields;

        return $this;
    }

    /**
     * Remove scientificFields
     *
     * @param \AppBundle\Entity\ScientificField $scientificFields
     */
    public function removeScientificField(\AppBundle\Entity\ScientificField $scientificFields)
    {
        $this->scientificFields->removeElement($scientificFields);
    }

    /**
     * Get scientificFields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScientificFields()
    {
        return $this->scientificFields;
    }

    /**
     * Add persons
     *
     * @param \AppBundle\Entity\Person $persons
     * @return ScientificBranch
     */
    public function addPerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons[] = $persons;

        return $this;
    }

    /**
     * Remove persons
     *
     * @param \AppBundle\Entity\Person $persons
     */
    public function removePerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons->removeElement($persons);
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons()
    {
        return $this->persons;
    }

    public function __toString()
    {
        return $this->name;
    }
}
