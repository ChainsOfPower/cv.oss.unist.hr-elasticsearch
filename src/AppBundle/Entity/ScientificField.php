<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_scientific_field")
 */
class ScientificField
{
    // ====
    // Data
    // ====

    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Naziv znanstvenog polja
     * @ORM\Column(name="scientific_field_name", type="string", length=128, nullable=false)
     * @var string
     */
    protected $name;

    /**
     * Naziv znanstvene grane
     * @ORM\Column(name="scientific_field_name_en", type="string", length=128, nullable=false)
     * @var string
     */
    protected $nameEn;

    // =========
    // Relations
    // =========

    /**
     * Referenca na znanstveno podrucje
     * @ORM\ManyToOne(targetEntity="ScientificArea", inversedBy="scientificFields")
     * @ORM\JoinColumn(name="scientific_area_id", referencedColumnName="id", onDelete="CASCADE")
     * @var ScientificArea
     */
    protected $scientificArea;

    /**
     * Lista znanstvenih grana
     * @ORM\OneToMany(targetEntity="ScientificBranch", mappedBy="scientificField")
     * @var ArrayCollection
     */
    protected $scientificBranches;

    /**
     * Lista osoba
     * @ORM\OneToMany(targetEntity="Person", mappedBy="scientificField")
     * @var ArrayCollection
     */
    protected $persons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scientificBranches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScientificField
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $name
     * @return University
     */
    public function setNameEn($name)
    {
        $this->nameEn = $name;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set scientificArea
     *
     * @param \AppBundle\Entity\ScientificArea $scientificArea
     * @return ScientificField
     */
    public function setScientificArea(\AppBundle\Entity\ScientificArea $scientificArea = null)
    {
        $this->scientificArea = $scientificArea;

        return $this;
    }

    /**
     * Get scientificArea
     *
     * @return \AppBundle\Entity\ScientificArea
     */
    public function getScientificArea()
    {
        return $this->scientificArea;
    }

    /**
     * Add scientificBranches
     *
     * @param \AppBundle\Entity\ScientificBranch $scientificBranches
     * @return ScientificField
     */
    public function addScientificBranch(\AppBundle\Entity\ScientificBranch $scientificBranches)
    {
        $this->scientificBranches[] = $scientificBranches;

        return $this;
    }

    /**
     * Remove scientificBranches
     *
     * @param \AppBundle\Entity\ScientificBranch $scientificBranches
     */
    public function removeScientificBranch(\AppBundle\Entity\ScientificBranch $scientificBranches)
    {
        $this->scientificBranches->removeElement($scientificBranches);
    }

    /**
     * Get scientificBranches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScientificBranches()
    {
        return $this->scientificBranches;
    }

    /**
     * Add persons
     *
     * @param \AppBundle\Entity\Person $persons
     * @return ScientificBranch
     */
    public function addPerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons[] = $persons;

        return $this;
    }

    /**
     * Remove persons
     *
     * @param \AppBundle\Entity\Person $persons
     */
    public function removePerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons->removeElement($persons);
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons()
    {
        return $this->persons;
    }

    public function __toString()
    {
        return $this->name;
    }
}
