<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SchoolRepository")
 * @ORM\Table(name="cv_school")
 */
class School
{
    /**
     * Identifikator
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Zvanje
     *
     * @ORM\Column(name="school_title", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $title;

    /**
     * Zvanje
     *
     * @ORM\Column(name="school_title_en", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $titleEn;

    /**
     * Institucija
     *
     * @ORM\Column(name="school_institution", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $institution;

    /**
     * Institucija
     *
     * @ORM\Column(name="school_institution_en", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $institutionEn;

    /**
     * Lokacija ustanove
     *
     * @ORM\Column(name="school_location", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $location;

    /**
     * Lokacija ustanove
     *
     * @ORM\Column(name="school_location_en", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $locationEn;

    /**
     * Datum zavrsetka
     *
     * @ORM\Column(name="school_date", type="date", nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var DateTime
     */
    protected $date;

    /**
     * Da li je riječ o najvišem postignutom stupnju? (da/ne)
     *
     * @ORM\Column(name="school_highest_degree", type="boolean", nullable=true)
     * @var boolean
     */
    protected $highestDegree;

    /**
     * Osoba
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="schooling")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     * @var Person
     */
    protected $person;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return School
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set titleEn
     *
     * @param string $title
     * @return School
     */
    public function setTitleEn($title)
    {
        $this->titleEn = $title;

        return $this;
    }

    /**
     * Get titleEn
     *
     * @return string
     */
    public function getTitleEn()
    {
        return $this->titleEn;
    }

    /**
     * Set institution
     *
     * @param string $institution
     * @return School
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get institution
     *
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set institutionEn
     *
     * @param string $institutionEn
     * @return School
     */
    public function setInstitutionEn($institutionEn)
    {
        $this->institutionEn = $institutionEn;

        return $this;
    }

    /**
     * Get institutionEn
     *
     * @return string
     */
    public function getInstitutionEn()
    {
        return $this->institutionEn;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return School
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set locationEn
     *
     * @param string $location
     * @return School
     */
    public function setLocationEn($location)
    {
        $this->locationEn = $location;

        return $this;
    }

    /**
     * Get locationEn
     *
     * @return string
     */
    public function getLocationEn()
    {
        return $this->locationEn;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return School
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Person $person
     * @return School
     */
    public function setPerson(\AppBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set highestDegree
     *
     * @param boolean $highestDegree
     */
    public function setHighestDegree($highestDegree)
    {
        $this->highestDegree = $highestDegree;

        return $this;
    }

    /**
     * Get highestDegree
     *
     * @return boolean
     */
    public function isHighestDegree()
    {
        return $this->highestDegree;
    }
}
