<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_teaching_activity_type")
 */
class TeachingActivityType
{
    // ====
    // Data
    // ====

    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Autori
     * @ORM\Column(name="teaching_activity_type_name", type="string", length=128, nullable=true)
     * @var string
     */
    protected $name;

    /**
     * Autori
     * @ORM\Column(name="teaching_activity_type_name_en", type="string", length=128, nullable=true)
     * @var string
     */
    protected $nameEn;

    // =========
    // Relations
    // =========

    /**
     * Referenca na krategoriju nastavne djelatnosti
     * @ORM\ManyToOne(targetEntity="TeachingActivityCategory", inversedBy="teachingActivityTypes")
     * @ORM\JoinColumn(name="teaching_activity_category_id", referencedColumnName="id", nullable=true)
     * @var TeachingActivityCategory
     */
    protected $teachingActivityCategory;

    /**
     * Lista nastavnih aktivnosti
     * @ORM\OneToMany(targetEntity="TeachingActivity", mappedBy="teachingActivityType", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $teachingActivities;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teachingActivities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TeachingActivityType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return ScientificActivityCategory
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set teachingActivityCategory
     *
     * @param \AppBundle\Entity\TeachingActivityCategory $teachingActivityCategory
     * @return TeachingActivityType
     */
    public function setTeachingActivityCategory(\AppBundle\Entity\TeachingActivityCategory $teachingActivityCategory = null)
    {
        $this->teachingActivityCategory = $teachingActivityCategory;

        return $this;
    }

    /**
     * Get teachingActivityCategory
     *
     * @return \AppBundle\Entity\TeachingActivityCategory
     */
    public function getTeachingActivityCategory()
    {
        return $this->teachingActivityCategory;
    }

    /**
     * Add teachingActivities
     *
     * @param \AppBundle\Entity\TeachingActivity $teachingActivities
     * @return TeachingActivityType
     */
    public function addTeachingActivity(\AppBundle\Entity\TeachingActivity $teachingActivities)
    {
        $this->teachingActivities[] = $teachingActivities;

        return $this;
    }

    /**
     * Remove teachingActivities
     *
     * @param \AppBundle\Entity\TeachingActivity $teachingActivities
     */
    public function removeTeachingActivity(\AppBundle\Entity\TeachingActivity $teachingActivities)
    {
        $this->teachingActivities->removeElement($teachingActivities);
    }

    /**
     * Get teachingActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeachingActivities()
    {
        return $this->teachingActivities;
    }

    public function __toString()
    {
        return $this->name;
    }
}
