<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeachingActivityRepository")
 * @ORM\Table(name="cv_teaching_activity")
 */
class TeachingActivity
{
    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Autori
     * @ORM\Column(name="teaching_activity_authors", type="string", length=256, nullable=false)
     * @var string
     */
    protected $authors;

    /**
     * Datum
     *
     * @ORM\Column(name="teaching_activity_date", type="date", nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno - služi za sortiranje.")
     * @var DateTime
     */
    protected $date;

    /**
     * Opis (sadrzaj)
     * @ORM\Column(name="teaching_activity_description", type="text", nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $description;

    /**
     * Opis (sadrzaj)
     * @ORM\Column(name="teaching_activity_description_en", type="text", nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $descriptionEn;

    /**
     * Referenca na osobu
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="teachingActivities")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", nullable=false)
     * @var Person
     */
    protected $person;

    /**
     * Referenca na krategoriju nastacne djelatnosti
     * @ORM\ManyToOne(targetEntity="TeachingActivityCategory", inversedBy="teachingActivities")
     * @ORM\JoinColumn(name="teaching_activity_category_id", referencedColumnName="id", nullable=true)
     * @Assert\NotNull(message = "Potrebno je odabrati kategoriju.")
     * @var TeachingActivityCategory
     */
    protected $teachingActivityCategory;

    /**
     * Referenca na tip nastacne djelatnosti
     * @ORM\ManyToOne(targetEntity="TeachingActivityType", inversedBy="teachingActivities")
     * @ORM\JoinColumn(name="teaching_activity_type_id", referencedColumnName="id", nullable=true)
     * @var TeachingActivityType
     */
    protected $teachingActivityType;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set authors
     *
     * @param string $authors
     * @return TeachingActivity
     */
    public function setAuthors($authors)
    {
        $this->authors = $authors;

        return $this;
    }

    /**
     * Get authors
     *
     * @return string
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ScientificActivity
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get date in string format for elasticsearch indexing
     *
     * @return string
     */
    public function getDateEs()
    {
        return $this->getDate()->format('Y-m-d');
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TeachingActivity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set descriptionEn
     *
     * @param string $description
     * @return ProfessionalActivity
     */
    public function setDescriptionEn($description)
    {
        $this->descriptionEn = $description;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->descriptionEn;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Person $person
     * @return TeachingActivity
     */
    public function setPerson(\AppBundle\Entity\Person $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set teachingActivityCategory
     *
     * @param \AppBundle\Entity\TeachingActivityCategory $teachingActivityCategory
     * @return TeachingActivity
     */
    public function setTeachingActivityCategory(\AppBundle\Entity\TeachingActivityCategory $teachingActivityCategory = null)
    {
        $this->teachingActivityCategory = $teachingActivityCategory;

        return $this;
    }

    /**
     * Get teachingActivityCategory
     *
     * @return \AppBundle\Entity\TeachingActivityCategory
     */
    public function getTeachingActivityCategory()
    {
        return $this->teachingActivityCategory;
    }

    /**
     * Set teachingActivityType
     *
     * @param \AppBundle\Entity\TeachingActivityType $teachingActivityType
     * @return TeachingActivity
     */
    public function setTeachingActivityType(\AppBundle\Entity\TeachingActivityType $teachingActivityType = null)
    {
        $this->teachingActivityType = $teachingActivityType;

        return $this;
    }

    /**
     * Get teachingActivityType
     *
     * @return \AppBundle\Entity\TeachingActivityType
     */
    public function getTeachingActivityType()
    {
        return $this->teachingActivityType;
    }
}
