<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_university_department_desk")
 */
class UniversityDepartmentDesk
{
    /**
     * Identifikator
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * Naziv
     *
     * @ORM\Column(name="university_department_desk_name", type="string", length=128, nullable=false)
     * @var string
     */
    private $name;

    /**
     * Naziv
     *
     * @ORM\Column(name="university_department_desk_name_en", type="string", length=128, nullable=false)
     * @var string
     */
    private $nameEn;

    /**
     * Kontakt informacije
     *
     * @ORM\Column(name="university_department_desk_contact", type="text", nullable=true)
     * @var string
     */
    private $contact;

    /**
     * Referenca na sastavnicu
     *
     * @ORM\ManyToOne(targetEntity="UniversityDepartment", inversedBy="desks")
     * @ORM\JoinColumn(name="university_department_id", referencedColumnName="id", onDelete="CASCADE")
     * @var ScientificArea
     */
    protected $universityDepartment;

    /**
     * Povezane osobe
     *
     * @ORM\OneToMany(targetEntity="Person", mappedBy="universityDepartmentDesk")
     * @var ArrayCollection
     */
    protected $persons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UniversityDepartmentDesk
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $name
     * @return UniversityDepartmentDesk
     */
    public function setNameEn($name)
    {
        $this->nameEn = $name;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return UniversityDepartmentDesk
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set universityDepartment
     *
     * @param \AppBundle\Entity\UniversityDepartment $universityDepartment
     * @return UniversityDepartmentDesk
     */
    public function setUniversityDepartment(\AppBundle\Entity\UniversityDepartment $universityDepartment = null)
    {
        $this->universityDepartment = $universityDepartment;

        return $this;
    }

    /**
     * Get universityDepartment
     *
     * @return \AppBundle\Entity\UniversityDepartment
     */
    public function getUniversityDepartment()
    {
        return $this->universityDepartment;
    }

    /**
     * Add persons
     *
     * @param \AppBundle\Entity\Person $persons
     * @return UniversityDepartmentDesk
     */
    public function addPerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons[] = $persons;

        return $this;
    }

    /**
     * Remove persons
     *
     * @param \AppBundle\Entity\Person $persons
     */
    public function removePerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons->removeElement($persons);
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}
