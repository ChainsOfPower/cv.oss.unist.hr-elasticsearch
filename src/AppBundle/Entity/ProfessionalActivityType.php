<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_professional_activity_type")
 */
class ProfessionalActivityType
{
    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Autori
     * @ORM\Column(name="professional_activity_type_name", type="string", length=256, nullable=true)
     * @var string
     */
    protected $name;

    /**
     * Autori
     * @ORM\Column(name="professional_activity_type_name_en", type="string", length=256, nullable=true)
     * @var string
     */
    protected $nameEn;

    /**
     * Referenca na krategoriju znanstvene djelatnosti
     * @ORM\ManyToOne(targetEntity="ProfessionalActivityCategory", inversedBy="professionalActivityTypes")
     * @ORM\JoinColumn(name="professional_activity_category_id", referencedColumnName="id", nullable=true)
     * @var ProfessionalActivityCategory
     */
    protected $professionalActivityCategory;

    /**
     * Lista znanstvenih aktivnosti
     * @ORM\OneToMany(targetEntity="ProfessionalActivity", mappedBy="professionalActivityType", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $professionalActivities;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->professionalActivities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProfessionalActivityType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return ScientificActivityCategory
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set professionalActivityCategory
     *
     * @param \AppBundle\Entity\ProfessionalActivityCategory $professionalActivityCategory
     * @return ProfessionalActivityType
     */
    public function setProfessionalActivityCategory(\AppBundle\Entity\ProfessionalActivityCategory $professionalActivityCategory = null)
    {
        $this->professionalActivityCategory = $professionalActivityCategory;

        return $this;
    }

    /**
     * Get professionalActivityCategory
     *
     * @return \AppBundle\Entity\ProfessionalActivityCategory
     */
    public function getProfessionalActivityCategory()
    {
        return $this->professionalActivityCategory;
    }

    /**
     * Add professionalActivities
     *
     * @param \AppBundle\Entity\ProfessionalActivity $professionalActivities
     * @return ProfessionalActivityType
     */
    public function addProfessionalActivity(\AppBundle\Entity\ProfessionalActivity $professionalActivities)
    {
        $this->professionalActivities[] = $professionalActivities;

        return $this;
    }

    /**
     * Remove professionalActivities
     *
     * @param \AppBundle\Entity\ProfessionalActivity $professionalActivities
     */
    public function removeProfessionalActivity(\AppBundle\Entity\ProfessionalActivity $professionalActivities)
    {
        $this->professionalActivities->removeElement($professionalActivities);
    }

    /**
     * Get professionalActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfessionalActivities()
    {
        return $this->professionalActivities;
    }

    public function __toString()
    {
        return $this->name;
    }
}
