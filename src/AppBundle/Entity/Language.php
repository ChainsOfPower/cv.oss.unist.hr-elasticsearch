<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_language")
 */
class Language
{
    // ====
    // Data
    // ====

    /**
     * Identification
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Ime jezika
     * @ORM\Column(name="language_name", type="string", length=128, nullable=false)
     * @var string
     */
    protected $name;

    /**
     * Ime jezika
     * @ORM\Column(name="language_name_en", type="string", length=128, nullable=false)
     * @var string
     */
    protected $nameEn;

    protected $locale;

    // =========
    // Relations
    // =========

    /**
     * Poznanstva jezika
     * @ORM\OneToMany(targetEntity="PersonLanguage", mappedBy="language")
     * @var ArrayCollection
     */
    protected $knownBy;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->knownBy = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Language
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $name
     * @return Language
     */
    public function setNameEn($name)
    {
        $this->nameEn = $name;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Add knownBy
     *
     * @param \AppBundle\Entity\PersonLanguage $knownBy
     * @return Language
     */
    public function addKnownBy(\AppBundle\Entity\PersonLanguage $knownBy)
    {
        $this->knownBy[] = $knownBy;

        return $this;
    }

    /**
     * Remove knownBy
     *
     * @param \AppBundle\Entity\PersonLanguage $knownBy
     */
    public function removeKnownBy(\AppBundle\Entity\PersonLanguage $knownBy)
    {
        $this->knownBy->removeElement($knownBy);
    }

    /**
     * Get knownBy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKnownBy()
    {
        return $this->knownBy;
    }

    /**
     * @param  string $locale
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->locale == 'en') {
            return $this->nameEn;
        } else {
            return $this->name;
        }
    }
}
