<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WorkExperienceRepository")
 * @ORM\Table(name="cv_work_experience")
 */
class WorkExperience
{
    /**
     * Identifikator
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Institucija
     *
     * @ORM\Column(name="work_experience_institution", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $institution;

    /**
     * Institucija
     *
     * @ORM\Column(name="work_experience_institution_en", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $institutionEn;

    /**
     * Datum i godina zaposlenja
     *
     * @ORM\Column(name="work_experience_date_from", type="date", nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var DateTime
     */
    protected $dateFrom;

    /**
     * Datum i godina kraja zaposlenja
     *
     * @ORM\Column(name="work_experience_date_to", type="date", nullable=true)
     * @var DateTime
     */
    protected $dateTo;

    /**
     * Pozicija
     *
     * @ORM\Column(name="work_experience_job_title", type="string", length=128, nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $jobTitle;

    /**
     * Pozicija
     *
     * @ORM\Column(name="work_experience_job_title_en", type="string", length=128, nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $jobTitleEn;

    /**
     * Podrucje rada
     *
     * @ORM\Column(name="work_experience_area", type="text", nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $area;

    /**
     * Podrucje rada
     *
     * @ORM\Column(name="work_experience_area_en", type="text", nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $areaEn;

    /**
     * Funkcija
     *
     * @ORM\Column(name="work_experience_function", type="text", nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $function;

    /**
     * Funkcija
     *
     * @ORM\Column(name="work_experience_function_en", type="text", nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $functionEn;

    /**
     * Da li je riječ o trenutnom zaposlenju (da/ne)
     *
     * @ORM\Column(name="work_experience_current", type="boolean", nullable=true)
     * @var bool
     */
    protected $current;

    /**
     * Referenca na osobu
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="workExperience")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     * @var Person
     */
    protected $person;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set institution
     *
     * @param string $institution
     * @return WorkExperience
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get institution
     *
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set institutionEn
     *
     * @param string $institution
     * @return WorkExperience
     */
    public function setInstitutionEn($institution)
    {
        $this->institutionEn = $institution;

        return $this;
    }

    /**
     * Get institutionEn
     *
     * @return string
     */
    public function getInstitutionEn()
    {
        return $this->institutionEn;
    }

    /**
     * Set jobTitle
     *
     * @param string $jobTitle
     * @return WorkExperience
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set jobTitleEn
     *
     * @param string $jobTitle
     * @return WorkExperience
     */
    public function setJobTitleEn($jobTitle)
    {
        $this->jobTitleEn = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitleEn
     *
     * @return string
     */
    public function getJobTitleEn()
    {
        return $this->jobTitleEn;
    }

    /**
     * Set area
     *
     * @param string $area
     * @return WorkExperience
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set areaEn
     *
     * @param string $area
     * @return WorkExperience
     */
    public function setAreaEn($area)
    {
        $this->areaEn = $area;

        return $this;
    }

    /**
     * Get areaEn
     *
     * @return string
     */
    public function getAreaEn()
    {
        return $this->areaEn;
    }

    /**
     * Set function
     *
     * @param string $function
     * @return WorkExperience
     */
    public function setFunction($function)
    {
        $this->function = $function;

        return $this;
    }

    /**
     * Get function
     *
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * Set functionEn
     *
     * @param string $function
     * @return WorkExperience
     */
    public function setFunctionEn($function)
    {
        $this->functionEn = $function;

        return $this;
    }

    /**
     * Get functionEn
     *
     * @return string
     */
    public function getFunctionEn()
    {
        return $this->functionEn;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Person $person
     * @return WorkExperience
     */
    public function setPerson(\AppBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set dateFrom
     *
     * @param \DateTime $dateFrom
     * @return WorkExperience
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Get date in string format for elasticsearch indexing
     *
     * @return string
     */
    public function getDateFromEs()
    {
        return $this->getDateFrom()->format('Y-m-d');
    }

    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     * @return WorkExperience
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Get date in string format for elasticsearch indexing
     *
     * @return string
     */
    public function getDateToEs()
    {
        if($this->getDateTo() === null)
        {
            return null;
        }
        return $this->getDateTo()->format('Y-m-d');
    }

    /**
     * Set current
     *
     * @param bool $current
     */
    public function setCurrent($current)
    {
        $this->current = $current;

        return $this;
    }

    /**
     * Get current
     *
     * @return bool
     */
    public function isCurrent()
    {
        return $this->current;
    }
}
