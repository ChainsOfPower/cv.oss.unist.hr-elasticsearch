<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_person_language")
 */
class PersonLanguage
{
    /**
     * Identifikator
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Da li je jezik i materinji jezik?
     *
     * @ORM\Column(name="person_language_mother_tongue", type="boolean", nullable=true)
     * @var bool
     */
    protected $motherTongue;

    /**
     * Ocjena
     *
     * @ORM\Column(name="person_language_grade", type="integer", length=1, nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var int
     */
    protected $grade;

    /**
     * Osoba
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="languages")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     * @var Person
     */
    protected $person;

    /**
     * Jezik
     *
     * @ORM\ManyToOne(targetEntity="Language", inversedBy="knownBy")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @var Language
     */
    protected $language;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set motherTongue
     *
     * @param boolean $motherTongue
     * @return PersonLanguage
     */
    public function setMotherTongue($motherTongue)
    {
        $this->motherTongue = $motherTongue;

        return $this;
    }

    /**
     * Get motherTongue
     *
     * @return boolean
     */
    public function isMotherTongue()
    {
        return $this->motherTongue;
    }

    /**
     * Set grade
     *
     * @param integer $grade
     * @return PersonLanguage
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return integer
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Person $person
     * @return PersonLanguage
     */
    public function setPerson(\AppBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     * @return PersonLanguage
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
