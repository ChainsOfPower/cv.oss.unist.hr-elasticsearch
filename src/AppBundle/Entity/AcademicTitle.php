<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_academic_title")
 */
class AcademicTitle
{
    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Naziv znanstvenog podrucja
     * @ORM\Column(name="academic_title_name", type="string", length=128, nullable=false)
     * @var string
     */
    protected $name;

    /**
     * Naziv znanstvenog podrucja (english)
     * @ORM\Column(name="academic_title_name_en", type="string", length=128, nullable=false)
     * @var string
     */
    protected $nameEn;

    /**
     * Lista osoba
     * @ORM\OneToMany(targetEntity="Person", mappedBy="academicTitle")
     * @var ArrayCollection
     */
    protected $persons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScientificArea
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return ScientificArea
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Add persons
     *
     * @param \AppBundle\Entity\Person $persons
     * @return ScientificBranch
     */
    public function addPerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons[] = $persons;

        return $this;
    }

    /**
     * Remove persons
     *
     * @param \AppBundle\Entity\Person $persons
     */
    public function removePerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons->removeElement($persons);
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons()
    {
        return $this->persons;
    }

    public function __toString()
    {
        return $this->name;
    }
}
