<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\User as BaseUser;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 * @ORM\Table(name="cv_person")
 */
class Person extends BaseUser
{
    /**
     * Identifikator
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @Assert\Email(message="Molimo, unesite email.", groups={"Registration", "Profile"})
     * @var string
     */
    protected $email;

    /**
     * Ime
     *
     * @ORM\Column(name="person_first_name", type="string", length=128, nullable=true)
     * @var string
     */
    protected $firstName;

    /**
     * Prezime
     *
     * @ORM\Column(name="person_last_name", type="string", length=128, nullable=true)
     * @var string
     */
    protected $lastName;

    /**
     * Spol
     *
     * @ORM\Column(name="person_gender", type="string", length=1, nullable=true)
     * @var string
     */
    protected $gender;

    /**
     * @var SplFileInfo
     *
     * @Assert\File(
     *     maxSize = "8192k",
     *     maxSizeMessage = "Slika mora biti manja od 8MB.",
     *     mimeTypes = {"image/jpeg", "image/jpg", "image/png", "image/gif"},
     *     mimeTypesMessage = "Slika mora biti u JPEG, JPG ili PNG formatu."
     * )
     */
    protected $profilePictureFile;

    /**
     * @var string
     *
     * @ORM\Column(name="person_profile_picture_path", type="string", length=256, nullable=true)
     */
    protected $profilePicturePath;

    /**
     * Znanstvena titula (dr.sc, mag.sc itd.)
     *
     * @ORM\Column(name="person_scientific_title_pre", type="string", length=64, nullable=true)
     * @var string
     */
    protected $scientificTitlePre;

    /**
     * Znanstvena titula (dr.sc, mag.sc itd.)
     *
     * @ORM\Column(name="person_scientific_title_pre_en", type="string", length=64, nullable=true)
     * @var string
     */
    protected $scientificTitlePreEn;

    /**
     * Znanstveno zvanje (prof v.s. itd.)
     *
     * @ORM\Column(name="person_scientific_title_post", type="string", length=64, nullable=true)
     * @var string
     */
    protected $scientificTitlePost;

    /**
     * Znanstveno zvanje (prof v.s. itd.)
     *
     * @ORM\Column(name="person_scientific_title_post_en", type="string", length=64, nullable=true)
     * @var string
     */
    protected $scientificTitlePostEn;

    /**
     * Sveuciliste
     *
     * @ORM\ManyToOne(targetEntity="University", inversedBy="persons")
     * @ORM\JoinColumn(name="university_id", referencedColumnName="id", onDelete="SET NULL")
     * @var City
     */
    protected $university;

    /**
     * Sastavnica sveucilista
     *
     * @ORM\ManyToOne(targetEntity="UniversityDepartment", inversedBy="persons")
     * @ORM\JoinColumn(name="university_department_id", referencedColumnName="id", onDelete="SET NULL")
     * @var City
     */
    protected $universityDepartment;

    /**
     * Odsjek sastavnice sveucilista
     *
     * @ORM\ManyToOne(targetEntity="UniversityDepartmentDesk", inversedBy="persons")
     * @ORM\JoinColumn(name="university_department_desk_id", referencedColumnName="id", onDelete="SET NULL")
     * @var City
     */
    protected $universityDepartmentDesk;

    /**
     * Honorarac
     *
     * @ORM\Column(name="person_freelancer", type="boolean", nullable=true)
     */
    protected $freelancer;

    /**
     * Državljanstvo
     *
     * @ORM\Column(name="person_citizenship", type="string", length=128, nullable=true)
     * @var string
     */
    protected $citizenship;

    /**
     * DržavljanstvoEn
     *
     * @ORM\Column(name="person_citizenship_en", type="string", length=128, nullable=true)
     * @var string
     */
    protected $citizenshipEn;

    /**
     * Adresa prebivalista
     *
     * @ORM\Column(name="person_address", type="string", length=128, nullable=true)
     * @var string
     */
    protected $address;

    /**
     * Grad prebivalista
     *
     * @ORM\ManyToOne(targetEntity="City", inversedBy="persons")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="city_area_code", onDelete="SET NULL")
     * @var City
     */
    protected $city;

    /**
     * Telefon
     *
     * @ORM\Column(name="person_phone", type="string", length=64, nullable=true)
     * @var string
     */
    protected $phone;

    /**
     * Telefaks
     *
     * @ORM\Column(name="person_fax", type="string", length=64, nullable=true)
     * @var string
     */
    protected $fax;

    /**
     * Datum i godina rođenja
     *
     * @ORM\Column(name="person_date_of_birth", type="date", nullable=true)
     * @var DateTime
     */
    protected $dateOfBirth;

    /**
     * Web stranica
     *
     * @ORM\Column(name="person_web", type="string", length=128, nullable=true)
     * @var string
     */
    protected $web;

    /**
     * Maticni broj iz Upisnika znanstvenika
     *
     * @ORM\Column(name="person_registration_number", type="string", length=64, nullable=true)
     * @var string
     */
    protected $registrationNumber;

    /**
     * Znanstveno ili umjetnicko zvanje
     *
     * @ORM\ManyToOne(targetEntity="ScientificTitle", inversedBy="persons")
     * @ORM\JoinColumn(name="scientific_title_id", referencedColumnName="id", onDelete="SET NULL")
     * @var City
     */
    protected $scientificTitle;

    /**
     * Znanstveno ili umjetnicko zvanje (datum posljednjeg izbora)
     *
     * @ORM\Column(name="person_scientific_title_date", type="date", nullable=true)
     * @var DateTime
     */
    protected $scientificTitleDate;

    /**
     * Znanstveno-nastavno, umjetnicko-nastavno ili nastavno zvanje
     *
     * @ORM\ManyToOne(targetEntity="AcademicTitle", inversedBy="persons")
     * @ORM\JoinColumn(name="academic_title_id", referencedColumnName="id", onDelete="SET NULL")
     * @var City
     */
    protected $academicTitle;

    /**
     * Znanstveno-nastavno, umjetnicko-nastavno ili nastavno zvanje (datum posljednjeg izbora)
     *
     * @ORM\Column(name="person_academic_title_date", type="date", nullable=true)
     * @var DateTime
     */
    protected $academicTitleDate;

    /**
     * NULL
     *
     * @ORM\ManyToOne(targetEntity="ScientificArea", inversedBy="persons")
     * @ORM\JoinColumn(name="scientific_area_id", referencedColumnName="id", onDelete="SET NULL")
     * @var ScientificArea
     */
    protected $scientificArea;

    /**
     * NULL
     *
     * @ORM\ManyToOne(targetEntity="ScientificField", inversedBy="persons")
     * @ORM\JoinColumn(name="scientific_field_id", referencedColumnName="id", onDelete="SET NULL")
     * @var ScientificField
     */
    protected $scientificField;

    /**
     * Podrucje i polje izbora u znanstveno ili umjetnicko zvanje
     *
     * @ORM\ManyToOne(targetEntity="ScientificBranch", inversedBy="persons")
     * @ORM\JoinColumn(name="scientific_branch_id", referencedColumnName="id", onDelete="SET NULL")
     * @var ScientificBranch
     */
    protected $scientificBranch;

    /**
     * Socijalne vjestine i kompetencije
     *
     * @ORM\Column(name="person_social_skills", type="text", nullable=true)
     * @var string
     */
    protected $socialSkills;

    /**
     * Socijalne vjestine i kompetencije
     *
     * @ORM\Column(name="person_social_skills_en", type="text", nullable=true)
     * @var string
     */
    protected $socialSkillsEn;

    /**
     * Organizacijske vjestine i kompetencije
     *
     * @ORM\Column(name="person_organizational_skills", type="text", nullable=true)
     * @var string
     */
    protected $organizationalSkills;

    /**
     * Organizacijske vjestine i kompetencije
     *
     * @ORM\Column(name="person_organizational_skills_en", type="text", nullable=true)
     * @var string
     */
    protected $organizationalSkillsEn;

    /**
     * Tehnicke vjestine i kompetencije
     *
     * @ORM\Column(name="person_technical_skills", type="text", nullable=true)
     * @var string
     */
    protected $technicalSkills;

    /**
     * Tehnicke vjestine i kompetencije
     *
     * @ORM\Column(name="person_technical_skills_en", type="text", nullable=true)
     * @var string
     */
    protected $technicalSkillsEn;

    /**
     * Umjetnicke vjestine i kompetencije
     *
     * @ORM\Column(name="person_art_skills", type="text", nullable=true)
     * @var string
     */
    protected $artSkills;

    /**
     * Umjetnicke vjestine i kompetencije
     *
     * @ORM\Column(name="person_art_skills_en", type="text", nullable=true)
     * @var string
     */
    protected $artSkillsEn;

    /**
     * Ostale vjestine i kompetencije
     *
     * @ORM\Column(name="person_misc_skills", type="text", nullable=true)
     * @var string
     */
    protected $miscSkills;

    /**
     * Ostale vjestine i kompetencije
     *
     * @ORM\Column(name="person_misc_skills_en", type="text", nullable=true)
     * @var string
     */
    protected $miscSkillsEn;

    /**
     * Vozacka dozvola
     *
     * @ORM\Column(name="person_driver_license", type="string", length=64, nullable=true)
     * @var string
     */
    protected $driverLicense;

    /**
     * Dodatni podaci
     *
     * @ORM\Column(name="person_additional", type="text", nullable=true)
     * @var string
     */
    protected $additional;

    /**
     * Dodatni podaci
     *
     * @ORM\Column(name="person_additional_en", type="text", nullable=true)
     * @var string
     */
    protected $additionalEn;

    /**
     * Radno iskustvo
     *
     * @ORM\OneToMany(targetEntity="WorkExperience", mappedBy="person", cascade={"remove", "persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"current" = "DESC", "dateFrom" = "DESC"})
     * @var ArrayCollection
     */
    protected $workExperience;

    /**
     * Skolovanje
     *
     * @ORM\OneToMany(targetEntity="School", mappedBy="person", cascade={"remove", "persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"date" = "DESC"})
     * @var ArrayCollection
     */
    protected $schooling;

    /**
     * Usavrsavanje
     *
     * @ORM\OneToMany(targetEntity="Specialization", mappedBy="person", cascade={"remove", "persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"date" = "DESC"})
     * @var ArrayCollection
     */
    protected $specialization;

    /**
     * Materinji i strani jezici
     *
     * @ORM\OneToMany(targetEntity="PersonLanguage", mappedBy="person", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $languages;

    /**
     * Lista znanstvenih djelatnosti
     *
     * @ORM\OneToMany(targetEntity="ScientificActivity", mappedBy="person", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $scientificActivities;

    /**
     * Lista nastavnih djelatnosti
     *
     * @ORM\OneToMany(targetEntity="TeachingActivity", mappedBy="person", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $teachingActivities;

    /**
     * Lista stucnih djelatnosti
     *
     * @ORM\OneToMany(targetEntity="ProfessionalActivity", mappedBy="person", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $professionalActivities;

    /**
     * Ukloni me s popisa osoblja
     *
     * @ORM\Column(name="person_hide", type="boolean", nullable=true)
     */
    protected $hide;


    public function shouldIndex()
    {
        return !($this->hide);
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->workExperience = new \Doctrine\Common\Collections\ArrayCollection();
        $this->schooling = new \Doctrine\Common\Collections\ArrayCollection();
        $this->specialization = new \Doctrine\Common\Collections\ArrayCollection();
        $this->languages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->scientificActivities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teachingActivities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->professionalActivities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Person
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Person
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set scientificTitlePre
     *
     * @param string $scientificTitlePre
     * @return Person
     */
    public function setScientificTitlePre($scientificTitlePre)
    {
        $this->scientificTitlePre = $scientificTitlePre;

        return $this;
    }

    /**
     * Get scientificTitlePre
     *
     * @return string
     */
    public function getScientificTitlePre()
    {
        return $this->scientificTitlePre;
    }

    /**
     * Set scientificTitlePreEn
     *
     * @param string $scientificTitlePre
     * @return Person
     */
    public function setScientificTitlePreEn($scientificTitlePre)
    {
        $this->scientificTitlePreEn = $scientificTitlePre;

        return $this;
    }

    /**
     * Get scientificTitlePreEn
     *
     * @return string
     */
    public function getScientificTitlePreEn()
    {
        return $this->scientificTitlePreEn;
    }

    /**
     * Set scientificTitlePost
     *
     * @param string $scientificTitlePost
     * @return Person
     */
    public function setScientificTitlePost($scientificTitlePost)
    {
        $this->scientificTitlePost = $scientificTitlePost;

        return $this;
    }

    /**
     * Get scientificTitlePost
     *
     * @return string
     */
    public function getScientificTitlePost()
    {
        return $this->scientificTitlePost;
    }

    /**
     * Set scientificTitlePostEn
     *
     * @param string $scientificTitlePost
     * @return Person
     */
    public function setScientificTitlePostEn($scientificTitlePost)
    {
        $this->scientificTitlePostEn = $scientificTitlePost;

        return $this;
    }

    /**
     * Get scientificTitlePostEn
     *
     * @return string
     */
    public function getScientificTitlePostEn()
    {
        return $this->scientificTitlePostEn;
    }

    /**
     * Set university
     *
     * @param University $university
     * @return Person
     */
    public function setUniversity(University $university = null)
    {
        $this->university = $university;

        return $this;
    }

    /**
     * Get university
     *
     * @return University
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * Set universityDepartment
     *
     * @param UniversityDepartment $universityDepartment
     * @return Person
     */
    public function setUniversityDepartment(UniversityDepartment $universityDepartment = null)
    {
        $this->universityDepartment = $universityDepartment;

        return $this;
    }

    /**
     * Get universityDepartment
     *
     * @return UniversityDepartment
     */
    public function getUniversityDepartment()
    {
        return $this->universityDepartment;
    }

    /**
     * Set universityDepartmentDesk
     *
     * @param UniversityDepartmentDesk $universityDepartmentDesk
     * @return Person
     */
    public function setUniversityDepartmentDesk(UniversityDepartmentDesk $universityDepartmentDesk = null)
    {
        $this->universityDepartmentDesk = $universityDepartmentDesk;

        return $this;
    }

    /**
     * Get universityDepartmentDesk
     *
     * @return UniversityDepartmentDesk
     */
    public function getUniversityDepartmentDesk()
    {
        return $this->universityDepartmentDesk;
    }

    /**
     * Set freelancer
     *
     * @param bool $freelancer
     * @return Person
     */
    public function setFreelancer($freelancer)
    {
        $this->freelancer = $freelancer;

        return $this;
    }

    /**
     * Get freelancer
     *
     * @return bool
     */
    public function getFreelancer()
    {
        return $this->freelancer;
    }

    /**
     * Set citizenship
     *
     * @param string $citizenship
     * @return Person
     */
    public function setCitizenship($citizenship)
    {
        $this->citizenship = $citizenship;

        return $this;
    }

    /**
     * Get citizenship
     *
     * @return string
     */
    public function getCitizenship()
    {
        return $this->citizenship;
    }

    /**
     * Set citizenshipEn
     *
     * @param string $citizenship
     * @return Person
     */
    public function setCitizenshipEn($citizenship)
    {
        $this->citizenshipEn = $citizenship;

        return $this;
    }

    /**
     * Get citizenshipEn
     *
     * @return string
     */
    public function getCitizenshipEn()
    {
        return $this->citizenshipEn;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Person
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Person
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Person
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     * @return Person
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set web
     *
     * @param string $web
     * @return Person
     */
    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    /**
     * Get web
     *
     * @return string
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set registrationNumber
     *
     * @param string $registrationNumber
     * @return Person
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    /**
     * Get registrationNumber
     *
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * Set scientificTitleDesc
     *
     * @param string $scientificTitleDesc
     * @return Person
     */
    public function setScientificTitleDesc($scientificTitleDesc)
    {
        $this->scientificTitleDesc = $scientificTitleDesc;

        return $this;
    }

    /**
     * Get scientificTitleDesc
     *
     * @return string
     */
    public function getScientificTitleDesc()
    {
        return $this->scientificTitleDesc;
    }





    public function setScientificTitle(ScientificTitle $scientificTitle = null)
    {
        $this->scientificTitle = $scientificTitle;

        return $this;
    }

    public function getScientificTitle()
    {
        return $this->scientificTitle;
    }

    public function setAcademicTitle(AcademicTitle $academicTitle = null)
    {
        $this->academicTitle = $academicTitle;

        return $this;
    }

    public function getAcademicTitle()
    {
        return $this->academicTitle;
    }

    public function setScientificTitleDate(\DateTime $scientificTitleDate = null)
    {
        $this->scientificTitleDate = $scientificTitleDate;

        return $this;
    }

    public function getScientificTitleDate()
    {
        return $this->scientificTitleDate;
    }

    public function setAcademicTitleDate(\DateTime $academicTitleDate = null)
    {
        $this->academicTitleDate = $academicTitleDate;

        return $this;
    }

    public function getAcademicTitleDate()
    {
        return $this->academicTitleDate;
    }





    /**
     * Set academicTitleDesc
     *
     * @param string $academicTitleDesc
     * @return Person
     */
    public function setAcademicTitleDesc($academicTitleDesc)
    {
        $this->academicTitleDesc = $academicTitleDesc;

        return $this;
    }

    /**
     * Get academicTitleDesc
     *
     * @return string
     */
    public function getAcademicTitleDesc()
    {
        return $this->academicTitleDesc;
    }

    /**
     * Set socialSkills
     *
     * @param string $socialSkills
     * @return Person
     */
    public function setSocialSkills($socialSkills)
    {
        $this->socialSkills = $socialSkills;

        return $this;
    }

    /**
     * Get socialSkills
     *
     * @return string
     */
    public function getSocialSkills()
    {
        return $this->socialSkills;
    }

    /**
     * Set organizationalSkills
     *
     * @param string $organizationalSkills
     * @return Person
     */
    public function setOrganizationalSkills($organizationalSkills)
    {
        $this->organizationalSkills = $organizationalSkills;

        return $this;
    }

    /**
     * Get organizationalSkills
     *
     * @return string
     */
    public function getOrganizationalSkills()
    {
        return $this->organizationalSkills;
    }

    /**
     * Set technicalSkills
     *
     * @param string $technicalSkills
     * @return Person
     */
    public function setTechnicalSkills($technicalSkills)
    {
        $this->technicalSkills = $technicalSkills;

        return $this;
    }

    /**
     * Get technicalSkills
     *
     * @return string
     */
    public function getTechnicalSkills()
    {
        return $this->technicalSkills;
    }

    /**
     * Set artSkills
     *
     * @param string $artSkills
     * @return Person
     */
    public function setArtSkills($artSkills)
    {
        $this->artSkills = $artSkills;

        return $this;
    }

    /**
     * Get artSkills
     *
     * @return string
     */
    public function getArtSkills()
    {
        return $this->artSkills;
    }

    /**
     * Set miscSkills
     *
     * @param string $miscSkills
     * @return Person
     */
    public function setMiscSkills($miscSkills)
    {
        $this->miscSkills = $miscSkills;

        return $this;
    }

    /**
     * Get miscSkills
     *
     * @return string
     */
    public function getMiscSkills()
    {
        return $this->miscSkills;
    }

    /**
     * Set socialSkillsEn
     *
     * @param string $socialSkills
     * @return Person
     */
    public function setSocialSkillsEn($socialSkills)
    {
        $this->socialSkillsEn = $socialSkills;

        return $this;
    }

    /**
     * Get socialSkillsEn
     *
     * @return string
     */
    public function getSocialSkillsEn()
    {
        return $this->socialSkillsEn;
    }

    /**
     * Set organizationalSkillsEn
     *
     * @param string $organizationalSkills
     * @return Person
     */
    public function setOrganizationalSkillsEn($organizationalSkills)
    {
        $this->organizationalSkillsEn = $organizationalSkills;

        return $this;
    }

    /**
     * Get organizationalSkillsEn
     *
     * @return string
     */
    public function getOrganizationalSkillsEn()
    {
        return $this->organizationalSkillsEn;
    }

    /**
     * Set technicalSkillsEn
     *
     * @param string $technicalSkills
     * @return Person
     */
    public function setTechnicalSkillsEn($technicalSkills)
    {
        $this->technicalSkillsEn = $technicalSkills;

        return $this;
    }

    /**
     * Get technicalSkillsEn
     *
     * @return string
     */
    public function getTechnicalSkillsEn()
    {
        return $this->technicalSkillsEn;
    }

    /**
     * Set artSkillsEn
     *
     * @param string $artSkills
     * @return Person
     */
    public function setArtSkillsEn($artSkills)
    {
        $this->artSkillsEn = $artSkills;

        return $this;
    }

    /**
     * Get artSkillsEn
     *
     * @return string
     */
    public function getArtSkillsEn()
    {
        return $this->artSkillsEn;
    }

    /**
     * Set miscSkillsEn
     *
     * @param string $miscSkills
     * @return Person
     */
    public function setMiscSkillsEn($miscSkills)
    {
        $this->miscSkillsEn = $miscSkills;

        return $this;
    }

    /**
     * Get miscSkillsEn
     *
     * @return string
     */
    public function getMiscSkillsEn()
    {
        return $this->miscSkillsEn;
    }

    /**
     * Set driverLicense
     *
     * @param string $driverLicense
     * @return Person
     */
    public function setDriverLicense($driverLicense)
    {
        $this->driverLicense = $driverLicense;

        return $this;
    }

    /**
     * Get driverLicense
     *
     * @return string
     */
    public function getDriverLicense()
    {
        return $this->driverLicense;
    }

    /**
     * Set additional
     *
     * @param string $additional
     * @return Person
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;

        return $this;
    }

    /**
     * Get additional
     *
     * @return string
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * Set additionalEn
     *
     * @param string $additional
     * @return Person
     */
    public function setAdditionalEn($additional)
    {
        $this->additionalEn = $additional;

        return $this;
    }

    /**
     * Get additional
     *
     * @return string
     */
    public function getAdditionalEn()
    {
        return $this->additionalEn;
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     * @return Person
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set scientificArea
     *
     * @param \AppBundle\Entity\ScientificArea $scientificArea
     * @return Person
     */
    public function setScientificArea(\AppBundle\Entity\ScientificArea $scientificArea = null)
    {
        $this->scientificArea = $scientificArea;

        return $this;
    }

    /**
     * Get scientificArea
     *
     * @return \AppBundle\Entity\ScientificArea
     */
    public function getScientificArea()
    {
        return $this->scientificArea;
    }

    /**
     * Set scientificField
     *
     * @param \AppBundle\Entity\ScientificField $scientificField
     * @return Person
     */
    public function setScientificField(\AppBundle\Entity\ScientificField $scientificField = null)
    {
        $this->scientificField = $scientificField;

        return $this;
    }

    /**
     * Get scientificField
     *
     * @return \AppBundle\Entity\ScientificField
     */
    public function getScientificField()
    {
        return $this->scientificField;
    }

    /**
     * Set scientificBranch
     *
     * @param \AppBundle\Entity\ScientificBranch $scientificBranch
     * @return Person
     */
    public function setScientificBranch(\AppBundle\Entity\ScientificBranch $scientificBranch = null)
    {
        $this->scientificBranch = $scientificBranch;

        return $this;
    }

    /**
     * Get scientificBranch
     *
     * @return \AppBundle\Entity\ScientificBranch
     */
    public function getScientificBranch()
    {
        return $this->scientificBranch;
    }

    /**
     * Add workExperience
     *
     * @param \AppBundle\Entity\WorkExperience $workExperience
     * @return Person
     */
    public function addWorkExperience(\AppBundle\Entity\WorkExperience $workExperience)
    {
        $workExperience->setPerson($this);
        $this->workExperience[] = $workExperience;

        return $this;
    }

    /**
     * Remove workExperience
     *
     * @param \AppBundle\Entity\WorkExperience $workExperience
     */
    public function removeWorkExperience(\AppBundle\Entity\WorkExperience $workExperience)
    {
        $this->workExperience->removeElement($workExperience);
    }

    /**
     * Get workExperience
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkExperience()
    {
        return $this->workExperience;
    }

    /**
     * Add schooling
     *
     * @param \AppBundle\Entity\School $schooling
     * @return Person
     */
    public function addSchooling(\AppBundle\Entity\School $school)
    {
        $school->setPerson($this);
        $this->schooling[] = $school;

        return $this;
    }

    /**
     * Remove schooling
     *
     * @param \AppBundle\Entity\School $schooling
     */
    public function removeSchooling(\AppBundle\Entity\School $schooling)
    {
        $this->schooling->removeElement($schooling);
    }

    /**
     * Get schooling
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchooling()
    {
        return $this->schooling;
    }

    /**
     * Add specialization
     *
     * @param \AppBundle\Entity\Specialization $specialization
     * @return Person
     */
    public function addSpecialization(\AppBundle\Entity\Specialization $specialization)
    {
        $specialization->setPerson($this);
        $this->specialization[] = $specialization;

        return $this;
    }

    /**
     * Remove specialization
     *
     * @param \AppBundle\Entity\Specialization $specialization
     */
    public function removeSpecialization(\AppBundle\Entity\Specialization $specialization)
    {
        $this->specialization->removeElement($specialization);
    }

    /**
     * Get specialization
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * Add languages
     *
     * @param \AppBundle\Entity\PersonLanguage $languages
     * @return Person
     */
    public function addLanguage(\AppBundle\Entity\PersonLanguage $language)
    {
        $language->setPerson($this);
        $this->languages[] = $language;

        return $this;
    }

    /**
     * Remove languages
     *
     * @param \AppBundle\Entity\PersonLanguage $languages
     */
    public function removeLanguage(\AppBundle\Entity\PersonLanguage $languages)
    {
        $this->languages->removeElement($languages);
    }

    /**
     * Get languages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Add scientificActivity
     *
     * @param \AppBundle\Entity\ScientificActivity $scientificActivity
     * @return Person
     */
    public function addScientificActivity(\AppBundle\Entity\ScientificActivity $scientificActivity)
    {
        $scientificActivity->setPerson($this);
        $this->scientificActivities[] = $scientificActivity;

        return $this;
    }

    /**
     * Remove scientificActivity
     *
     * @param \AppBundle\Entity\ScientificActivity $scientificActivity
     */
    public function removeScientificActivity(\AppBundle\Entity\ScientificActivity $scientificActivity)
    {
        $this->scientificActivities->removeElement($scientificActivity);
    }

    /**
     * Get scientificActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScientificActivities()
    {
        return $this->scientificActivities;
    }

    /**
     * Add teachingActivity
     *
     * @param \AppBundle\Entity\TeachingActivity $teachingActivity
     * @return Person
     */
    public function addTeachingActivity(\AppBundle\Entity\TeachingActivity $teachingActivity)
    {
        $teachingActivity->setPerson($this);
        $this->teachingActivities[] = $teachingActivity;

        return $this;
    }

    /**
     * Remove teachingActivity
     *
     * @param \AppBundle\Entity\TeachingActivity $teachingActivity
     */
    public function removeTeachingActivity(\AppBundle\Entity\TeachingActivity $teachingActivity)
    {
        $this->teachingActivities->removeElement($teachingActivity);
    }

    /**
     * Get teachingActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeachingActivities()
    {
        return $this->teachingActivities;
    }

    /**
     * Add professionalActivity
     *
     * @param \AppBundle\Entity\ProfessionalActivity $professionalActivity
     * @return Person
     */
    public function addProfessionalActivity(\AppBundle\Entity\ProfessionalActivity $professionalActivity)
    {
        $professionalActivity->setPerson($this);
        $this->professionalActivities[] = $professionalActivity;

        return $this;
    }

    /**
     * Remove professionalActivity
     *
     * @param \AppBundle\Entity\ProfessionalActivity $professionalActivity
     */
    public function removeProfessionalActivity(\AppBundle\Entity\ProfessionalActivity $professionalActivity)
    {
        $this->professionalActivities->removeElement($professionalActivity);
    }

    /**
     * Get professionalActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfessionalActivities()
    {
        return $this->professionalActivities;
    }

    public function getSimpleLangList($locale = 'hr')
    {
        $list = '';
        foreach ($this->languages as $language) {
            if (!$language->isMotherTongue()) {
                if ($locale == 'en') {
                    $list .= $language->getLanguage()->getNameEn() . ' - ' . $language->getGrade();
                } else {
                    $list .= $language->getLanguage()->getName() . ' - ' . $language->getGrade();
                }
                if ( $language->getGrade() == '2' ) {
                    if ($locale == 'en') {
                        $list .= ' (sufficient)';
                    } else {
                        $list .= ' (dovoljan)';
                    }
                } elseif ( $language->getGrade() == '3' ) {
                    if ($locale == 'en') {
                        $list .= ' (good)';
                    } else {
                        $list .= ' (dobar)';
                    }
                } elseif ( $language->getGrade() == '4' ) {
                    if ($locale == 'en') {
                        $list .= ' (very good)';
                    } else {
                        $list .= ' (dobar)';
                    }
                } elseif ( $language->getGrade() == '5' ) {
                    if ($locale == 'en') {
                        $list .= ' (excellent)';
                    } else {
                        $list .= ' (dobar)';
                    }
                }
                $list .= '<br />';
            }
        }

        return $list;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setProfilePictureFile(UploadedFile $file = null)
    {
        $this->profilePictureFile = $file;
    }

    public function setProfilePicturePath($profilePicturePath)
    {
        $this->profilePicturePath = $profilePicturePath;

        return $this;
    }

    public function getProfilePicturePath()
    {
        return $this->profilePicturePath;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getProfilePictureFile()
    {
        return $this->profilePictureFile;
    }

    public function getAbsolutePath()
    {
        return null === $this->profilePicturePath
            ? null
            : $this->getUploadRootDir().'/'.$this->profilePicturePath;
    }

    public function getWebPath()
    {
        return null === $this->profilePicturePath
            ? null
            : $this->getUploadDir().'/'.$this->profilePicturePath;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/profile/' . $this->id;
    }

    public function upload()
    {
        if (null === $this->getProfilePictureFile()) {
            return;
        }

        $this->getProfilePictureFile()->move(
            $this->getUploadRootDir(),
            $this->getProfilePictureFile()->getClientOriginalName()
        );

        $this->profilePicturePath = $this->getProfilePictureFile()->getClientOriginalName();

        $this->file = null;
    }

    public function getSwitchLink()
    {
        return '<a href="/?_switch_user=' . $this->username . '">Upotrijebi račun</a>';
    }
}
