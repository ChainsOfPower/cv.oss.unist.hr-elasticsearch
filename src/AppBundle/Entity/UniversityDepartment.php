<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_university_department")
 */
class UniversityDepartment
{
    /**
     * Identifikator
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * Naziv
     *
     * @ORM\Column(name="university_department_name", type="string", length=128, nullable=false)
     * @var string
     */
    private $name;

    /**
     * Naziv
     *
     * @ORM\Column(name="university_department_name_en", type="string", length=128, nullable=false)
     * @var string
     */
    private $nameEn;

    /**
     * Adresa
     *
     * @ORM\Column(name="university_department_address", type="string", length=128, nullable=true)
     * @var string
     */
    private $address;

    /**
     * Kontakt informacije
     *
     * @ORM\Column(name="university_department_contact", type="text", nullable=true)
     * @var string
     */
    private $contact;

    /**
     * Referenca na sveučilište
     *
     * @ORM\ManyToOne(targetEntity="University", inversedBy="departments")
     * @ORM\JoinColumn(name="university_id", referencedColumnName="id", onDelete="CASCADE")
     * @var ScientificArea
     */
    protected $university;

    /**
     * Katedre
     *
     * @ORM\OneToMany(targetEntity="UniversityDepartmentDesk", mappedBy="universityDepartment")
     * @var ArrayCollection
     */
    protected $desks;

    /**
     * Povezane osobe
     *
     * @ORM\OneToMany(targetEntity="Person", mappedBy="universityDepartment")
     * @var ArrayCollection
     */
    protected $persons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->desks = new \Doctrine\Common\Collections\ArrayCollection();
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UniversityDepartment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $name
     * @return UniversityDepartment
     */
    public function setNameEn($name)
    {
        $this->nameEn = $name;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return UniversityDepartment
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return UniversityDepartment
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set university
     *
     * @param \AppBundle\Entity\University $university
     * @return UniversityDepartment
     */
    public function setUniversity(\AppBundle\Entity\University $university = null)
    {
        $this->university = $university;

        return $this;
    }

    /**
     * Get university
     *
     * @return \AppBundle\Entity\University
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * Add desks
     *
     * @param \AppBundle\Entity\UniversityDepartmentDesk $desks
     * @return UniversityDepartment
     */
    public function addDesk(\AppBundle\Entity\UniversityDepartmentDesk $desks)
    {
        $this->desks[] = $desks;

        return $this;
    }

    /**
     * Remove desks
     *
     * @param \AppBundle\Entity\UniversityDepartmentDesk $desks
     */
    public function removeDesk(\AppBundle\Entity\UniversityDepartmentDesk $desks)
    {
        $this->desks->removeElement($desks);
    }

    /**
     * Get desks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesks()
    {
        return $this->desks;
    }

    /**
     * Add persons
     *
     * @param \AppBundle\Entity\Person $persons
     * @return UniversityDepartment
     */
    public function addPerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons[] = $persons;

        return $this;
    }

    /**
     * Remove persons
     *
     * @param \AppBundle\Entity\Person $persons
     */
    public function removePerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons->removeElement($persons);
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (null !== $this->university) {
            return "{$this->name} ({$this->university->getName()})";
        }

        return $this->name;
    }
}
