<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_teaching_activity_category")
 */
class TeachingActivityCategory
{
    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Autori
     * @ORM\Column(name="teaching_activity_category_name", type="string", length=128, nullable=true)
     * @var string
     */
    protected $name;

    /**
     * Autori
     * @ORM\Column(name="teaching_activity_category_name_en", type="string", length=128, nullable=true)
     * @var string
     */
    protected $nameEn;

    /**
     * Lista tipova kategorije nastavne djelatnosti
     * @ORM\OneToMany(targetEntity="TeachingActivityType", mappedBy="teachingActivityCategory", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $teachingActivityTypes;

    /**
     * Lista nastavnih aktivnosti
     * @ORM\OneToMany(targetEntity="TeachingActivity", mappedBy="teachingActivityCategory", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $teachingActivities;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teachingActivityTypes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teachingActivities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TeachingActivityCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return ScientificActivityCategory
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Add teachingActivityTypes
     *
     * @param \AppBundle\Entity\TeachingActivityType $teachingActivityTypes
     * @return TeachingActivityCategory
     */
    public function addTeachingActivityType(\AppBundle\Entity\TeachingActivityType $teachingActivityTypes)
    {
        $this->teachingActivityTypes[] = $teachingActivityTypes;

        return $this;
    }

    /**
     * Remove teachingActivityTypes
     *
     * @param \AppBundle\Entity\TeachingActivityType $teachingActivityTypes
     */
    public function removeTeachingActivityType(\AppBundle\Entity\TeachingActivityType $teachingActivityTypes)
    {
        $this->teachingActivityTypes->removeElement($teachingActivityTypes);
    }

    /**
     * Get teachingActivityTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeachingActivityTypes()
    {
        return $this->teachingActivityTypes;
    }

    /**
     * Add teachingActivities
     *
     * @param \AppBundle\Entity\TeachingActivity $teachingActivities
     * @return TeachingActivityCategory
     */
    public function addTeachingActivity(\AppBundle\Entity\TeachingActivity $teachingActivities)
    {
        $this->teachingActivities[] = $teachingActivities;

        return $this;
    }

    /**
     * Remove teachingActivities
     *
     * @param \AppBundle\Entity\TeachingActivity $teachingActivities
     */
    public function removeTeachingActivity(\AppBundle\Entity\TeachingActivity $teachingActivities)
    {
        $this->teachingActivities->removeElement($teachingActivities);
    }

    /**
     * Get teachingActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeachingActivities()
    {
        return $this->teachingActivities;
    }

    public function __toString()
    {
        return $this->name;
    }
}
