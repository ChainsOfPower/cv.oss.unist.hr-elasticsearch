<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_scientific_branch")
 */
class ScientificBranch
{
    // ====
    // Data
    // ====

    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Naziv znanstvene grane
     * @ORM\Column(name="scientific_branch_name", type="string", length=128, nullable=false)
     * @var string
     */
    protected $name;

    /**
     * Naziv znanstvene grane
     * @ORM\Column(name="scientific_branch_name_en", type="string", length=128, nullable=false)
     * @var string
     */
    protected $nameEn;

    // =========
    // Relations
    // =========

    /**
     * Referenca na znanstveno polje
     * @ORM\ManyToOne(targetEntity="ScientificField", inversedBy="scientificBranches")
     * @ORM\JoinColumn(name="scientific_field_id", referencedColumnName="id", onDelete="CASCADE")
     * @var ScientificField
     */
    protected $scientificField;

    /**
     * Lista osoba
     * @ORM\OneToMany(targetEntity="Person", mappedBy="scientificBranch")
     * @var ArrayCollection
     */
    protected $persons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScientificBranch
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $name
     * @return University
     */
    public function setNameEn($name)
    {
        $this->nameEn = $name;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set scientificField
     *
     * @param \AppBundle\Entity\ScientificField $scientificField
     * @return ScientificBranch
     */
    public function setScientificField(\AppBundle\Entity\ScientificField $scientificField = null)
    {
        $this->scientificField = $scientificField;

        return $this;
    }

    /**
     * Get scientificField
     *
     * @return \AppBundle\Entity\ScientificField
     */
    public function getScientificField()
    {
        return $this->scientificField;
    }

    /**
     * Add persons
     *
     * @param \AppBundle\Entity\Person $persons
     * @return ScientificBranch
     */
    public function addPerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons[] = $persons;

        return $this;
    }

    /**
     * Remove persons
     *
     * @param \AppBundle\Entity\Person $persons
     */
    public function removePerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons->removeElement($persons);
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
        // return $this->name . ' (' . $this->scientificField->getName() . ', ' . $this->scientificField->getScientificArea()->getName() . ')';
    }
}
