<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_professional_activity_category")
 */
class ProfessionalActivityCategory
{
    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Autori
     * @ORM\Column(name="professional_activity_category_name", type="string", length=128, nullable=true)
     * @var string
     */
    protected $name;

    /**
     * Autori
     * @ORM\Column(name="professional_activity_category_name_en", type="string", length=128, nullable=true)
     * @var string
     */
    protected $nameEn;

    /**
     * Lista tipova kategorije znanstvene djelatnosti
     * @ORM\OneToMany(targetEntity="ProfessionalActivityType", mappedBy="professionalActivityCategory", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $professionalActivityTypes;

    /**
     * Lista znanstvenih aktivnosti
     * @ORM\OneToMany(targetEntity="ProfessionalActivity", mappedBy="professionalActivityCategory", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $professionalActivities;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->professionalActivityTypes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->professionalActivities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProfessionalActivityCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return ScientificActivityCategory
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Add professionalActivityTypes
     *
     * @param \AppBundle\Entity\ProfessionalActivityType $professionalActivityTypes
     * @return ProfessionalActivityCategory
     */
    public function addProfessionalActivityType(\AppBundle\Entity\ProfessionalActivityType $professionalActivityTypes)
    {
        $this->professionalActivityTypes[] = $professionalActivityTypes;

        return $this;
    }

    /**
     * Remove professionalActivityTypes
     *
     * @param \AppBundle\Entity\ProfessionalActivityType $professionalActivityTypes
     */
    public function removeProfessionalActivityType(\AppBundle\Entity\ProfessionalActivityType $professionalActivityTypes)
    {
        $this->professionalActivityTypes->removeElement($professionalActivityTypes);
    }

    /**
     * Get professionalActivityTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfessionalActivityTypes()
    {
        return $this->professionalActivityTypes;
    }

    /**
     * Add professionalActivities
     *
     * @param \AppBundle\Entity\ProfessionalActivity $professionalActivities
     * @return ProfessionalActivityCategory
     */
    public function addProfessionalActivity(\AppBundle\Entity\ProfessionalActivity $professionalActivities)
    {
        $this->professionalActivities[] = $professionalActivities;

        return $this;
    }

    /**
     * Remove professionalActivities
     *
     * @param \AppBundle\Entity\ProfessionalActivity $professionalActivities
     */
    public function removeProfessionalActivity(\AppBundle\Entity\ProfessionalActivity $professionalActivities)
    {
        $this->professionalActivities->removeElement($professionalActivities);
    }

    /**
     * Get professionalActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfessionalActivities()
    {
        return $this->professionalActivities;
    }

    public function __toString()
    {
        return $this->name;
    }
}
