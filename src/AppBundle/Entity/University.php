<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_university")
 */
class University
{
    /**
     * Identifikator
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * Naziv
     *
     * @ORM\Column(name="university_name", type="string", length=128, nullable=false)
     * @var string
     */
    private $name;

    /**
     * Naziv
     *
     * @ORM\Column(name="university_name_en", type="string", length=128, nullable=false)
     * @var string
     */
    private $nameEn;

    /**
     * Adresa
     *
     * @ORM\Column(name="university_address", type="string", length=128, nullable=true)
     * @var string
     */
    private $address;

    /**
     * Kontakt informacije
     *
     * @ORM\Column(name="university_contact", type="text", nullable=true)
     * @var string
     */
    private $contact;

    /**
     * Sastavnice
     *
     * @ORM\OneToMany(targetEntity="UniversityDepartment", mappedBy="university")
     * @var ArrayCollection
     */
    protected $departments;

    /**
     * Povezane osobe
     *
     * @ORM\OneToMany(targetEntity="Person", mappedBy="university")
     * @var ArrayCollection
     */
    protected $persons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->departments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return University
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $name
     * @return University
     */
    public function setNameEn($name)
    {
        $this->nameEn = $name;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return University
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return University
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Add departments
     *
     * @param \AppBundle\Entity\UniversityDepartment $departments
     * @return University
     */
    public function addDepartment(\AppBundle\Entity\UniversityDepartment $departments)
    {
        $this->departments[] = $departments;

        return $this;
    }

    /**
     * Remove departments
     *
     * @param \AppBundle\Entity\UniversityDepartment $departments
     */
    public function removeDepartment(\AppBundle\Entity\UniversityDepartment $departments)
    {
        $this->departments->removeElement($departments);
    }

    /**
     * Get departments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * Add persons
     *
     * @param \AppBundle\Entity\Person $persons
     * @return University
     */
    public function addPerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons[] = $persons;

        return $this;
    }

    /**
     * Remove persons
     *
     * @param \AppBundle\Entity\Person $persons
     */
    public function removePerson(\AppBundle\Entity\Person $persons)
    {
        $this->persons->removeElement($persons);
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}
