<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ScientificActivityRepository")
 * @ORM\Table(name="cv_scientific_activity")
 */
class ScientificActivity
{
    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Autori
     *
     * @ORM\Column(name="scientific_activity_authors", type="string", length=256, nullable=false)
     * @var string
     */
    protected $authors;

    /**
     * Datum
     *
     * @ORM\Column(name="scientific_activity_date", type="date", nullable=true)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno - služi za sortiranje.")
     * @var DateTime
     */
    protected $date;

    /**
     * Opis (sadrzaj)
     * @ORM\Column(name="scientific_activity_description", type="text", nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $description;

    /**
     * Opis (sadrzaj)
     * @ORM\Column(name="scientific_activity_description_en", type="text", nullable=false)
     * @Assert\NotBlank(message = "Ovo polje ne smije biti prazno")
     * @var string
     */
    protected $descriptionEn;

    /**
     * Referenca na osobu
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="scientificActivities")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", nullable=false)
     * @var Person
     */
    protected $person;

    /**
     * Referenca na krategoriju znanstvene djelatnosti
     * @ORM\ManyToOne(targetEntity="ScientificActivityCategory", inversedBy="scientificActivities")
     * @ORM\JoinColumn(name="scientific_activity_category_id", referencedColumnName="id", nullable=true)
     * @Assert\NotNull(message = "Potrebno je odabrati kategoriju.")
     * @var ScientificActivityCategory
     */
    protected $scientificActivityCategory;

    /**
     * Referenca na tip znanstvene djelatnosti
     * @ORM\ManyToOne(targetEntity="ScientificActivityType", inversedBy="scientificActivities")
     * @ORM\JoinColumn(name="scientific_activity_type_id", referencedColumnName="id", nullable=true)
     * @var ScientificActivityType
     */
    protected $scientificActivityType;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set authors
     *
     * @param string $authors
     * @return ScientificActivity
     */
    public function setAuthors($authors)
    {
        $this->authors = $authors;

        return $this;
    }

    /**
     * Get authors
     *
     * @return string
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ScientificActivity
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get date in string format for elasticsearch indexing
     *
     * @return string
     */
    public function getDateEs()
    {
        return $this->getDate()->format('Y-m-d');
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ScientificActivity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set descriptionEn
     *
     * @param string $description
     * @return ProfessionalActivity
     */
    public function setDescriptionEn($description)
    {
        $this->descriptionEn = $description;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->descriptionEn;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Person $person
     * @return ScientificActivity
     */
    public function setPerson(\AppBundle\Entity\Person $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set scientificActivityCategory
     *
     * @param \AppBundle\Entity\ScientificActivityCategory $scientificActivityCategory
     * @return ScientificActivity
     */
    public function setScientificActivityCategory(\AppBundle\Entity\ScientificActivityCategory $scientificActivityCategory = null)
    {
        $this->scientificActivityCategory = $scientificActivityCategory;

        return $this;
    }

    /**
     * Get scientificActivityCategory
     *
     * @return \AppBundle\Entity\ScientificActivityCategory
     */
    public function getScientificActivityCategory()
    {
        return $this->scientificActivityCategory;
    }

    /**
     * Set scientificActivityType
     *
     * @param \AppBundle\Entity\ScientificActivityType $scientificActivityType
     * @return ScientificActivity
     */
    public function setScientificActivityType(\AppBundle\Entity\ScientificActivityType $scientificActivityType = null)
    {
        $this->scientificActivityType = $scientificActivityType;

        return $this;
    }

    /**
     * Get scientificActivityType
     *
     * @return \AppBundle\Entity\ScientificActivityType
     */
    public function getScientificActivityType()
    {
        return $this->scientificActivityType;
    }
}
