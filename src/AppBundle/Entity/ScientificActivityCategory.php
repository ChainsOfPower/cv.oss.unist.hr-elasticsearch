<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cv_scientific_activity_category")
 */
class ScientificActivityCategory
{
    // ====
    // Data
    // ====

    /**
     * Identifikator
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * Autori
     * @ORM\Column(name="scientific_activity_category_name", type="string", length=128, nullable=true)
     * @var string
     */
    protected $name;

    /**
     * Autori
     * @ORM\Column(name="scientific_activity_category_name_en", type="string", length=128, nullable=true)
     * @var string
     */
    protected $nameEn;

    // =========
    // Relations
    // =========

    /**
     * Lista tipova kategorije znanstvene djelatnosti
     * @ORM\OneToMany(targetEntity="ScientificActivityType", mappedBy="scientificActivityCategory", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $scientificActivityTypes;

    /**
     * Lista znanstvenih aktivnosti
     * @ORM\OneToMany(targetEntity="ScientificActivity", mappedBy="scientificActivityCategory", cascade={"remove", "persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    protected $scientificActivities;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scientificActivityTypes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->scientificActivities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScientificActivityCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return ScientificActivityCategory
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Add scientificActivityTypes
     *
     * @param \AppBundle\Entity\ScientificActivityType $scientificActivityTypes
     * @return ScientificActivityCategory
     */
    public function addScientificActivityType(\AppBundle\Entity\ScientificActivityType $scientificActivityTypes)
    {
        $this->scientificActivityTypes[] = $scientificActivityTypes;

        return $this;
    }

    /**
     * Remove scientificActivityTypes
     *
     * @param \AppBundle\Entity\ScientificActivityType $scientificActivityTypes
     */
    public function removeScientificActivityType(\AppBundle\Entity\ScientificActivityType $scientificActivityTypes)
    {
        $this->scientificActivityTypes->removeElement($scientificActivityTypes);
    }

    /**
     * Get scientificActivityTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScientificActivityTypes()
    {
        return $this->scientificActivityTypes;
    }

    /**
     * Add scientificActivities
     *
     * @param \AppBundle\Entity\ScientificActivity $scientificActivities
     * @return ScientificActivityCategory
     */
    public function addScientificActivity(\AppBundle\Entity\ScientificActivity $scientificActivities)
    {
        $this->scientificActivities[] = $scientificActivities;

        return $this;
    }

    /**
     * Remove scientificActivities
     *
     * @param \AppBundle\Entity\ScientificActivity $scientificActivities
     */
    public function removeScientificActivity(\AppBundle\Entity\ScientificActivity $scientificActivities)
    {
        $this->scientificActivities->removeElement($scientificActivities);
    }

    /**
     * Get scientificActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScientificActivities()
    {
        return $this->scientificActivities;
    }

    public function __toString()
    {
        return $this->name;
    }
}
