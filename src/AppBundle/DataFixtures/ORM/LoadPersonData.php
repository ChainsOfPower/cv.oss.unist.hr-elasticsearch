<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 29.8.2016.
 * Time: 1:21
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Person;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPersonData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {


        $personAdmin = new Person();
        $personAdmin->setUsername("ivan");
        $personAdmin->setFirstName("Ivan");
        $personAdmin->setLastName("Vukman");
        $personAdmin->setEmail("ivan.vukman@gmail.com");
        $personAdmin->setPlainPassword("123");
        $personAdmin->setRoles(array('ROLE_ADMIN'));
        $personAdmin->setEnabled(true);
        $personAdmin->setHide(true);
        $personAdmin->setGender("M");


        $personUser1 = new Person();
        $personUser1->setUsername("mate");
        $personUser1->setFirstName("Mate");
        $personUser1->setLastName("Matic");
        $personUser1->setEmail("mate@gmail.com");
        $personUser1->setPlainPassword("123");
        $personUser1->setRoles(array('ROLE_USER'));
        $personUser1->setEnabled(true);
        $personUser1->setHide(false);
        $personUser1->setGender("M");


        $personUser2 = new Person();
        $personUser2->setUsername("ivo");
        $personUser2->setFirstName("Ivo");
        $personUser2->setLastName("Ivic");
        $personUser2->setEmail("ivo@gmail.com");
        $personUser2->setPlainPassword("123");
        $personUser2->setRoles(array('ROLE_USER'));
        $personUser2->setEnabled(true);
        $personUser2->setHide(false);
        $personUser2->setGender("M");

        $manager->persist($personAdmin);
        $manager->persist($personUser1);
        $manager->persist($personUser2);

        $manager->flush();
    }

}