<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 14.9.2016.
 * Time: 1:27
 */

namespace AppBundle\Services;

use AppBundle\Entity\Person;
use AppBundle\Entity\ProfessionalActivity;
use AppBundle\Entity\ScientificActivity;
use AppBundle\Entity\TeachingActivity;
use AppBundle\Entity\WorkExperience;
use Doctrine\ORM\EntityManager;
use Elastica\Exception\NotFoundException;
use FOS\ElasticaBundle\Persister\ObjectPersister;
use FOS\ElasticaBundle\Elastica\Index;

class EsIndexableService
{
    protected $em;
    protected $elasticaIndexManager;
    protected $scientificActivityPersister;
    protected $professionalActivityPersister;
    protected $teachingActivityPersister;
    protected $workExperiencePersister;


    public function __construct(EntityManager $entityManager,Index $elasticaIndexManager, ObjectPersister $scientificActivityPersister, ObjectPersister $professinalActivityPersister, ObjectPersister $teachingActivityPersister, ObjectPersister $workExperiencePersister)
    {
        $this->em = $entityManager;
        $this->elasticaIndexManager = $elasticaIndexManager;
        $this->scientificActivityPersister = $scientificActivityPersister;
        $this->professionalActivityPersister = $professinalActivityPersister;
        $this->teachingActivityPersister = $teachingActivityPersister;
        $this->workExperiencePersister = $workExperiencePersister;
    }

    public function personIndexable(Person $person)
    {
        $scientificActivities = $person->getScientificActivities();
        $professionalActivities = $person->getProfessionalActivities();
        $teachingActivities = $person->getTeachingActivities();
        $workExperiences = $person->getWorkExperience();
        $elasticaScientificActivityIndexManager = $this->elasticaIndexManager->getType('scientificActivity');
        if(count($scientificActivities) == 0)
        {
            return $person->shouldIndex();
        }
        if ($person->shouldIndex() == false) {
            try
            {
                $exists = $elasticaScientificActivityIndexManager->getDocument($scientificActivities[0]->getId());
                foreach ($scientificActivities as $scientificActivity) {
                    $this->scientificActivityPersister->deleteById($scientificActivity->getId());
                }
                foreach ($professionalActivities as $professionalActivity) {
                    $this->professionalActivityPersister->deleteById($professionalActivity->getId());
                }
                foreach ($teachingActivities as $teachingActivity) {
                    $this->teachingActivityPersister->deleteById($teachingActivity->getId());
                }
                foreach ($workExperiences as $workExperience) {
                    $this->workExperiencePersister->deleteById($workExperience->getId());
                }
            }
            catch(NotFoundException $e)
            {
                return $person->shouldIndex();
            }

        } else {
            try
            {
                $exists = $elasticaScientificActivityIndexManager->getDocument($scientificActivities[0]->getId());
                return $person->shouldIndex();
            }
            catch(NotFoundException $e)
            {
                foreach ($scientificActivities as $scientificActivity) {
                    $this->scientificActivityPersister->insertOne($scientificActivity);
                }
                foreach ($professionalActivities as $professionalActivity) {
                    $this->professionalActivityPersister->insertOne($professionalActivity);
                }
                foreach ($teachingActivities as $teachingActivity) {
                    $this->teachingActivityPersister->insertOne($teachingActivity);
                }
                foreach ($workExperiences as $workExperience) {
                    $this->workExperiencePersister->insertOne($workExperience);
                }

                return $person->shouldIndex();
            }
        }
    }

    public function scientificActivityIndexable(ScientificActivity $scientificActivity)
    {
        $person = $scientificActivity->getPerson();

        return $person->shouldIndex();
    }

    public function professionalActivityIndexable(ProfessionalActivity $professionalActivity)
    {
        $person = $professionalActivity->getPerson();

        return $person->shouldIndex();
    }

    public function teachingActivityIndexable(TeachingActivity $teachingActivity)
    {
        $person = $teachingActivity->getPerson();

        return $person->shouldIndex();
    }

    public function workExperienceIndexable(WorkExperience $workExperience)
    {
        $person = $workExperience->getPerson();

        return $person->shouldIndex();
    }
}