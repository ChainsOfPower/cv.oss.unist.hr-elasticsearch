<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', null, array(
                'label'    => 'Ime:',
                'required' => false
            ))
            ->add('lastName', null, array(
                'label'    => 'Prezime:',
                'required' => false
            ))
            ->add('gender', 'choice', array(
                'choices'  => array(
                    'M' => 'Muško',
                    'Z' => 'Žensko'
                ),
                'label'    => 'Spol',
                'required' => false
            ))
            ->add('profilePictureFile', 'file', array(
                'label'    => 'Fotografija',
                'required' => false
            ))
            ->add('dateOfBirth', 'date', array(
                'label'    => 'Datum i godina rođenja:',
                'required' => false,
                'years'    => range(date('Y'), date('Y') - 120)
            ))
            ->add('scientificTitlePre', null, array(
                'label'    => 'Tekst ispred imena (primjer: dr. sc., mr. sc., doc...):',
                'required' => false
            ))
            ->add('scientificTitlePreEn', null, array(
                'label'    => 'Tekst ispred imena - ENGLESKI:',
                'required' => false
            ))
            ->add('scientificTitlePost', null, array(
                'label'    => 'Tekst iza imena (primjer: dr.med., dipl. ing., predavač, viši predavač, lektor...):',
                'required' => false
            ))
            ->add('scientificTitlePostEn', null, array(
                'label'    => 'Tekst iza imena - ENGLESKI:',
                'required' => false
            ))
            ->add('university', null, array(
                'empty_value'  => '-- Odaberite sveučilište',
                'label'        => 'Sveučilište:',
                'required'     => false
            ))
            ->add('universityDepartment', 'shtumi_dependent_filtered_entity', array(
                'entity_alias' => 'university_department_by_university',
                'empty_value'  => '-- Odaberite sastavnicu',
                'parent_field' => 'university',
                'label'        => 'Sastavnica:',
                'required'     => false
            ))
            ->add('universityDepartmentDesk', 'shtumi_dependent_filtered_entity', array(
                'entity_alias' => 'university_department_desk_by_university_department',
                'empty_value'  => '-- Odaberite odsjek/zavod',
                'parent_field' => 'universityDepartment',
                'label'        => 'Odsjek/Zavod:',
                'required'     => false
            ))
            ->add('freelancer', null, array(
                'label'    => 'Vanjski suradnik u nastavi (honorarac)',
                'required' => false
            ))
            ->add('citizenship', null, array(
                'label'    => 'Državljanstvo:',
                'required' => false
            ))
            ->add('citizenshipEn', null, array(
                'label'    => 'Državljanstvo - ENGLESKI:',
                'required' => false
            ))
            ->add('address', null, array(
                'label'    => 'Adresa:',
                'required' => false
            ))
            ->add('city', null, array(
                'label'       => 'Grad:',
                'required'    => false,
                'placeholder' => '-- Odaberite',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')->orderBy('c.name', 'ASC');
                }
            ))
            ->add('phone', null, array(
                'label'    => 'Telefon:',
                'required' => false
            ))
            ->add('fax', null, array(
                'label'    => 'Fax:',
                'required' => false
            ))
            ->add('email', null, array(
                'label'    => 'E-mail:',
                'required' => false
            ))
            ->add('web', null, array(
                'label'    => 'Osobna web stranica:',
                'required' => false
            ))
            ->add('registrationNumber', null, array(
                'label'    => 'Matični broj iz Upisnika znanstvenika:',
                'required' => false
            ))
            ->add('scientificArea', null, array(
                'label'       => 'Znanstveno područje:',
                'required'    => false,
                'placeholder' => '-- Odaberite područje',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                    return $er->createQueryBuilder('a')->orderBy('a.name', 'ASC');
                }
            ))
            ->add('scientificField', 'shtumi_dependent_filtered_entity', array(
                'label'       => 'Znanstveno polje:',
                'entity_alias' => 'scientific_field_by_scientific_area',
                'empty_value'  => '-- Odaberite polje',
                'parent_field' => 'scientificArea',
                'required'     => false
            ))
            ->add('scientificBranch', 'shtumi_dependent_filtered_entity', array(
                'label'       => 'Znanstvena grana:',
                'entity_alias' => 'scientific_branch_by_scientific_field',
                'empty_value'  => '-- Odaberite granu',
                'parent_field' => 'scientificField',
                'required'     => false
            ))
            ->add('scientificTitle', null, array(
                'label' => 'Znanstveno ili umjetničko zvanje i datum posljednjeg izbora:'
            ))
            ->add('scientificTitleDate', null, array(
                'label'    => '',
                'years'    => range(date('Y'), date('Y') - 70),
                'required' => false
            ))
            ->add('academicTitle', null, array(
                'label' => 'Znanstveno-nastavno ili umjetničko-nastavno ili nastavno zvanje i datum posljednjeg izbora:'
            ))
            ->add('academicTitleDate', null, array(
                'label'    => '',
                'years'    => range(date('Y'), date('Y') - 70),
                'required' => false
            ))
            ->add('driverLicense', null, array(
                'label'    => 'Vozačka dozvola:',
                'required' => false
            ))
            ->add('additional', null, array(
                'label'    => 'Dodatni podaci:',
                'required' => false
            ))
            ->add('additionalEn', null, array(
                'label'    => 'Dodatni podaci - ENGLESKI:',
                'required' => false
            ))
            ->add('hide', null, array(
                'label'    => 'Ne želim da se moj profil prikaže na popisu osoblja',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Person',
            'locale' => 'hr'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_person_personal';
    }
}
