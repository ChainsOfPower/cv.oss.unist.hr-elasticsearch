<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Form\Collection\PersonLanguageType as PersonLanguageBaseType;

class PersonLanguageType extends AbstractType
{
    /**
     * @var string Current locale
     */
    private $_locale;

    /**
     * @param string $_locale
     */
    public function __construct($_locale = 'hr') {
        $this->_locale = $_locale;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('languages', 'collection', array(
                'type'         => new PersonLanguageBaseType(),
                'allow_add'    => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'      => array('label' => false)
            ));
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\Person',
            'cascade_validation' => true,
            'locale'             => 'hr'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_person_language';
    }
}
