<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 18.9.2016.
 * Time: 2:31
 */

namespace AppBundle\Form\ElasticSearch;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchProfessionalActivitiesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', 'text', array(
                'label'                 =>  'professional_activity_description',
                'required'              =>  false,
                'translation_domain'    =>  'messages'
            ))
            ->add('authors', 'text', array(
                'label'                 =>  'authors',
                'required'              =>  false,
                'translation_domain'    =>  'messages'
            ))
            ->add('age', 'choice', array(
                'choices'   =>  array(
                    '/'             =>  null,
                    'one_year'      => 1,
                    'two_years'     => 2,
                    'three_years'   => 3,
                    'four_years'    => 4,
                    'five_years'    => 5
                ),
                'choices_as_values'     => true,
                'label'                 =>  'do_not_take_into_account_older_than',
                'required'              =>  true,
                'translation_domain'    => 'messages'
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    =>  null
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'professionalActivitiesSearch';
    }
}