<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 15.9.2016.
 * Time: 3:52
 */

namespace AppBundle\Form\ElasticSearch;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class SearchSkillsAndCompetencesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('social_skills', 'text', array(
                'label'                 => 'social_skills_and_competences',
                'required'              => false,
                'translation_domain'    => 'messages'
            ))
            ->add('organization_skills', 'text', array(
                'label'                 => 'organizational_skills_and_competences',
                'required'              => false,
                'translation_domain'    => 'messages'
            ))
            ->add('technical_skills', 'text', array(
                'label'                 => 'technical_skills_and_competences',
                'required'              => false,
                'translation_domain'    => 'messages'
            ))
            ->add('art_skills', 'text', array(
                'label'                 => 'art_skills_and_competences',
                'required'              => false,
                'translation_domain'    => 'messages'
            ))
            ->add('misc_skills', 'text', array(
                'label'                 => 'misc_skills_and_competences',
                'required'              => false,
                'translation_domain'    => 'messages'
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'skillsAndCompetencesSearch';
    }
}