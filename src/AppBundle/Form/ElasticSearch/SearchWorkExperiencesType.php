<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 18.9.2016.
 * Time: 19:39
 */

namespace AppBundle\Form\ElasticSearch;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchWorkExperiencesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('job_title', 'text', array(
                'label'                 => 'job_title',
                'required'              => false,
                'translation_domain'    => 'messages'
            ))
            ->add('area', 'text', array(
                'label'                 => 'area',
                'required'              => false,
                'translation_domain'    => 'messages'
            ))
            ->add('institution', 'text', array(
                'label'                 => 'institution',
                'required'              => false,
                'translation_domain'    => 'messages'
            ))
            ->add('function', 'text', array(
                'label'                 => 'function',
                'required'              => false,
                'translation_domain'    => 'messages'
            ))
            ->add('current', 'checkbox', array(
                'label'                 => 'show_only_current_jobs',
                'required'              => true,
                'translation_domain'    => 'messages'
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'workExperiencesSearch';
    }
}