<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SkillsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('socialSkills', null, array(
                'label'    => 'Socijalne vještine i kompetencije:',
                'required' => false
            ))
            ->add('socialSkillsEn', null, array(
                'label'    => 'Socijalne vještine i kompetencije - ENGLESKI:',
                'required' => false
            ))
            ->add('organizationalSkills', null, array(
                'label'    => 'Organizacijske vještine i kompetencije:',
                'required' => false
            ))
            ->add('organizationalSkillsEn', null, array(
                'label'    => 'Organizacijske vještine i kompetencije - ENGLESKI:',
                'required' => false
            ))
            ->add('technicalSkills', null, array(
                'label'    => 'Tehničke vještine i kompetencije:',
                'required' => false
            ))
            ->add('technicalSkillsEn', null, array(
                'label'    => 'Tehničke vještine i kompetencije - ENGLESKI:',
                'required' => false
            ))
            ->add('artSkills', null, array(
                'label'    => 'Umjetničke vještine i kompetencije:',
                'required' => false
            ))
            ->add('artSkillsEn', null, array(
                'label'    => 'Umjetničke vještine i kompetencije - ENGLESKI:',
                'required' => false
            ))
            ->add('miscSkills', null, array(
                'label'    => 'Ostale vještine i kompetencije:',
                'required' => false
            ))
            ->add('miscSkillsEn', null, array(
                'label'    => 'Ostale vještine i kompetencije - ENGLESKI:',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Person',
            'locale' => 'hr'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_person_skills';
    }
}
