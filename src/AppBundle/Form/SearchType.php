<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', 'text', array(
                'label'        => 'search.filter.fist_name',
                'required'     => false
            ))
            ->add('last_name', 'text', array(
                'label'        => 'search.filter.last_name',
                'required'     => false
            ))
            ->add('university', 'entity', array(
                'empty_value'  => 'dropdown_university_choose',
                'class'        => 'AppBundle:University',
                'label'        => 'search.filter.university',
                'property'     => ($options['locale'] == 'hr')? 'name' : 'nameEn',
                'required'     => false
            ))
            ->add('universityDepartment', 'shtumi_dependent_filtered_entity', array(
                'entity_alias' => ($options['locale'] == 'hr')? 'search_university_department_by_university' : 'search_university_department_by_university_en',
                'empty_value'  => ($options['locale'] == 'hr')? 'dropdown_universityDepartment_choose' : 'dropdown_universityDepartment_choose_en',
                'parent_field' => 'university',
                'label'        => 'search.filter.university_department',
                'required'     => false
            ))
            ->add('universityDepartmentDesk', 'shtumi_dependent_filtered_entity', array(
                'entity_alias' => ($options['locale'] == 'hr')? 'search_university_department_desk_by_university_department' : 'search_university_department_desk_by_university_department_en',
                'empty_value'  => ($options['locale'] == 'hr')? 'dropdown_universityDepartmentDesk_choose' : 'dropdown_universityDepartmentDesk_choose_en',
                'parent_field' => 'universityDepartment',
                'label'        => 'search.filter.university_department_desk',
                'required'     => false
            ))
            ->add('scientificArea', 'entity', array(
                'empty_value'  => 'dropdown_scientificArea_choose',
                'class'        => 'AppBundle:ScientificArea',
                'label'        => 'search.filter.scientific_area',
                'property'     => ($options['locale'] == 'hr')? 'name' : 'nameEn',
                'required'     => false
            ))
            ->add('scientificField', 'shtumi_dependent_filtered_entity', array(
                'entity_alias' => ($options['locale'] == 'hr')? 'search_scientific_field_by_scientific_area' : 'search_scientific_field_by_scientific_area_en',
                'empty_value'  => ($options['locale'] == 'hr')? 'dropdown_scientificField_choose' : 'dropdown_scientificField_choose_en',
                'parent_field' => 'scientificArea',
                'label'        => 'search.filter.scientific_field',
                'required'     => false
            ))
            ->add('scientificBranch', 'shtumi_dependent_filtered_entity', array(
                'entity_alias' => ($options['locale'] == 'hr')? 'search_scientific_branch_by_scientific_field' : 'search_scientific_branch_by_scientific_field_en',
                'empty_value'  => ($options['locale'] == 'hr')? 'dropdown_scientificBranch_choose' : 'dropdown_scientificBranch_choose_en',
                'parent_field' => 'scientificField',
                'label'        => 'search.filter.scientific_branch',
                'required'     => false
            ))
            ->add('dateFrom', 'date', array(
                'label' => 'Datum od:',
                'years' => range(date('Y'), date('Y') - 70)
            ))
            ->add('dateTo', 'date', array(
                'label' => 'Datum do:',
                'years' => range(date('Y'), date('Y') - 70)
            ))
            ->add('freelancer', 'checkbox', array(
                'label'    => 'search.filter.freelancers',
                'data'     => true,
                'required' => false
            ))
            ->add('expiration', 'checkbox', array(
                'label'    => 'search.filter.about_to_expire',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'locale'     => 'hr'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'database_search';
    }
}
