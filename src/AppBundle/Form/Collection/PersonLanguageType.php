<?php

namespace AppBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonLanguageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', null, array(
                'label'       => 'Jezik:',
                'required'    => true,
                'placeholder' => '-- Odaberite',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    return $repository->createQueryBuilder('l')->orderBy('l.name', 'ASC');
                }
            ))
            ->add('grade', 'choice', array(
                'label'    => 'Ocjena:',
                'required' => true,
                'choices'  => array(
                    '2' => '2 (dovoljan)',
                    '3' => '3 (dobar)',
                    '4' => '4 (vrlo dobar)',
                    '5' => '5 (izvrstan)'
                ),
                'placeholder' => '-- Odaberite'
            ))
            ->add('motherTongue', null, array(
                'label'    => 'Postavi kao materinji jezik',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PersonLanguage'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_personlanguage';
    }
}
