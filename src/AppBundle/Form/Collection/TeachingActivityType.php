<?php

namespace AppBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TeachingActivityType extends AbstractType
{
    /**
     * @var string Current locale
     */
    private $_locale;

    /**
     * @param string $_locale
     */
    public function __construct($_locale = 'hr') {
        $this->_locale = $_locale;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('authors', null, array(
                'label'    => 'Autori:',
                'required' => true
            ))
            ->add('description', null, array(
                'label'    => 'Opis (sadržaj):',
                'required' => true
            ))
            ->add('descriptionEn', null, array(
                'label'    => 'Opis (sadržaj) - ENGLESKI:',
                'required' => true
            ))
            ->add('teachingActivityCategory', null, array(
                'label'       => 'Kategorija:',
                'empty_value' => '-- Odaberite kategoriju',
                'required'    => false
            ))
            ->add('teachingActivityType', 'shtumi_dependent_filtered_entity', array(
                'entity_alias' => 'teaching_type_by_category',
                'empty_value'  => '-- Odaberite tip',
                'parent_field' => 'teachingActivityCategory',
                'label'        => 'Tip:',
                'required'     => false
            ))
            ->add('date', null, array(
                'label'      => 'Datum:',
                'years'      => range(date('Y'), date('Y') - 70),
                'required'   => false
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TeachingActivity'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_teaching_activity';
    }
}
