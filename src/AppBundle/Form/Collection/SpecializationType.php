<?php

namespace AppBundle\Form\Collection;

use Symfony\Component\Form\CallbackValidator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SpecializationType extends AbstractType
{
    /**
     * @var string Current locale
     */
    private $_locale;

    /**
     * @param string $_locale
     */
    public function __construct($_locale = 'hr') {
        $this->_locale = $_locale;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('institution', null, array(
                'label'    => 'Ustanova:',
                'required' => true
            ))
            ->add('institutionEn', null, array(
                'label'    => 'Ustanova - ENGLESKI:',
                'required' => true
            ))
            ->add('location', null, array(
                'label'    => 'Mjesto:',
                'required' => false
            ))
            ->add('locationEn', null, array(
                'label'    => 'Mjesto - ENGLESKI:',
                'required' => false
            ))
            ->add('specializationArea', null, array(
                'label'    => 'Područje:',
                'required' => false
            ))
            ->add('specializationAreaEn', null, array(
                'label'    => 'Područje - ENGLESKI:',
                'required' => false
            ))
            ->add('date', null, array(
                'label'    => 'Datum i godina:',
                'years'    => range(date('Y'), date('Y') - 70),
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Specialization'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_specialization';
    }
}
