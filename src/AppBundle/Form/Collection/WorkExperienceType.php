<?php

namespace AppBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WorkExperienceType extends AbstractType
{
    /**
     * @var string Current locale
     */
    private $_locale;

    /**
     * @param string $_locale
     */
    public function __construct($_locale = 'hr') {
        $this->_locale = $_locale;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('institution', null, array(
                'label'    => 'Ustanova zaposlenja:',
                'required' => true
            ))
            ->add('institutionEn', null, array(
                'label'    => 'Ustanova zaposlenja - ENGLESKI:',
                'required' => true
            ))
            ->add('jobTitle', null, array(
                'label'    => 'Naziv radnog mjesta:',
                'required' => false
            ))
            ->add('jobTitleEn', null, array(
                'label'    => 'Naziv radnog mjesta - ENGLESKI:',
                'required' => false
            ))
            ->add('function', null, array(
                'label'    => 'Funkcija:',
                'required' => false
            ))
            ->add('functionEn', null, array(
                'label'    => 'Funkcija - ENGLESKI:',
                'required' => false
            ))
            ->add('area', null, array(
                'label'    => 'Područje rada:',
                'required' => false
            ))
            ->add('areaEn', null, array(
                'label'    => 'Područje rada - ENGLESKI:',
                'required' => false
            ))
            ->add('dateFrom', null, array(
                'label'    => 'Datum zaposlenja (od):',
                'years'    => range(date('Y'), date('Y') - 70),
                'required' => false
            ))
            ->add('dateTo', null, array(
                'label'    => 'Datum zaposlenja (do):',
                'years'    => range(date('Y'), date('Y') - 70),
                'required' => false
            ))
            ->add('current', null, array(
                'label'    => 'Postavi kao trenutno zaposlenje',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\WorkExperience'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_workexperience';
    }
}
