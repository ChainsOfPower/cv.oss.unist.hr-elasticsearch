<?php

namespace AppBundle\Form\Collection;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SchoolType extends AbstractType
{
    /**
     * @var string Current locale
     */
    private $_locale;

    /**
     * @param string $_locale
     */
    public function __construct($_locale = 'hr') {
        $this->_locale = $_locale;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('location', null, array(
                'label'    => 'Mjesto:',
                'required' => true
            ))
            ->add('locationEn', null, array(
                'label'    => 'Mjesto - ENGLESKI:',
                'required' => true
            ))
            ->add('institution', null, array(
                'label'    => 'Ustanova:',
                'required' => true
            ))
            ->add('institutionEn', null, array(
                'label'    => 'Ustanova - ENGLESKI:',
                'required' => true
            ))
            ->add('title', null, array(
                'label'    => 'Zvanje:',
                'required' => true
            ))
            ->add('titleEn', null, array(
                'label'    => 'Zvanje - ENGLESKI:',
                'required' => true
            ))
            ->add('date', null, array(
                'label'    => 'Datum:',
                'years'    => range(date('Y'), date('Y') - 70),
                'required' => false
            ))
            ->add('highestDegree', null, array(
                'label'    => 'Postavi kao najviši stupanj',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\School'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_school';
    }
}
