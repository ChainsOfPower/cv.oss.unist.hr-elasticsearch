<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Form\Collection\ScientificActivityType as ScientificActivityBaseType;

class ScientificActivityType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('scientificActivities', 'collection', array(
                'type'               => new ScientificActivityBaseType($options['locale']),
                'allow_add'          => true,
                'allow_delete'       => true,
                'by_reference'       => false,
                'options'            => array('label' => false)
            ));
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\Person',
            'cascade_validation' => true,
            'locale'             => 'hr'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_person_scientific_activity';
    }
}
