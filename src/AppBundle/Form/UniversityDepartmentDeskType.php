<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UniversityDepartmentDeskType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'label'    => 'Naziv:',
                'required' => true
            ))
            ->add('nameEn', null, array(
                'label'    => 'Naziv - ENGLESKI:',
                'required' => true
            ))
            ->add('contact', null, array(
                'label'    => 'Kontakt:',
                'required' => false
            ))
            ->add('universityDepartment', null, array(
                'label'    => 'Sveučilište:',
                'required' => true
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\UniversityDepartmentDesk'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_universitydepartmentdesk';
    }
}
